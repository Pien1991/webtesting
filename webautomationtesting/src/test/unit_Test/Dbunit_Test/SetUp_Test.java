package Dbunit_Test;

import org.dbunit.Assertion;
import org.dbunit.DatabaseUnitException;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.database.QueryDataSet;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.filter.DefaultColumnFilter;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.dbunit.util.fileloader.DataFileLoader;
import org.dbunit.util.fileloader.FlatXmlDataFileLoader;
import org.testng.annotations.*;
import util.databaseHelper.ConnectDatabase;
import util.parser.FileLoader;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * Created by workstation47 on 12/20/16.
 */
public class SetUp_Test {


    private IDataSet testDataSet = new FlatXmlDataFileLoader().load("/DbUnitDataSet/tutorials/test_2.xml");

    @BeforeTest(alwaysRun = true)
    public void BeforeTest(){
        ConnectDatabase.connect();
    }


    @BeforeClass(alwaysRun = true)
    public void DataWrite_Test(){
        long start = System.currentTimeMillis();
        try {
            Connection connection = ConnectDatabase.getConnection();
            IDatabaseConnection conn = new DatabaseConnection(connection);
            QueryDataSet queryDataSet = new QueryDataSet(conn);
            queryDataSet.addTable("wallet_balance");
            File tempFile = new File(FileLoader.getAbsolutePath("/DbUnitDataSet/tutorials/backupDataSet.xml"));
            System.out.println(FileLoader.getAbsolutePath("/DbUnitDataSet/tutorials/backupDataSet.xml"));
            FlatXmlDataSet.write(queryDataSet, new FileWriter(tempFile),"UTF-8");

        } catch (DatabaseUnitException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            System.out.println(System.currentTimeMillis()-start);
        }
    }


    @Test
    public void DataInsert_Test(){
        long start = System.currentTimeMillis();
        try {
            Connection connection = ConnectDatabase.getConnection();
            IDatabaseConnection conn = new DatabaseConnection(connection);
            System.out.println(testDataSet.getTable("wallet_balance").getValue(1,"ccy"));
            DatabaseOperation.REFRESH.execute(conn,testDataSet);
        } catch (DatabaseUnitException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            System.out.println(System.currentTimeMillis()-start);
        }
    }



    @Test(dependsOnMethods = {"DataInsert_Test"})
    public void DataAssertion_Test(){
        long start = System.currentTimeMillis();
        try {
            Connection connection = ConnectDatabase.getConnection();
            IDatabaseConnection conn = new DatabaseConnection(connection);
            ITable expectedTable = testDataSet.getTable("wallet_balance");
            ITable actualTable = conn.createTable("wallet_balance");
            actualTable = DefaultColumnFilter.includedColumnsTable(actualTable, new String[]{"id","version","ccy"});

            Assertion.assertEquals(expectedTable,actualTable);

        } catch (DatabaseUnitException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            System.out.println(System.currentTimeMillis()-start);
        }
    }




    @AfterClass(alwaysRun = true)
    public void DataRockBack_Test(){
        long start = System.currentTimeMillis();
        try {
            Connection connection = ConnectDatabase.getConnection();
            IDatabaseConnection conn = new DatabaseConnection(connection);
            DataFileLoader loader = new FlatXmlDataFileLoader();
            IDataSet dataSet = loader.load("/DbUnitDataSet/tutorials/backupDataSet.xml");

            DatabaseOperation.CLEAN_INSERT.execute(conn,dataSet);
        } catch (DatabaseUnitException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            System.out.println(System.currentTimeMillis()-start);
        }
    }


    @AfterTest(alwaysRun = true)
    public void AfterTest(){
        ConnectDatabase.disconnect();
    }

}
