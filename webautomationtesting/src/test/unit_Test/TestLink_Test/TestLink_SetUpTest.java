package TestLink_Test;

import br.eti.kinoshita.testlinkjavaapi.TestLinkAPI;
import br.eti.kinoshita.testlinkjavaapi.constants.ExecutionStatus;
import br.eti.kinoshita.testlinkjavaapi.constants.ExecutionType;
import br.eti.kinoshita.testlinkjavaapi.constants.ResponseDetails;
import br.eti.kinoshita.testlinkjavaapi.constants.TestCaseDetails;
import br.eti.kinoshita.testlinkjavaapi.model.*;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

/**
 * Created by shepardpin on 2/21/17.
 */
public class TestLink_SetUpTest {

    // Substitute your Dev Key Here
    public static final String DEV_KEY =  "c528142a1dd1c635ac6471053acc8cee";
    // Substitute your Server URL Here
    public static  URL SERVER_URL ;

    TestLinkAPI api;
    TestPlan testPlan ;
    TestProject testProject;
    TestCase[] testCases;
    Build build ;


    {
        try {
            SERVER_URL = new URL("http://testlink.anxintl.com/lib/api/xmlrpc/v1/xmlrpc.php");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    @BeforeClass
    public void ConnectTestLink(){
        api = new TestLinkAPI(SERVER_URL,DEV_KEY);
    }

    @Test
    public void  Test_On_Test_Plan(){
        testProject  = api.getTestProjectByName("Card");

        testPlan = api.getTestPlanByName("General Regression Test Plan",testProject.getName());
        build = api.getLatestBuildForTestPlan(testPlan.getId());

        System.out.println("Test testProject : "+testProject.getName());
        System.out.println("Test Plan : "+testPlan.getName());
        System.out.println("Test Build : "+build.getName());
        System.out.println("=========================================================");

    }

    @Test(dependsOnMethods = "Test_On_Test_Plan")
    public void Test_On_Test_Platform(){
        Platform[] platforms = api.getTestPlanPlatforms(testPlan.getId());

        for (int i = 0; i < platforms.length; i++) {
            Platform platform = platforms[i];
            System.out.println(platform.getName());
            System.out.println(platform.getNotes());

        }

    }

    @Test(dependsOnMethods = "Test_On_Test_Plan")
    public void Test_On_The_Latest_Build(){
        Build build = api.getLatestBuildForTestPlan(testPlan.getId());

        System.out.println(build.getName());
        System.out.println(build.getNotes());

    }



    @Test(dependsOnMethods = "Test_On_Test_Plan")
    public void Test_On_Test_Cases(){
        testCases = api.getTestCasesForTestPlan(testPlan.getId(),null,null,null,null,null,null,null,ExecutionType.AUTOMATED,false,TestCaseDetails.FULL );

        for (int i = 0; i < testCases.length; i++) {
            TestCase testCase = testCases[i];


            System.out.println("Test Plan - Test Case Platform  : "+testCase.getPlatform().getName());
            System.out.println("Test Plan - Test Case Full External Id : "+testCase.getFullExternalId());
            System.out.println("Test Plan - Test Case Id : "+testCase.getId());
            System.out.println("Test Plan - Test Case Version : "+testCase.getVersion());


            // There is a bug for TestCase Detail filter , user cannot get preconditions or summary
            CustomField AFT = api.getTestCaseCustomFieldDesignValue(testCase.getId(),testCase.getInternalId(),testCase.getVersion(),testProject.getId(),"AFT", ResponseDetails.FULL);
            CustomField APFT = api.getTestCaseCustomFieldDesignValue(testCase.getId(),testCase.getInternalId(),testCase.getVersion(),testProject.getId(),"APFT", ResponseDetails.FULL);


//            System.out.println("CustomField Label : "+customField.getLabel());
            System.out.println("Test Plan - Test Case Account for testing  : "+AFT.getValue());
            System.out.println("Test Plan - Test Case Account Password for testing  : "+APFT.getValue());


            List<TestCaseStep> testCaseSteps = testCase.getSteps();
            for (int j = 0; j < testCaseSteps.size(); j++) {
                TestCaseStep testCaseStep =  testCaseSteps.get(j);
                System.out.println("Test Plan - Test Case Step Action : "+testCaseStep.getActions());
                System.out.println("Test Plan - Test Case Step Expected Result : "+testCaseStep.getExpectedResults());
            }

            System.out.println("=========================================================");

        }
    }

    @Test(dependsOnMethods = {"Test_On_Test_Plan","Test_On_Test_Cases"})
    public  void  Test_On_Test_Execution_Result(){
        TestCase testCase_1 = testCases[0];
        Platform platform = testCase_1.getPlatform();
        api.setTestCaseExecutionResult(testCase_1.getId(),testCase_1.getInternalId(),testPlan.getId(), ExecutionStatus.FAILED,build.getId(),null,null,null,null,platform.getId(),null,null,false);


    }

}
