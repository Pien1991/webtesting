package util;

import config.Components;
import dataTemplate.Component;
import util.parser.FileLoader;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by shepardpin on 4/25/17.
 */
public class ComponentHelper {


    private static final Map<Components, Component> componentsCache = new HashMap<>();

    public static Component getComponent(Components target) {
        Component component =  componentsCache.get(target);
        if (component==null){

            File configFile = FileLoader.getFile(target.toString());
            component = new Component(configFile);
            componentsCache.put(target,component);
        }
        return component;
    }

}
