package util.navigationHelper;

import config.Page;
import config.TestEnv;
import config.Site;
import dataTemplate.User;
import org.openqa.selenium.WebDriver;
import remoteObject.pages.login.LoginPage;

/**
 * Created by workstation47 on 12/5/16.
 */
public class NavigationHelper {


    public static void navigate(WebDriver driver,TestEnv testEnv,Site site){
       navigate(driver,testEnv,site,Page.FRONT);
    }

    /**
     *
     *  The navigation Helper will ONLY help to navigate to page but it will NOT to wait all element until being loaded .
     *
     */
    public static void navigate(WebDriver driver, TestEnv testEnv, Site site, Page page){
        String baseUrl = Site.getUrl(site, testEnv);

        //
        if (page==Page.FRONT || page== Page.EXPLORER_FRONT){
            driver.get(baseUrl);
        }else {
            driver.get(baseUrl+page.toString());
        }

    }



    public  static void login(WebDriver driver, TestEnv testEnv, Site site , User user){
        LoginPage loginPage = new LoginPage(driver,testEnv,site);
        loginPage.login(user);
    }
}
