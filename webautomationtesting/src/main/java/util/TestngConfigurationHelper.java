package util;

import config.DriverType;
import config.TestEnv;
import config.Site;
import config.SuiteConfig;
import dataTemplate.User;
import managers.DriverManager;
import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.ITestResult;
import util.parser.UsersParser;

import java.util.Map;

/**
 * Created by workstation47 on 12/12/16.
 */
public class TestngConfigurationHelper {





    public static String getXmlParameter(ITestContext context , SuiteConfig key){
        /*
         * In TestNg , getXmlParameter will search local para .
         * If it is null , it will return global parameter .
         *
         */
        return context.getCurrentXmlTest().getParameter(key.toString());
    }

    public static String getXmlParameter(ITestResult result , SuiteConfig key){
        /*
         * In TestNg , getXmlParameter will search local para .
         * If it is null , it will return global parameter .
         *
         */
        return getXmlParameter(result.getTestContext(),key);
    }

    public static Site getSite(ITestContext context ){
        String website = getWebsiteName(context);

        // Set default testing site is ANX
        if (website==null){
            return Site.ANX;
        }else {
            return Site.getSite(website);
        }
    }



    public static TestEnv getTestEnv(ITestContext context ){
        return TestEnv.getTestEnv(getTestEnvironmentName(context));
    }

    public static  WebDriver getDriver(ITestContext context) {
        DriverType type = getDriverType(context);

        if (DriverManager.getDriver(type)==null ){
            DriverManager.setThreadDriver(type);
        }
            return DriverManager.getDriver(type);

    }




    /**
     *
     *  @author Shepard
     *
     *  The function is only to find user data saved in local csv .
     *
     *  Now we try to save testing user name inside test case on TestLink with :
     *  Custom field label : Account For Testing
     *
     */

    public static User getUser(ITestContext context){
        TestEnv testEnv = getTestEnv(context);
        Site site = getSite(context);
        return getUser(context, testEnv,site);
    }



    private static User getUser(ITestContext context, TestEnv testEnv, Site site){

        /*
         *
         * There will be a scenario that there is two same parameter in different suite level :
         * 1. Global parameter in suite level
         * 2. Local parameter in test level
         *
         * Therefore , the return parameter should be local one if it exists .
         *
         *
         */

        User user = null ;


        Map<String, String> localMap = context.getCurrentXmlTest().getLocalParameters();

        String localAlias  = localMap.get(SuiteConfig.USERALIAS.toString());
        if (localAlias!=null){
            return UsersParser.getUserByAlias(localAlias, testEnv,site);
        }

        String localName = localMap.get(SuiteConfig.USERNAME.toString());
        if (localName!=null){
            return UsersParser.getUserByUsername(localName, testEnv,site);
        }

        String globalName  = getXmlParameter(context, SuiteConfig.USERNAME);
        if (globalName!=null){
            user =  UsersParser.getUserByUsername(globalName, testEnv,site);

        }

        String globalAlias = getXmlParameter(context, SuiteConfig.USERALIAS);
        if (globalAlias!=null){
            user = UsersParser.getUserByAlias(globalAlias, testEnv,site);
        }

        return user;
    }



    public static String getBrowserName(ITestContext context) {
        return getXmlParameter(context, SuiteConfig.BROWSER);
    }

    public static DriverType getDriverType (ITestContext context){
        return DriverType.getDriverType(getBrowserName(context));
    }

    public static String getWebsiteName(ITestContext context ){
        return getXmlParameter(context, SuiteConfig.WEBSITE);
    }

    public static String getTestEnvironmentName(ITestContext context){
        return getXmlParameter(context, SuiteConfig.TEST_ENV);
    }

    public static String getTestProjectName(ITestResult iTestResult){
        return getXmlParameter(iTestResult, SuiteConfig.TEST_PROJECT);
    }

    public static String getTestPlanName(ITestResult iTestResult){
        return getXmlParameter(iTestResult, SuiteConfig.TEST_PLAN);
    }


}
