package util.parser;

import java.io.File;

/**
 * Created by workstation47 on 12/21/16.
 */
public class FileLoader {

    //TODO

    public  static String getAbsolutePath(String path){

        return FileLoader.class.getResource(path).getFile();
    }

    public  static File getFile(String path){

        return new File(FileLoader.class.getResource(path).getFile());
    }

}
