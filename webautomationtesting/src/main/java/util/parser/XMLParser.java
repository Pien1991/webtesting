package util.parser;


import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by workstation47 on 12/5/16.
 */
public class XMLParser {


    private XMLParser() {

    }



    private static Document read(File file) throws DocumentException {
        SAXReader reader = new SAXReader();
        Document doc = reader.read(file);
        return doc;
    }

    public static HashMap<String,Element> getXMLMap(File file) throws DocumentException {
        HashMap<String,Element> map = new HashMap();
        ArrayList<Element> tempAL =new ArrayList<>();
        treeWalk(map,tempAL,read(file).getRootElement());

        return map;
    }





    private static void treeWalk(HashMap<String,Element> map,ArrayList<Element> tempAL,final Element element){

        map.put(element.getName(),element);
        tempAL.addAll(element.elements());
        if (tempAL.size()==0){
            return;
        }else {
            Element subElement = tempAL.remove(0);
            treeWalk(map,tempAL,subElement);
        }

    }


}


