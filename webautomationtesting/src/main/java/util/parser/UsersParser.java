package util.parser;


import com.opencsv.CSVReader;
import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.HeaderColumnNameMappingStrategy;
import config.TestEnv;
import config.Site;
import dataTemplate.User;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by workstation47 on 11/25/16.
 */
public class UsersParser {

    private static String cp = "/dataSet/users/users.csv";

    private static List<User> users ;

    private static UsersParser instance = new UsersParser();

    static  {
        String filePath = UsersParser.class.getResource(cp).getFile();
        CSVReader reader ;
        try {
            //Note the default separator is ","
            reader = new CSVReader(new FileReader(filePath));
            users = readUsers(reader);

        } catch (FileNotFoundException e) {
            throw new NullPointerException("Cannot find the csv file hence cannot initialize the reader");
        }
    }

    private UsersParser() {
    }

    private static List<User> readUsers(CSVReader reader){

        HeaderColumnNameMappingStrategy strategy = new HeaderColumnNameMappingStrategy();
        strategy.setType(User.class);


        CsvToBean csv = new CsvToBean();

        return csv.parse(strategy,reader);
    }

    public static List<User> getUserList(){
        return users;
    }

    public static  User getUserByID(String id){
        List<User> trackUser = new ArrayList<User>();

        for (User user : users) {
            if (user.getId().equals(id)){
                trackUser.add(user);
            }
        }

        if (trackUser.size()>1){
            throw new IllegalArgumentException("More than one user have same id : "+id);
        }else {
            return trackUser.get(0);
        }


    }

    public static  User getUserByAlias(String Alias, TestEnv testEnv, Site site){
        List<User> trackUser = new ArrayList<User>();
        String serverName = testEnv.toString();
        String siteName = site.toString();
        for (User user : users) {

            if (user.getAlias()!=null&&user.getTestEnv()!=null&&user.getSite()!=null){
                if (user.getAlias().equals(Alias)&& user.getTestEnv().equals(serverName) && user.getSite().equals(siteName) )
                trackUser.add(user);
            }
        }

        if (trackUser.size()>1){
            throw new IllegalArgumentException("More than one user have same Alias and same testEnv  where Alias : "+Alias+ " on testEnv = "+ testEnv +" and site = "+siteName);
        }else if (trackUser.size()==0){
            throw new IllegalArgumentException("No user with the Alias : "+Alias+ " on testEnv = "+ testEnv +" and site = "+siteName) ;
        }else {
            return trackUser.get(0);
        }

    }

    public static  User getUserByUsername(String username , TestEnv testEnv, Site site){
        List<User> trackUser = new ArrayList<User>();
        String serverName = testEnv.toString();
        String siteName = site.toString();



        for (User user : users) {
            if (user.getUsername()!=null&&user.getTestEnv()!=null&&user.getSite()!=null){
                if (user.getUsername().equals(username)&& user.getTestEnv().equals(serverName) && user.getSite().equals(siteName) )
                    trackUser.add(user);
            }
        }


        if (trackUser.size()>1){
            throw new IllegalArgumentException("More than one user have same name and same testEnv  where username : "+username+ " on testEnv = "+ testEnv +" and site = "+siteName);
        }else if (trackUser.size()==0){
            throw new IllegalArgumentException("No user with the name : "+username+ " on testEnv = "+ testEnv +" and site = "+siteName) ;
        }else {
            return trackUser.get(0);
        }
    }

    public static  User getUserByUserId(String userId , TestEnv testEnv, Site site){
        List<User> trackUser = new ArrayList<User>();
        String serverName = testEnv.toString();
        String siteName = site.toString();
        for (User user : users) {
            if (user.getUserId()!=null&&user.getTestEnv()!=null&&user.getSite()!=null){
                if (user.getUserId().equals(userId)&& user.getTestEnv().equals(serverName) && user.getSite().equals(siteName) )
                    trackUser.add(user);
            }
        }


        if (trackUser.size()>1){
            throw new IllegalArgumentException("More than one user have same name and same testEnv  where user_id : "+userId+ " on testEnv = "+ testEnv +" and site = "+siteName);
        }else if (trackUser.size()==0){
            throw new IllegalArgumentException("No user with the user_id : "+userId+ " on testEnv = "+ testEnv +" and site = "+siteName) ;
        }else {
            return trackUser.get(0);
        }
    }

}
