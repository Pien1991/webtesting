package util.parser;

import config.LocatorType;
import org.openqa.selenium.By;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

public class LocatorsParser extends PropertiesParser {



	public LocatorsParser(String fileName) throws IOException
	{
		super(fileName);
	}

	public LocatorsParser(File file) throws IOException {
		super(file);
	}


	public String getLocatorValue(String locatorName){
		return super.getArrayValue(locatorName)[1];
	}

	public By getObjectLocator(String locatorName)
	{
		String [] locators = super.getArrayValue(locatorName);
		LocatorType locatorType = LocatorType.valueOf(locators[0].trim());
		String locatorValue = locators[1].trim();
		return LocatorType.getLocator(locatorType,locatorValue);
	}


	@Override
	public HashMap<String,By> getKeyValueMap(){
		Set<Object> element_keys = super.propertyFile.keySet();

		Iterator<Object> itr = element_keys.iterator();
		HashMap<String,By> map = new HashMap<String, By>();

		while (itr.hasNext()){
			String key  = (String)itr.next();
			map.put(key,this.getObjectLocator(key));
		}
		return map;
	}



}