package util.parser;

import org.openqa.selenium.By;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.*;

/**
 * Created by bianxiangwei on 18/1/2017.
 */
public class PropertiesParser {

    protected Properties propertyFile = new Properties();
    private String separator  = ":";

    public PropertiesParser(String fileName) throws IOException
    {
        propertyFile.load(new FileInputStream(fileName));
    }

    public PropertiesParser(File file) throws IOException {
        propertyFile.load(new FileInputStream(file));
    }

    public void setSeparator(String separator1){
        this.separator = separator1 ;
    }

    protected String[] splitting(String property){
        return property.split(this.separator);
    }

    public String getProperty(String key){
       return this.propertyFile.getProperty(key);
    }

    public String[] getArrayValue(String key){
        String value = this.propertyFile.getProperty(key);
        String [] propertiesArray = this.splitting(value);
        return propertiesArray;
    }

    public HashMap getKeyValueMap(){
        Set<Object> element_keys = this.propertyFile.keySet();
        Iterator<Object> itr = element_keys.iterator();
        HashMap map = new HashMap();
        while (itr.hasNext()){
            String key  = (String)itr.next();
            map.put(key,this.getArrayValue(key));
        }
        return map;
    }


}
