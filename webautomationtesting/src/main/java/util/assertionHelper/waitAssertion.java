package util.assertionHelper;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.testng.Assert;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;

/**
 * Created by workstation47 on 11/8/16.
 */
public class waitAssertion {

    static final int INTERVAL_WAITING_TIME = 500;


    public static void assertEquals(String methodName, Object instanceThatContainsTheMethod, int interval, Object... instancesThatMethodNeedAsPara){
        assertEquals(methodName,instanceThatContainsTheMethod,null,interval,instancesThatMethodNeedAsPara);
    }

    public static void assertEquals(String methodName, Object instanceThatContainsTheMethod, int interval) throws NoSuchMethodException {
        assertEquals(methodName,instanceThatContainsTheMethod,null,interval);
    }


    public static void assertEquals(String methodName, Object instanceThatContainsTheMethod, Object expectedResult, int interval, Object... instancesThatMethodNeedAsPara)  {
        // For methods with instancesThatMethodNeedAsPara
        Method  method = getMethod(methodName,instanceThatContainsTheMethod,instancesThatMethodNeedAsPara);
            //For a certain time , waiting the assertion become true
        Object ActualResult = null;

        for (int i = 0; i < interval; i++) {

            try {
                if (expectedResult!=null) {
                    ActualResult = method.invoke(instanceThatContainsTheMethod, instancesThatMethodNeedAsPara);
                }else {
                    method.invoke(instanceThatContainsTheMethod, instancesThatMethodNeedAsPara);
                    return;
                }
            } catch (IllegalAccessException e) {
            } catch (InvocationTargetException e) {
            }
            if (expectedResult!=null){
                try {
                    Assert.assertEquals(ActualResult, expectedResult);
                    return;
                } catch (AssertionError e) {

                }
            }
            try {
                Thread.sleep(INTERVAL_WAITING_TIME);
            } catch (InterruptedException e1) {
                e1.printStackTrace();
            }


        }

        Assert.assertEquals(ActualResult, expectedResult);

    }




    public static  void assertEquals(String methodName, Object instanceThatContainsTheMethod, Object expectedResult, int interval)  {
        // For methods without instancesThatMethodNeedAsPara
        assertEquals(methodName, instanceThatContainsTheMethod, expectedResult, interval, (Class<?>[])null);
    }


    public static void assertFail(String methodName, Object instanceThatContainsTheMethod, int interval)  {
        assertFail(methodName, instanceThatContainsTheMethod, interval, (Class<?>[])null);
    }

    public static void assertFail(String methodName, Object instanceThatContainsTheMethod, int interval, Object... instancesThatMethodNeedAsPara)  {
        // For methods with instancesThatMethodNeedAsPara
        Method  method = getMethod(methodName,instanceThatContainsTheMethod,instancesThatMethodNeedAsPara);


        //For a certain time , waiting the assertion become true
        for (int i = 0; i < interval; i++) {

            try {
                method.invoke(instanceThatContainsTheMethod,instancesThatMethodNeedAsPara);
            } catch (IllegalAccessException e) {
            } catch (InvocationTargetException e) {
                if (e.getCause() instanceof WebDriverException){
                    return;
                }
            }
            try {
                Thread.sleep(INTERVAL_WAITING_TIME);
            } catch (InterruptedException e1) {
            }

        }
        Assert.fail("The method is not failed =>"+methodName+" ,for instanceThatContainsTheMethod => "+instanceThatContainsTheMethod +" and with paras => "+ (instancesThatMethodNeedAsPara==null?"No parameters": Arrays.toString(instancesThatMethodNeedAsPara)));

    }


    private static Method getMethod(String methodName, Object instanceThatContainsTheMethod, Object... instancesThatMethodNeedAsPara)  {
        Method method = null;

        if (instancesThatMethodNeedAsPara==null){
            try {
                method = instanceThatContainsTheMethod.getClass().getMethod(methodName,null);
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            }

        }else {

            Class[] classes = new Class[instancesThatMethodNeedAsPara.length];
            for (int i = 0; i < instancesThatMethodNeedAsPara.length; i++) {

                if (WebDriver.class.isAssignableFrom(instancesThatMethodNeedAsPara[i].getClass())) {
                    classes[i] = WebDriver.class;
                } else {
                    classes[i] = instancesThatMethodNeedAsPara[i].getClass();
                }

            }
            try {
                method = instanceThatContainsTheMethod.getClass().getMethod(methodName, classes);
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            }

        }

        if (method == null) {
            throw new IllegalArgumentException("No such method : " + methodName);
        } else {
            return method;
        }

    }



}
