package util.databaseHelper;

import com.jcraft.jsch.*;
import config.TestEnv;
import util.parser.FileLoader;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.BindException;
import java.util.Properties;

/**
 * Created by workstation47 on 10/18/16.
 */
public class ConnectServer {
    // Account
    private static String user ;

    // Host' IP , only for OAT for now
    private static String host ;

    private static int host_port ;

    //Only for this environment
    private static  String privateKeyPath;

    private static Session session = null;

    private static Channel channel = null ;


    static {

        Properties propertyFile = new Properties();
        try {
            propertyFile.load(new FileInputStream(FileLoader.getFile("/sshConfig/connectServerConfig.properties")));
        } catch (IOException e) {
            e.printStackTrace();
        }

        user = propertyFile.getProperty("user");
        host = TestEnv.getIP(propertyFile.getProperty("host"));
        host_port = Integer.parseInt(propertyFile.getProperty("host_port"));
        privateKeyPath=propertyFile.getProperty("privateKeyPath");

        JSch jsch = new JSch();

        try {
            jsch.addIdentity(privateKeyPath);
            session = jsch.getSession(user, host, host_port);
        } catch (JSchException e) {
            e.printStackTrace();
        }

        Properties config = new java.util.Properties();
        config.put("StrictHostKeyChecking", "no");
        session.setConfig(config);
    }

    public static Session getSession() {
        return session;
    }

    public static void sshConnect() throws JSchException {

        session.connect();
    }

    public static void sshDisconnect(){
        System.out.println("In sshDisconnect");
        session.disconnect();
        System.out.println("Finish sshDisconnect");

    }


    public static void setLocalPortForward(int lpost, String rhost , int rport) {
        int assinged_port = 0;
        try {
            assinged_port = session.setPortForwardingL(lpost, rhost, rport);
            System.out.println("localhost:"+assinged_port+" -> "+rhost+":"+rport);

        } catch (JSchException e) {
            if (e.getCause() instanceof BindException){
                // The local port forwarding could be already set up.
                System.out.println("The Local Port Forward has already been set up");

            }else {
                e.printStackTrace();
            }
        }
    }
    public static void delLocalPortForward(int lpost ) {
        try {
            session.delPortForwardingL(lpost);
            System.out.println("localhost "+lpost+" 's port forward now is closed");
        } catch (JSchException e) {
            e.printStackTrace();
        }


    }


    public static void executeCommand(String command) throws JSchException{

        if (!session.isConnected()){
            System.out.println("*** Failed to connect host ***");
        }else {
            try {
                channel = session.openChannel("exec");
                ((ChannelExec) channel).setCommand(command);
                channel.connect();

            } catch (JSchException e) {
                System.out.println("*** Failed to open tunnel ***");
            }

            try (BufferedReader br = new BufferedReader(new InputStreamReader(channel.getInputStream()))) {
                String line;
                while ((line = br.readLine()) != null) {
                    System.out.printf("%s%n", line);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                channel.disconnect();
            }catch (Exception e){
                e.printStackTrace();
            }
        }



    }



    public static void main(String[] args) throws IOException, JSchException {
        int lport = 2222;
        int rport = 3306;
        String rhost = "dbserver";
        sshConnect();
        setLocalPortForward(lport,rhost,rport);
        executeCommand("ifconfig");
        delLocalPortForward(lport);
        sshDisconnect();
    }

}
