package util.databaseHelper;


import com.jcraft.jsch.JSchException;

import java.sql.*;

/**
 * Created by workstation47 on 10/18/16.
 */
public class ConnectDatabase {

    private static final String  JDBC_DRIVER = "com.mysql.jdbc.Driver";
    private static int lport = 2222;
    private static int rport = 3306;
    private static String rhost = "dbserver";

    private static final String dbUrl = "jdbc:mysql://localhost:" + lport  + "/dragoncoin";
    private static final String dbUser = "root";
    private static final String dbPassword = "";


    private static Connection connection = null;

    private ConnectDatabase(){
        try {
            Class.forName(JDBC_DRIVER);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static Connection getConnection() {
        return connection;
    }

    public static synchronized void connect() {
        try {
            connection = DriverManager.getConnection(dbUrl,dbUser,dbPassword);
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }



    public static synchronized int executeUpdate(String sql)  {



        Statement stmt = null;
        int impactedRow = 0;
        try {
            stmt = connection.createStatement();
            impactedRow = stmt.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return impactedRow;
    }

    public static synchronized ResultSet executeQuery(String sql)  {


        Statement stmt = null;
        ResultSet resultSet = null;
        try {
            stmt = connection.createStatement();
            resultSet = stmt.executeQuery(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            return resultSet;
        }

    }



    public static synchronized void disconnect(){
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (NullPointerException e){
            // If user didn't link to  database. The connection is null.
        }
    }


    public static void main(String[] args) throws SQLException {
        try{
            ConnectServer.sshConnect();
            ConnectServer.setLocalPortForward(lport,rhost,rport);
            connect();
            ResultSet resultSet=executeQuery("select * from user where id=4108");
            while (resultSet.next()){
                System.out.println(resultSet.getString("username"));
            }
        } catch (JSchException e) {
            e.printStackTrace();
        } finally {
            disconnect();
            ConnectServer.sshDisconnect();
        }
    }
}
