package util;

import config.DriverType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import util.parser.FileLoader;
import util.validation.OSValidator;

/**
 * Created by shepardpin on 4/20/17.
 */
public class BrowserFactory {

    public  static WebDriver createBrowser(DriverType type){

        String driverPath = "/driver/" + OSValidator.getOS();

        switch (type) {
            case FIREFOX:
                System.setProperty("webdriver.gecko.driver", FileLoader.getAbsolutePath(driverPath + "/geckodriver"));
                DesiredCapabilities capabilities = DesiredCapabilities.firefox();
                return new FirefoxDriver(capabilities);

            case CHROME:
                System.setProperty("webdriver.chrome.driver", FileLoader.getAbsolutePath(driverPath + "/chromedriver"));
                return new ChromeDriver();

            default:
                throw new NullPointerException("Do not support this driver : " + type);
        }
    }
}
