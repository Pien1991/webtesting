package util;

import br.eti.kinoshita.testlinkjavaapi.constants.ExecutionStatus;
import br.eti.kinoshita.testlinkjavaapi.constants.ExecutionType;
import br.eti.kinoshita.testlinkjavaapi.constants.TestCaseDetails;
import br.eti.kinoshita.testlinkjavaapi.model.*;
import managers.TestLinkApiManager;
import org.testng.ITestResult;




/**
 * Created by shepardpin on 4/11/17.
 */
public class TestLinkHelper {


    private static TestCase[] testCases ;
    private static TestProject testProject ;
    private static TestPlan testPlan ;


    /**
     *
     * @param iTestResult
     * @return Boolean
     *
     *  The function will check the validation of test case in test suite :
     *  1. Whether the test case inside automated test plan
     *  2. Whether the test case platform is supported
     *
     */
    public static boolean isTestCaseValid(ITestResult iTestResult ){

        if (getTestCase(iTestResult)!=null){
            return true;
        }else {
            return false;
        }
    }


    public static TestCase getTestCase(ITestResult iTestResult){

        String testCaseId = getTestCaseId(iTestResult.getTestClass().getName());

        String browserName = TestngConfigurationHelper.getBrowserName(iTestResult.getTestContext());

        // A cache for test cases array
        if (testCases == null){
            testCases = getTestCasesInsideTestPlan(iTestResult, ExecutionType.AUTOMATED);
        }

        //TODO : Can be improved !
        for (int i = 0; i < testCases.length; i++) {
            TestCase testCase = testCases[i];
            String selectedTestCaseId =  testCase.getFullExternalId();
            if (selectedTestCaseId.equals(testCaseId)){
                if (isTestPlatformSupport(testCase,browserName)){
                    return testCase ;
                }
            }
        }

        return null ;
    }




    public static void setTestCaseResult(ITestResult iTestResult,ExecutionStatus executionStatus){
        TestCase selectedTestCase =  getTestCase(iTestResult);
        Build build = getLatestBuild(iTestResult);
        TestPlan testPlan = getTestPlan(iTestResult);
        Platform platform = selectedTestCase.getPlatform();
        TestLinkApiManager.getAPI().setTestCaseExecutionResult(selectedTestCase.getId(),selectedTestCase.getInternalId(),testPlan.getId(), executionStatus,build.getId(),null,null,null,null,platform.getId(),null,null,false);
    }


    public  static Build getLatestBuild(ITestResult iTestResult){
        TestPlan testPlan = getTestPlan(iTestResult);
        return  TestLinkApiManager.getAPI().getLatestBuildForTestPlan(testPlan.getId());
    }

    public static TestProject getTestProject (ITestResult iTestResult){

        String testProjectName = TestngConfigurationHelper.getTestProjectName(iTestResult);

        if(testProjectName==null){
            throw new IllegalArgumentException(" The testProjectName should NOT be null , cannot get test project name");
        }

        if (testProject==null){
            testProject = TestLinkApiManager.getAPI().getTestProjectByName(testProjectName);;
        }

        return  testProject;
    }

    public  static TestPlan getTestPlan (ITestResult iTestResult){

        String testPlanName = TestngConfigurationHelper.getTestPlanName(iTestResult);
        String testProjectName = TestngConfigurationHelper.getTestProjectName(iTestResult);

        if (testPlanName==null  ){
            throw new IllegalArgumentException(" The testPlanName should NOT be null , cannot get test plan name");
        }

        if (testPlan==null){
            testPlan = TestLinkApiManager.getAPI().getTestPlanByName(testPlanName,testProjectName);;
        }

        return testPlan ;
    }

    public static String getTestCaseId(Class Class)  {
        return  getTestCaseId(Class.getSimpleName());
    }

    private static TestCase[] getTestCasesInsideTestPlan(ITestResult iTestResult, ExecutionType executionType){

        if (testPlan==null){
            testPlan =  getTestPlan(iTestResult);
        }
        return TestLinkApiManager.getAPI().getTestCasesForTestPlan(testPlan.getId(),null,null,null,null,null,null,null,executionType,false, TestCaseDetails.FULL );

    }

    private static boolean isTestPlatformSupport(TestCase testCase , String browserName){
        if (testCase.getPlatform().getName().toUpperCase().equals(browserName)){
            return true ;
        }
        return false ;
    }

    private static String getTestCaseId(String className)  {
        if (className==null){
            throw new IllegalArgumentException(" The classname format should NOT be null , cannot get test case name");
        }
        String [] strings = className.split("\\.");
        int last_element_number = strings.length-1;
        return testCaseIdTransform(strings[last_element_number]) ;
    }

    private static String testCaseIdTransform(String testCaseName)  {
        return testCaseName.replaceFirst("_","-");
    }




}
