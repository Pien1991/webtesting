package util.configsHelper;

import com.aventstack.extentreports.Status;
import config.TestEnv;
import config.Site;
import managers.ExtentManager;
import org.openqa.selenium.WebDriver;
import actions.ProfileActions;
import dataTemplate.User;
import util.navigationHelper.NavigationHelper;

/**
 * Created by workstation47 on 11/1/16.
 */
public class ConfigHelper {

    public static synchronized void loginBeforeTestStart(WebDriver driver, User user, Site site, TestEnv testEnv){

        //Assign website as category to test label
        //ExtentManager.getClassExtent().assignCategory(site.toString());


        if (!Site.isSameSite(site,driver)){
            // The website should delete ALL cookies to avoid any side-effect from last test case execution.
            driver.manage().deleteAllCookies();
            NavigationHelper.login(driver, testEnv,site,user);

        }else{
            // To check whether the user is in login state .
            if (!ProfileActions.isLogin(driver)){
                NavigationHelper.login(driver, testEnv,site,user);
            }else{
                // If user login already in last test case and the site is SAME , user may still remain login state for latest test case execution
                // Hence , another user cannot user and the test case execution will be marked as failed.
                // Therefore, user should log out first .
                ProfileActions.logout(driver);
                NavigationHelper.login(driver, testEnv,site,user);

            }

        }

        //Log when user function success
        ExtentManager.log(Status.INFO,"Login success");
    }

    public static synchronized void logoutBeforeTestStart(WebDriver driver) {


        // To check whether the user is in login state .
        if (ProfileActions.isLogin(driver)) {
            ProfileActions.logout(driver);
        }

        //Log when user function success
        ExtentManager.log(Status.INFO,"Logout success");

    }


}
