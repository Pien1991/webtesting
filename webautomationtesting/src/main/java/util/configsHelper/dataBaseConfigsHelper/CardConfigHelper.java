package util.configsHelper.dataBaseConfigsHelper;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by workstation47 on 12/30/16.
 */
public class CardConfigHelper {

    private static String getResult(Connection connection , String internal_name , String columnLabel,String tableName){
        String state = "";
        String sql  = "SELECT "+columnLabel+" FROM "+tableName+" WHERE internal_name=?";
        try {
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setNString(1,internal_name);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()){
                state = rs.getString(columnLabel);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return state;
    }

    public static String getOrderCost(Connection connection , String internal_name )  {
        String columnLabel = "order_cost";
        String tableName= "card_type";
        return getResult(connection,internal_name,columnLabel,tableName);
    }

    public static String getOrderCurrency(Connection connection , String internal_name )  {
        String columnLabel = "order_currency";
        String tableName= "card_type";
        return getResult(connection,internal_name,columnLabel,tableName);
    }

    public static String getBaseCurrency(Connection connection , String internal_name )  {
        String columnLabel = "base_currency";
        String tableName= "card_type";
        return getResult(connection,internal_name,columnLabel,tableName);
    }

    public static String getShipCost(Connection connection , String internal_name )  {
        String columnLabel = "per_shipping_charge";
        String tableName= "shipping_type";
        return getResult(connection,internal_name,columnLabel,tableName);
    }

    public static String getShipCostCurrency(Connection connection , String internal_name )  {
        String columnLabel = "charged_in_currency";
        String tableName= "shipping_type";
        return getResult(connection,internal_name,columnLabel,tableName);
    }


}
