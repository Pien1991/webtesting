package util.configsHelper.dataBaseConfigsHelper;

import java.sql.*;

/**
 * Created by workstation47 on 12/30/16.
 */
public class UserConfigHelper {

    private static String getResult(Connection connection ,int user_id , String columnLabel){
        String state = "";
        String sql  = "SELECT "+columnLabel+" FROM user WHERE id=?";
        try {
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setInt(1,user_id);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()){
                state = rs.getString(columnLabel);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return state;
    }




    public static String getUserVerificationState(Connection connection , int user_id)  {
        String columnLabel = "verification_state";
        return getResult(connection,user_id,columnLabel);
    }

    public static boolean doesUserHaveCard(Connection connection , int user_id)  {
        String columnLabel = "id";
        String sql  = "SELECT "+columnLabel+" FROM card WHERE user_id=?";
        try {
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setInt(1,user_id);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()){
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static boolean isBetaUser(Connection connection , int user_id)  {
        String columnLabel = "beta_features";
        String rs = getResult(connection,user_id,columnLabel);
        if (rs.equals("1")){
            return true;
        }else {
            return false;
        }

    }

}
