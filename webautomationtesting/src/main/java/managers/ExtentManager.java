package managers;

import com.aventstack.extentreports.*;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.model.ExceptionInfo;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.ChartLocation;
import com.aventstack.extentreports.reporter.configuration.Theme;
import com.aventstack.extentreports.utils.ExceptionUtil;
import dataTemplate.ModifiedExtentXReporter;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.http.conn.HttpHostConnectException;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;


/**
 * @author : Shepard Pin
 * @since : 1/27/17.
 */
public class ExtentManager {

    private static ExtentReports extent;
    private static ThreadLocal<ExtentTest> classExtent = new ThreadLocal();
    private static ThreadLocal<ExtentTest> methodExtent = new ThreadLocal();

    private static int verboseLevel = 10;

    //TODO : NEED TO CONSIDER ISSUE UNDER MULTI HOST
    private static String reportFolderPath = System.getProperty("user.dir")+File.separator+"extentReport";

    private ExtentManager() {
    }

    public static synchronized ExtentReports getExtentReportsInstance() {

        if (extent == null) createInstance();
        return extent;
    }

    private static ExtentReports createInstance() {
        extent = new ExtentReports();
        resetStatusHierarchy();
        initHtmlReporter();
        return extent;
    }

    public static String getReportFolderPath(){
        return reportFolderPath;
    }

    public static synchronized ExtentTest getClassExtent(){

        // To avoid class extent equal to null , it should be init .

        if (classExtent.get() ==null){

            ExtentTest classNode = ExtentManager.getExtentReportsInstance().createTest("Default","");

            setClassExtent(classNode);
        }
        return classExtent.get();
    }

    public static synchronized void setClassExtent( ExtentTest test){
        classExtent.set(test);
    }

    public static synchronized void setMethodExtent( ExtentTest test){
        methodExtent.set(test);
    }

    public static synchronized ExtentTest getMethodExtent(){

        // To avoid method extent equal to null , it should be init .
        if (methodExtent.get() ==null){

            ExtentTest methodNode = ExtentManager.getExtentReportsInstance().createTest("Default","");

            setMethodExtent(methodNode);
        }

        return methodExtent.get();
    }

    public static synchronized void setVerboseLevel(int verbose){
        verboseLevel = verbose;
    }

    public static synchronized void log(Status status,String log){

        ExtentTest test = getMethodExtent();
        if (test==null){
            return;
        }
        ExtentColor color = colorSelector(status);
        // The brief log , skip warning & info & debug
        switch (status) {
            case PASS:
                test.log(status, MarkupHelper.createLabel(log, color));
                break;
            case SKIP:
                test.log(status, MarkupHelper.createLabel(log, color));
                break;
            case FATAL:
                test.log(status, MarkupHelper.createLabel(log, color));
                break;
            case ERROR:
                test.log(status, MarkupHelper.createLabel(log, color));
                break;
            case FAIL:
                test.log(status, MarkupHelper.createLabel(log, color));
                break;
            case WARNING:
                if (verboseLevel>=2) test.log(status,MarkupHelper.createLabel(log,color));
                break;
            case INFO:
                if (verboseLevel>=2) test.log(status,MarkupHelper.createLabel(log,color));
                break;
             case DEBUG:
                 if (verboseLevel>7) test.log(status, MarkupHelper.createLabel(log, color));
                 break;
            default:
                break;
            }

    }

    private static ExtentColor colorSelector(Status status){
        switch (status){
            case INFO   :   return ExtentColor.CYAN;
            case DEBUG  :   return ExtentColor.TEAL;
            case WARNING:   return ExtentColor.AMBER;
            case PASS   :   return ExtentColor.GREEN;
            case SKIP   :   return ExtentColor.GREY;
            case FATAL  :   return ExtentColor.PURPLE;
            case ERROR  :   return ExtentColor.ORANGE;
            case FAIL   :   return ExtentColor.RED;
            default     :   return ExtentColor.WHITE;
        }
    }

    public static synchronized void log(Status status,Throwable throwable){
        log(status,throwable,null,getMethodExtent());
    }

    public static synchronized void log(Status status,Throwable throwable,File screenShotFile){
        log(status,throwable,screenShotFile,getMethodExtent());
    }

    public static synchronized void log(Status status,Throwable throwable,File screenShotFile,ExtentTest node){

        //If there is throwable , no matter the status it should be recorded.

        ExtentTest test = node;

        if (test==null){
            return;
        }

        test.log(status,MarkupHelper.createCodeBlock(throwable.getMessage()));
        if (screenShotFile!=null){
            try {
                String screenShotPath = screenShotFile.getPath();
                String lastOne = "."+File.separator+screenShotPath.substring(screenShotPath.lastIndexOf(File.separator)+1);
                test.addScreenCaptureFromPath(lastOne);
            } catch (IOException e) {
                ExtentManager.log(Status.WARNING,"In Adding Screen Shot to Report : "+e.getMessage());
            }
        }

        ExceptionInfo exceptionInfo =new ExceptionInfo();
        exceptionInfo.setException(throwable);
        exceptionInfo.setExceptionName(ExceptionUtil.getExceptionHeadline(throwable));
        //TODO : According to <pre> element , improvement can be added.
        exceptionInfo.setStackTrace("<The root cause message :> \n"+ ExceptionUtils.getRootCauseMessage(throwable));
        test.getModel().setExceptionInfo(exceptionInfo);
    }

    private static  boolean initHtmlReporter() {
        File file = new File(reportFolderPath);
        if (!file.exists()) {
            if (file.mkdir()) {
                System.out.println("Success to create directory in : "+ reportFolderPath);

            } else {
                System.out.println("Failed to create directory in : "+ reportFolderPath);
                return false;
            }
        }

        ExtentHtmlReporter htmlReporter= new ExtentHtmlReporter("extentReport/extent.html");
        htmlReporter.config().setTestViewChartLocation(ChartLocation.BOTTOM);
        htmlReporter.config().setChartVisibilityOnOpen(true);
        htmlReporter.config().setReportName("Test Report");
        htmlReporter.config().setTheme(Theme.STANDARD);
        htmlReporter.config().setDocumentTitle("Automation Test Report");
        htmlReporter.config().setEncoding("utf-8");
        extent.attachReporter(htmlReporter);
        return true;
    }

    public static  boolean initXReporter(String ProjectName ,String ReportName) {
        ModifiedExtentXReporter extentx =  new ModifiedExtentXReporter("localhost") ;

        //Set project name
        extentx.config().setProjectName(ProjectName);

        // report or build name
        extentx.config().setReportName(ReportName);

        // server URL
        // Must provide this to be able to upload snapshots !!!
        extentx.config().setServerUrl("http://localhost:1337");
        //Attach report to reportExtent
        extent.attachReporter(extentx);

        return true;

    }

    private static void resetStatusHierarchy(){
        extent.config().statusConfigurator().setStatusHierarchy(
                Arrays.asList(
                        Status.FATAL,
                        Status.FAIL,
                        Status.ERROR,
                        Status.SKIP,
                        Status.PASS,
                        Status.WARNING,
                        Status.INFO,
                        Status.DEBUG
                )
        );
    }





}
