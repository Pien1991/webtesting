package managers;

import br.eti.kinoshita.testlinkjavaapi.TestLinkAPI;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by bianxiangwei on 2/4/2017.
 */
public class TestLinkApiManager {

    //Shepard 's  key
    private static final String DEV_KEY =  "c528142a1dd1c635ac6471053acc8cee";

    private static URL SERVER_URL ;

    private static TestLinkAPI api;




    private TestLinkApiManager() {
    }

    public static synchronized TestLinkAPI getAPI() {
        if (api == null) createInstance();
        return api;
    }

    private static TestLinkAPI createInstance() {
        try {
            SERVER_URL = new URL("http://testlink.anxintl.com/lib/api/xmlrpc/v1/xmlrpc.php");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        api = new TestLinkAPI(SERVER_URL,DEV_KEY);
        return api;
    }





}
