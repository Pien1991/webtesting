package managers;

import config.DriverType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import util.BrowserFactory;
import util.driverHelper.EventHandler;
import util.parser.FileLoader;
import util.validation.OSValidator;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by bianxiangwei on 3/2/2017.
 */
public class DriverManager {


    private static ThreadLocal<Map<DriverType,WebDriver>> ThreadDriver = new ThreadLocal<>();


    public static WebDriver getDriver(DriverType type) {
        if (ThreadDriver.get()==null){
            ThreadDriver.set(new HashMap<>());
        }
        return ThreadDriver.get().get(type);
    }

    public static synchronized void setThreadDriver(DriverType type) {


        WebDriver driver = null;

        if (ThreadDriver.get()==null){
            ThreadDriver.set(new HashMap<>());
        }


        driver = BrowserFactory.createBrowser(type);

        // Add event handler.
        EventFiringWebDriver eventDriver = new EventFiringWebDriver(driver);
        eventDriver.register(new EventHandler());
        ThreadDriver.get().put(type,eventDriver);
    }

}

