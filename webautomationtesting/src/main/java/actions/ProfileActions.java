package actions;

import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;
import remoteObject.globalComponents.Header;
import dataTemplate.User;

/**
 * Created by workstation47 on 11/1/16.
 */
public class ProfileActions {

    private static Header Header = new Header();

    public static String getUsernameInProfile(WebDriver driver){

        String name ;

        if (!Header.isProfileListOpen(driver)) {
            Header.getElement(driver,"Profile_Button").click();
            name = Header.getElement(driver,"Username_Text").getText();
            Header.getElement(driver,"Profile_Button").click();
        }else {
            name = Header.getElement(driver,"Username_Text").getText();
        }


        return name;
    }

    public static void logout(WebDriver driver) {

        if (!Header.isProfileListOpen(driver)) {
            Header.getElement(driver,"Profile_Button").click();
        }

        Header.getElement(driver,"Logout_Button").click();
    }

    public static boolean isSameUser(WebDriver driver , User user){
        if (getUsernameInProfile(driver).equals(user.getUsername())){
            return true;
        }else {
            return false;
        }
    }

    public static  boolean isLogin(WebDriver driver) {
        Cookie cookie =  driver.manage().getCookieNamed("isLoggedIn");

        if (cookie!=null && cookie.getValue().contains("true")){
            return true;
        }else {
            return false;
        }
    }

}
