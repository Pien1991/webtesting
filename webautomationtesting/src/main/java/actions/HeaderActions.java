package actions;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;
import remoteObject.pages.card.CardPage;
import remoteObject.pages.portfolio.PortfolioPage;
import remoteObject.globalComponents.Header;
import util.conditionsHelper.EnhancedExpectedConditions;

/**
 * Created by workstation47 on 10/19/16.
 */
public class HeaderActions {

    private static Header header = new Header();

    private static final int TIME_FOR_HEADER_BUTTON= 4;

    public static void clickPortfolioButton(WebDriver driver){
        String el_key = "Portfolio_Link";
        WebElement element = header.getElement(driver,el_key, EnhancedExpectedConditions.presenceOfElementLocated());
        clickUntilSuccess(driver,element);

    }

    public static synchronized void clickActivityButton(WebDriver driver){
        String el_key = "Activity_Link";
        WebElement element = header.getElement(driver,el_key, EnhancedExpectedConditions.presenceOfElementLocated());
        clickUntilSuccess(driver,element);

    }

    public static synchronized void clickFundsButton(WebDriver driver)  {
        String el_key = "Funds_Link";
        WebElement element = header.getElement(driver,el_key, EnhancedExpectedConditions.presenceOfElementLocated());
        clickUntilSuccess(driver,element);

    }

    public static synchronized void clickDebitCardButton(WebDriver driver){
        String el_key = "DebitCard_Link";
        WebElement element = header.getElement(driver,el_key, EnhancedExpectedConditions.presenceOfElementLocated());
        clickUntilSuccess(driver,element);

    }

    public static void clickTradeButton(WebDriver driver){
        String el_key = "Trade_Link";
        WebElement element = header.getElement(driver,el_key, EnhancedExpectedConditions.presenceOfElementLocated());
        clickUntilSuccess(driver,element);

    }


    public static synchronized void ClickLoginButton(WebDriver driver)  {
        String el_key = "Login_Link";
        WebElement element = header.getElement(driver,el_key, EnhancedExpectedConditions.presenceOfElementLocated());
        clickUntilSuccess(driver,element);

    }

    public static synchronized void ClickRegisterButton(WebDriver driver)  {
        String el_key = "Register_Link";
        WebElement element = header.getElement(driver,el_key, EnhancedExpectedConditions.presenceOfElementLocated());
        clickUntilSuccess(driver,element);
    }

    private static void clickUntilSuccess(WebDriver driver,WebElement element){
        new WebDriverWait(driver,TIME_FOR_HEADER_BUTTON).
                until(EnhancedExpectedConditions.clickElementUntilSuccess(element));
    }
}
