package actions;

import config.Language;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import remoteObject.globalComponents.Header;

/**
 * Created by workstation47 on 10/19/16.
 */
public class LanguageActions {



    private static Header header = new Header();

    public static synchronized void ChangeLanguageTo(WebDriver driver, Language lg){

        WebElement language_dropList ;

        if (ProfileActions.isLogin(driver)){

            if (!header.isProfileListOpen(driver)){
                //To open the list if it is closed.
                header.getElement(driver,"Profile_Button").click();
            }

            String el_key = "Language_Button";
            header.getElement(driver,el_key).click();


            header.getLanguageChoice(driver,lg).click();

            //Closed the list to avoid cover any other irrelevant elements .
            header.getElement(driver,"Profile_Button").click();

        }else {

            //TODO
            String el_key = "BeforeLoginLanguage_DropList";
            header.getElement(driver,el_key).click();
            header.getLanguageChoice(driver,lg).click();

        }
    }




}
