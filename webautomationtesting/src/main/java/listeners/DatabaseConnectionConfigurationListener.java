package listeners;

import org.testng.IExecutionListener;
import util.databaseHelper.ConnectDatabase;
import util.databaseHelper.ConnectServer;

/**
 * Created by workstation47 on 12/30/16.
 */


public class DatabaseConnectionConfigurationListener implements IExecutionListener{
    @Override
    public void onExecutionStart() {
        try {
            ConnectDatabase.connect();
            System.out.println("Database Connected");
        }catch (Exception e){
            System.out.println("Cannot connect database, programme stopped");
            System.exit(2);
        }
    }

    @Override
    public void onExecutionFinish() {
        try {
            ConnectDatabase.disconnect();
            System.out.println("Database Disconnected");
        }catch (Exception e){
            System.out.println("Error occurred, programme stopped");
            System.exit(2);
        }
    }
}
