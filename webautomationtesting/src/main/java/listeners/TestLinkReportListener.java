package listeners;


import br.eti.kinoshita.testlinkjavaapi.constants.ExecutionStatus;
import org.testng.IConfigurationListener;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;
import util.TestLinkHelper;

/**
 * Created by shepardpin on 4/18/17.
 */
public class TestLinkReportListener implements ITestListener,IConfigurationListener {
    @Override
    public void onConfigurationSuccess(ITestResult iTestResult) {

    }

    @Override
    public void onConfigurationFailure(ITestResult iTestResult) {

        // If any configuration test is failed , we mark it as BLOCKED .
        TestLinkHelper.setTestCaseResult(iTestResult, ExecutionStatus.BLOCKED);
    }

    @Override
    public void onConfigurationSkip(ITestResult iTestResult) {

    }

    @Override
    public void onTestStart(ITestResult iTestResult) {

    }

    @Override
    public void onTestSuccess(ITestResult iTestResult) {
        TestLinkHelper.setTestCaseResult(iTestResult, ExecutionStatus.PASSED);

    }

    @Override
    public void onTestFailure(ITestResult iTestResult) {
        TestLinkHelper.setTestCaseResult(iTestResult, ExecutionStatus.FAILED);
    }

    @Override
    public void onTestSkipped(ITestResult iTestResult) {

    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult iTestResult) {

    }

    @Override
    public void onStart(ITestContext iTestContext) {

    }

    @Override
    public void onFinish(ITestContext iTestContext) {

    }
}
