package listeners;


import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import config.DriverType;
import config.SuiteConfig;
import managers.DriverManager;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.*;
import managers.ExtentManager;
import util.TestngConfigurationHelper;
import util.validation.OSValidator;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

/**
 * Created by workstation47 on 1/10/17.
 */
public class ExtentTestNGReporterListener implements ISuiteListener,IExecutionListener,IConfigurationListener2,ITestListener {
    private long configurationStartTime;
    private long testStartTime;


    private String getTestMethodName(ITestResult result) {
        return result.getMethod().getConstructorOrMethod().getName();
    }

    private String getTestDescription(ITestResult result) {
        return result.getMethod().getDescription();
    }

    private String getClassName(ITestResult result) {
        String className = result.getTestClass().getName();
        String lastOne = className.substring(className.lastIndexOf(".")+1);
        return lastOne;
    }

    private Date getTime(long millis) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(millis);
        return calendar.getTime();
    }



    @Override
    public void onExecutionStart() {




    }

    @Override
    public void onExecutionFinish() {

        // Need this!!
        for (String s : Reporter.getOutput()) {
            ExtentManager.getExtentReportsInstance().setTestRunnerOutput(s);
        }

        ExtentManager.getExtentReportsInstance().flush();
    }

    @Override
    public synchronized void  beforeConfiguration(ITestResult tr) {
        configurationStartTime = System.currentTimeMillis();
        String  name = getClassName(tr);


        if (getTestMethodName(tr).equals("WebTestConfigSetUp")) {
            return;
        }
        //Note : getModel().setDescription(description); cannot add category to database!!!!!
        //Never write something like this -> ExtentManager.getClassExtent().getModel().setDescription(description);

        // In @BeforeClass , I will write the description of the whole test class
        ExtentTest classNode = ExtentManager.getExtentReportsInstance().createTest(name,getTestDescription(tr));

        //Set start time
        classNode.getModel().setStartTime(getTime(configurationStartTime));

        //Assign Host as category to test label.
        classNode.assignCategory(tr.getHost());

        //Assign OS information as category to test label
        classNode.assignCategory(OSValidator.getOS());



        ITestContext context = tr.getTestContext();
        // Assign the test suite as category to test label.
        String TestSuiteName = context.getName();
        classNode.assignCategory(TestSuiteName);

         //Assign browser as category to test label.
        String browserName = TestngConfigurationHelper.getBrowserName(context);
        classNode.assignCategory(browserName);




        // Create A method label inside the test for configuration test;
        ExtentTest setUpMethodNode = classNode.createNode(getTestMethodName(tr));

        // Create A test label for class
        ExtentManager.setClassExtent(classNode);
        // Create A test label for setup method
        ExtentManager.setMethodExtent(setUpMethodNode);

        //Note : getModel().setCategory(category) cannot add category to database!!!!! Which is known bug in ReportX
        //Never write something like this -> ExtentManager.getClassExtent().getModel().setCategory(category);
     }


    @Override
    public synchronized void onConfigurationSuccess(ITestResult itr) {
        String methodName = getTestMethodName(itr);

        if (!methodName.equals("WebTestConfigSetUp") && itr.getMethod().isBeforeClassConfiguration() ){

            Throwable throwable = itr.getThrowable();
            if (throwable!=null){
                ExtentManager.log(Status.WARNING,throwable);
            }

            ExtentManager.log(Status.PASS,"Test Set Up Passed");
            ExtentManager.getMethodExtent().getModel().setEndTime(getTime(System.currentTimeMillis()-configurationStartTime));


        }

    }

    @Override
    public synchronized void onConfigurationFailure(ITestResult itr) {
        String methodName = getTestMethodName(itr);

        if (!methodName.equals("WebTestConfigSetUp") && itr.getMethod().isBeforeClassConfiguration() ){

            ExtentManager.log(Status.FAIL,"Test Set Up Failed");

            Throwable throwable =itr.getThrowable();

            DriverType driverType = TestngConfigurationHelper.getDriverType(itr.getTestContext());


            String screenShotName = "test_failure_on_class_"+getClassName(itr)+"_with_method_"+methodName;
            File screenShot = getScreenShot(screenShotName ,driverType);
            //Only when the media is in the top test node level , it can be displayed in Report Server
            ExtentManager.log(Status.FAIL,throwable,screenShot,ExtentManager.getClassExtent());


            ExtentManager.getMethodExtent().getModel().setEndTime(getTime(System.currentTimeMillis()-configurationStartTime));

        }

    }

    @Override
    public synchronized void onConfigurationSkip(ITestResult itr) {
        String methodName = getTestMethodName(itr);
        if (!methodName.equals("WebTestConfigSetUp") && itr.getMethod().isBeforeClassConfiguration() ){
            ExtentManager.log(Status.SKIP,"Test Set Up Skipped");
            ExtentManager.getMethodExtent().getModel().setEndTime(getTime(System.currentTimeMillis()-configurationStartTime));
        }

    }

    @Override
    public synchronized void onTestStart(ITestResult result) {
        testStartTime = System.currentTimeMillis();
        //Create test node
        ExtentTest method = ExtentManager.getClassExtent().createNode(getTestMethodName(result),getTestDescription(result));
        //Set test node per Thread
        ExtentManager.setMethodExtent(method);
        ExtentManager.getMethodExtent().getModel().setStartTime(getTime(testStartTime));
    }

    @Override
    public synchronized void onTestSuccess(ITestResult result) {
        String methodName = getTestMethodName(result);

        Throwable throwable = result.getThrowable();
        if (throwable!=null) ExtentManager.log(Status.WARNING,throwable);

        ExtentManager.log(Status.PASS,"Test Passed");
        ExtentManager.getMethodExtent().getModel().setEndTime(getTime(System.currentTimeMillis()-testStartTime));


    }

    @Override
    public synchronized void onTestFailure(ITestResult result) {
        String methodName = getTestMethodName(result);

        //Try to log the error with screenShot
        ExtentManager.log(Status.FAIL,"Test Failed");
        Throwable throwable =result.getThrowable();

        DriverType driverType = TestngConfigurationHelper.getDriverType(result.getTestContext());


        String screenShotName = "test_failure_on_class_"+getClassName(result)+"_with_method_"+methodName;
        File screenShot = getScreenShot(screenShotName ,driverType);
        //Only when the media is in the top test node level , it can be displayed in Report Server
        ExtentManager.log(Status.FAIL,throwable,screenShot,ExtentManager.getClassExtent());



        ExtentManager.getMethodExtent().getModel().setEndTime(getTime(System.currentTimeMillis()-testStartTime));


    }

    @Override
    public synchronized void onTestSkipped(ITestResult result) {
        String methodName = getTestMethodName(result);

        Throwable throwable = result.getThrowable();
        if (throwable!=null) ExtentManager.log(Status.WARNING,throwable);

        ExtentManager.log(Status.SKIP,"Test Skipped");
        ExtentManager.getMethodExtent().getModel().setEndTime(getTime(System.currentTimeMillis()-testStartTime));



    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult result) {

    }

    @Override
    public void onStart(ITestContext context) {
    }

    @Override
    public synchronized void onFinish(ITestContext context) {
    }


    @Override
    public synchronized void  onStart(ISuite suite) {



        //Set Operation System information
        ExtentManager.getExtentReportsInstance().setSystemInfo("Test Environment",suite.getParameter(SuiteConfig.TEST_ENV.toString()));

        int verboseLevel = suite.getXmlSuite().getVerbose();
        ExtentManager.getExtentReportsInstance().setSystemInfo("verboseLevel",Integer.toString(verboseLevel));
        ExtentManager.setVerboseLevel(verboseLevel);


        //Check whether need to store test report to server
        String reportConfig = suite.getParameter(SuiteConfig.REPORT_SERVER.toString());
        if (reportConfig != null && reportConfig.toLowerCase().equals("true") ){
            //TODO : NEED TO THINK THE PROJECT NAME
            //Start Recording test result to Test Report Server
            ExtentManager.initXReporter(suite.getName(),"Testing");
        }

        //TODO : WANT TO DYNAMICALLY SET TEST PARAMETERS

    }

    @Override
    public void onFinish(ISuite suite) {

    }


    private File getScreenShot(String screenShotName ,DriverType type ){
        WebDriver driver = DriverManager.getDriver(type);
        File target =null;
        if (driver !=null){
            try {
                File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
                // Need to make sure the png is unique for each test node
                target = new File(ExtentManager.getReportFolderPath()+File.separator+screenShotName+"_"+ new Random().nextInt(1000)+1+".png");
                FileUtils.copyFile(scrFile, target);
            } catch (IOException e) {
                ExtentManager.log(Status.WARNING,"In getScreenShot : "+e.getMessage());
            }
        }

        return target;
    }



}
