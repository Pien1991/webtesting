package listeners;


import org.testng.*;

/**
 * @author : Shepard Pin
 * @since : 2/6/17.
 */
public class TestLogPublisher implements ITestListener,IExecutionListener,IConfigurationListener2 {
    @Override
    public void onTestStart(ITestResult result) {

    }

    @Override
    public void onTestSuccess(ITestResult result) {
        System.out.println("For test name "+getClassName(result)+" on test method " + getTestMethodName(result) + " : Passed");
    }

    @Override
    public void onTestFailure(ITestResult result) {
        System.out.println("For test name "+getClassName(result)+" on test method " + getTestMethodName(result) + " : Failed");

    }

    @Override
    public void onTestSkipped(ITestResult result) {
        System.out.println("For test name "+getClassName(result)+" on test method " + getTestMethodName(result) + " : Skipped");
    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult result) {

    }

    @Override
    public void onStart(ITestContext context) {

    }

    @Override
    public void onFinish(ITestContext context) {

    }

    @Override
    public void onExecutionStart() {
        System.out.println("TestNG is going to start");
    }

    @Override
    public void onExecutionFinish() {
        System.out.println("TestNG has finished");
    }

    @Override
    public void beforeConfiguration(ITestResult tr) {

    }

    @Override
    public void onConfigurationSuccess(ITestResult itr) {
        System.out.println("In test "+getClassName(itr)+" -- Test configuration "+getTestMethodName(itr)+" : Passed \n ");

    }

    @Override
    public void onConfigurationFailure(ITestResult itr) {
        System.out.println("In test "+getClassName(itr)+" -- Test configuration "+getTestMethodName(itr)+" : Failed \n ");
    }

    @Override
    public void onConfigurationSkip(ITestResult itr) {
        System.out.println("In test "+getClassName(itr)+" -- Test configuration "+getTestMethodName(itr)+" : Skipped \n ");
    }

    private String getTestMethodName(ITestResult result) {
        return result.getMethod().getConstructorOrMethod().getName();
    }

    private String getClassName(ITestResult result) {
        String className = result.getTestClass().getName();
        String lastOne = className.substring(className.lastIndexOf(".")+1);
        return lastOne;
    }
}
