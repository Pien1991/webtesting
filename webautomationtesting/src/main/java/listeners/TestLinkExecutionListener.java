package listeners;


import config.DriverType;
import managers.DriverManager;
import org.testng.*;
import util.TestLinkHelper;
import util.TestngConfigurationHelper;


/**
 * Created by shepardpin on 4/13/17.
 */
public class TestLinkExecutionListener implements ITestListener {



    @Override
    public void onTestStart(ITestResult iTestResult) {


        if (!TestLinkHelper.isTestCaseValid(iTestResult)){
            // If test cases are not included in test plan , they should not be executed .



            throw  new SkipException("The test case is NOT in automated test plan or the test platform is NOT supported ");
        }


    }

    @Override
    public void onTestSuccess(ITestResult iTestResult) {


    }

    @Override
    public void onTestFailure(ITestResult iTestResult) {
    }

    @Override
    public void onTestSkipped(ITestResult iTestResult) {
    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult iTestResult) {

    }

    @Override
    public void onStart(ITestContext iTestContext) {

    }

    @Override
    public void onFinish(ITestContext iTestContext) {
        DriverType driverType = TestngConfigurationHelper.getDriverType(iTestContext);
        DriverManager.getDriver(driverType).quit();
    }


}
