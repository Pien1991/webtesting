package remoteObject.globalComponents;

import config.Language;
import dataTemplate.Component;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import util.parser.FileLoader;

/**
 * Created by workstation47 on 10/18/16.
 */
public class Header extends Component {

    private final static String cp = "/locators/Header.xml";

    public Header() {
        super(10, FileLoader.getFile(cp));
    }

    public boolean isProfileListOpen(WebDriver driver){
        return getElement(driver,"Profile_Button").getAttribute("class").contains("open");
    }

    public  WebElement getLanguageChoice(WebDriver driver, Language lg)  {

        String el_key =null;

        switch (lg){
            case ENG    :   el_key = "English_Choice";
                            break;
            case TCHI   :   el_key = "TChinese_Choice";
                            break;
            case SCHI   :   el_key = "SChinese_Choice";
                            break;
            case JP     :   el_key = "Japanese_Choice";
                            break;
        }

        return getElement(driver,el_key);

    }





}
