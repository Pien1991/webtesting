package remoteObject.globalComponents;

import dataTemplate.Component;
import util.parser.FileLoader;

/**
 * Created by shepardpin on 5/11/17.
 */
public class PushErrorMessageBox extends Component{

    private final static String cp = "/locators/PushErrorMessageBox.xml";

    public PushErrorMessageBox() {
        super(5, FileLoader.getFile(cp));
    }
}
