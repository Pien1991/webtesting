package remoteObject.pages.card;

import actions.WaitLoading;
import com.aventstack.extentreports.Status;
import config.Components;
import config.Page;
import config.Site;
import config.TestEnv;
import dataTemplate.Component;
import dataTemplate.PageObject;
import managers.ExtentManager;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ISelect;
import util.ComponentHelper;

import java.util.List;

/**
 * Created by workstation47 on 10/25/16.
 */
public class CardPage extends PageObject{


    private Component CardNavigationBar = ComponentHelper.getComponent(Components.CARD_NAVIGATION_BAR);
    private Component AliasPanel  = ComponentHelper.getComponent(Components.CARD_ALIAS_PANEL);
    private Component CardSelectionPanel  = ComponentHelper.getComponent(Components.CARD_SELECTION_PANEL);
    private Component CardDetailsPanel  = ComponentHelper.getComponent(Components.CARD_DETAIL_PANEL);
    private Component CardFundPanel  = ComponentHelper.getComponent(Components.CARD_FUNDS_PANEL);


    public CardPage(WebDriver dr, TestEnv testEnv , Site site) {
        super(dr, Page.CARD ,testEnv,site);
    }

    @Override
    protected void waitUntilLoaded() {
        WaitLoading.waitUntilSplashLoaded(super.driver);
        WaitLoading.waitUntilLocalLoadingPanelLoaded(super.driver);
    }


    public int getCardAmount(){
        try{
            WebElement cardSelection_Container = CardSelectionPanel.getElement(driver,"Cards_Container");
            By by = By.xpath(".//div[@role='button']");
            List cards = cardSelection_Container.findElements(by);
            return  cards.size();

        }catch (WebDriverException e){
            throw new  WebDriverException("Can't get amount of card - Reasons as follow :\n"+e.getMessage());
        }

    }

    public ISelect getRechargeUnitSelection(){
        return CardFundPanel.getSelect(driver,"RechargeUnit_Select");
    }

    public ISelect getRechargeAccountSelection(){
        return CardFundPanel.getSelect(driver,"ChargeAccount_Select");
    }


    public void clickMycardButton(){
        CardNavigationBar.getElement(super.driver,"CardPage_Link").click();
    }

    public void clickRequestCardsButton(){
        CardNavigationBar.getElement(super.driver,"CardRequestPage_Link").click();
    }

    public void clickDailyLimitButton(){
        CardDetailsPanel.getElement(super.driver,"DailyLimit_Button").click();
    }

    public void clickSuspendButton(){
        CardDetailsPanel.getElement(super.driver,"Suspend_Button").click();
    }

    public void clickReportLostButton(){
        CardDetailsPanel.getElement(super.driver,"ReportLost_Button").click();
    }

    public  void clickAliasButton(WebDriver driver){
        CardNavigationBar.getElement(driver,"CardName_Button").click();
    }

    public void clickFreeUpgradeButton(){
        CardDetailsPanel.getElement(super.driver,"FreeUpgrade_Button").click();
    }

    public void clickViewButton(){
        CardDetailsPanel.getElement(super.driver,"View_Button").click();
    }


    public String getCurrentCardAlias(WebDriver driver){
        JavascriptExecutor js = (JavascriptExecutor) driver ;
        String alias =  js.executeScript("return document.getElementById(\"aliasTitle\").value").toString();
        return alias;
    }

    public void enterAlias(WebDriver driver,String alias) {
        WebElement input = AliasPanel.getElement(driver,"Alias_Input");
        input.clear();
        new Actions(driver).sendKeys(input,alias).perform();
    }


    public  boolean isAliasInputPopUp(WebDriver driver){
        try{
            AliasPanel.getElement(driver,"Alias_Panel");
            return true;
        }catch(NoSuchElementException e){
            return false;
        }catch (TimeoutException e){
            return false;
        }
    }

    public  void ClickAliasConfirmButton(WebDriver driver){
        AliasPanel.getElement(driver,"Confirm_Button").click();
    }

    public void clickChargeByFiatTab(){
        CardFundPanel.getElement(super.driver,"Currency_Div").click();
    }
    public void clickChargeByAddressTab(){
        CardFundPanel.getElement(super.driver,"Address_Div").click();
    }


    public void changeAlias(WebDriver driver,String alias){
        clickAliasButton(driver);
        if (isAliasInputPopUp(driver)){
            enterAlias(driver,alias);
            ClickAliasConfirmButton(driver);
            ExtentManager.log(Status.DEBUG,"Card name has changed to : "+alias);
        }else {
            ExtentManager.log(Status.WARNING,"Alias Input do not pop up.");
        }
    }

}
