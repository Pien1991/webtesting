package remoteObject.pages.card_request;

import actions.WaitLoading;
import config.Components;
import config.Page;
import config.Site;
import config.TestEnv;
import dataTemplate.Component;
import dataTemplate.PageObject;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.support.ui.ISelect;
import dataTemplate.tables.CardOrderTable;
import util.ComponentHelper;

/**
 * Created by workstation47 on 10/31/16.
 */
public class CardRequestPage extends PageObject{

    Component requestStageBar = ComponentHelper.getComponent(Components.REQUEST_CARD_STAGE_BAR);
    Component cardChoicePanel = ComponentHelper.getComponent(Components.REQUEST_SELECT_CARD_PANEL);
    Component cardDeliveryPanel = ComponentHelper.getComponent(Components.REQUEST_CARD_DELIVERY_PANEL);
    Component cardConfirmPanel = ComponentHelper.getComponent(Components.REQUEST_CARD_CONFIRM_PANEL);


    public CardRequestPage(WebDriver dr, TestEnv testEnv , Site site) {
        super(dr, Page.CARD_REQUEST ,testEnv,site);
    }


    @Override
    protected void waitUntilLoaded() {
        WaitLoading.waitUntilSplashLoaded(super.driver);
        WaitLoading.waitUntilLocalLoadingPanelLoaded(super.driver);
    }

    public void clickSelectCardStage(){
        requestStageBar.getElement(super.driver,"SelectCard_Button").click();
    }

    public void clickDeliveryStage(){
        requestStageBar.getElement(super.driver,"Delivery_Button").click();
    }

    public void clickConfirmationStage(){
        requestStageBar.getElement(super.driver,"Confirmation_Button").click();
    }

    public ISelect getDeliveryMethodSelect(){
        return cardDeliveryPanel.getSelect(super.driver,"DeliveryMethod_Select");
    }

    public void selectCardAmount(Component card , int amount){
        card.getSelect(super.driver,"CardAmount_Select").selectByVisibleText(Integer.toString(amount));
    }

    public void selectPhysical(Component card ){
        card.getElement(super.driver,"ATMCard_Choice").click();
    }

    public void selectVirtual(Component card ){
        card.getElement(super.driver,"DigitalCard_Choice").click();
    }

    public void clickContinueButton(){
        try {
            cardDeliveryPanel.getElement(super.driver,"Continue_Button").click();
        }catch (WebDriverException e){
            cardChoicePanel.getElement(super.driver,"Continue_Button").click();
        }
    }

    public CardOrderTable getCardOrderTable(){
        return new CardOrderTable(cardConfirmPanel.getElement(super.driver,"Confirmation_Table"));
    }


    public void clickConfirmButton(){
        cardConfirmPanel.getElement(super.driver,"Confirm_Button").click();
    }

    public void clickCancelButton(){
        cardConfirmPanel.getElement(super.driver,"Cancel_Button").click();
    }


}
