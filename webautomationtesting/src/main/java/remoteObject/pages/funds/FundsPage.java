package remoteObject.pages.funds;

import actions.WaitLoading;
import config.Page;
import config.Site;
import config.TestEnv;
import org.openqa.selenium.WebDriver;
import dataTemplate.PageObject;


/**
 * Created by workstation47 on 11/3/16.
 */
public class FundsPage extends PageObject {


    public FundsPage(WebDriver dr, TestEnv testEnv , Site site) {
        super(dr, Page.FUNDS ,testEnv,site);
    }

    @Override
    protected void waitUntilLoaded() {
        WaitLoading.waitUntilSplashLoaded(driver);
        WaitLoading.waitUntilLocalLoadingPanelLoaded(driver);
    }


}
