package remoteObject.pages.front;

import actions.WaitLoading;
import config.Page;
import dataTemplate.Component;
import org.openqa.selenium.WebDriver;
import dataTemplate.PageObject;
import org.testng.TestNGException;
import util.parser.FileLoader;
import util.parser.PropertiesParser;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by workstation47 on 11/3/16.
 */
public  class FrontPage  {

    private static final Map<String, Component> componentsCache = new HashMap<>();
    private static Map componentsConfigs;
    private static final String cp = "/config/pageObject/FrontPage.properties";

    static {
        try {
            componentsConfigs = new PropertiesParser(FileLoader.getFile(cp)).getKeyValueMap();
        } catch (IOException e) {
            throw new TestNGException("Cannot Load page object properties in location : "+cp);
        }
    }




}
