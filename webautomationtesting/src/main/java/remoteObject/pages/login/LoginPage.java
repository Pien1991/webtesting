package remoteObject.pages.login;

import actions.WaitLoading;
import config.Components;
import config.Page;
import config.Site;
import config.TestEnv;
import dataTemplate.Component;
import dataTemplate.PageObject;
import dataTemplate.User;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import util.ComponentHelper;

/**
 * Created by workstation47 on 1/19/17.
 */
public class LoginPage extends PageObject {

    private int LOGIN_WAITING_TIME = 8;

    private Component LoginForm = ComponentHelper.getComponent(Components.LOGIN_FORM);



    public LoginPage(WebDriver dr, TestEnv testEnv , Site site) {
        super(dr, Page.LOGIN ,testEnv,site);
    }


    @Override
    protected void waitUntilLoaded() {
        WaitLoading.waitUntilSplashLoaded(super.driver);
    }



    public void inputEmail(String email){
        WebElement element = LoginForm.getElement(super.driver,"Email_Input");
        element.clear();
        element.sendKeys(email);
    }

    public  void inputPassword(String password){
        WebElement element = LoginForm.getElement(super.driver,"Password_Input");
        element.clear();
        element.sendKeys(password);
    }

    public  void clickLoginSubmitButton(){
        LoginForm.getElement(super.driver,"Submit_Button").click();
    }

    public  void login( User user){
        login(user.getUsername(),user.getPassword());
    }

    public  void login(String username,String password){

        inputEmail(username);
        inputPassword(password);
        clickLoginSubmitButton();

        //Assertion : make sure the user is logged in
        new WebDriverWait(super.driver,LOGIN_WAITING_TIME).until(ExpectedConditions.urlContains("user"));

        // After user , the browser will navigate to /user page . It needs at least 1 sec to stay on /user page .
        // Otherwise , the user will be log out incidentally .
        //

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
