package remoteObject.pages.explorer;


import dataTemplate.PageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;



/**
 * Created by workstation47 on 1/11/17.
 */
public class ExplorerFrontPage extends PageObject{
    public ExplorerFrontPage(WebDriver dr, String  url) {
        super(dr, url);
    }

    @Override
    protected void waitUntilLoaded() {
        WebDriverWait wait = new WebDriverWait(driver,10);
        By LoadingPanelLocator = By.id("splashscreen");
        wait.until(ExpectedConditions.presenceOfElementLocated(LoadingPanelLocator));
        wait.until(ExpectedConditions.stalenessOf(driver.findElement(LoadingPanelLocator)));
    }



}
