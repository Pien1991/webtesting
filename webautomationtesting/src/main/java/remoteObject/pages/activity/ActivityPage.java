package remoteObject.pages.activity;

import config.Page;
import dataTemplate.PageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import util.parser.FileLoader;
import util.parser.LocatorsParser;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by workstation47 on 11/8/16.
 */
public class ActivityPage  {

    private final static String cp = "/locators/activity/FundsPage.properties";

    protected static HashMap<String,By> locatorMap ;

    private static final int TIME_FOR_FUNDS_PAGE_ELEMENTS = 30 ;


    static {
        try {
            locatorMap = new LocatorsParser(FileLoader.getAbsolutePath(cp)).getKeyValueMap();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }



}
