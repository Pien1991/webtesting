package remoteObject.pages.portfolio;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import util.parser.FileLoader;
import util.parser.LocatorsParser;

import java.io.IOException;
import java.util.HashMap;

/**
 * Created by workstation47 on 12/29/16.
 */
public class PortfolioPage {


    //TODO
    private final static String cp = "/locators/portfolio/PortfolioPage.properties";

    protected static HashMap<String,By> locatorMap ;

    private static final int TIME_FOR_PORTFOLIO_PAGE_ELEMENTS = 20 ;


    static {
        try {
            locatorMap = new LocatorsParser(FileLoader.getAbsolutePath(cp)).getKeyValueMap();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }



    public   void waitLoading(WebDriver driver) {
        //TODO
        WebDriverWait wait = new WebDriverWait(driver,TIME_FOR_PORTFOLIO_PAGE_ELEMENTS);
        wait.until(ExpectedConditions.attributeContains(locatorMap.get("Loading_Panel"),"aria-hidden","true"));


    }
}
