package dataTemplate.tables;

import dataTemplate.tables.Table;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 * Created by workstation47 on 11/1/16.
 */
public class CardOrderTable extends Table {

    public final static int CANCEL_COLUMN_INDEX = 4;
    public final static int PRICE_COLUMN_INDEX = 3;
    public final static int QUANTITY_COLUMN_INDEX = 2;
    public final static int ITEM_COLUMN_INDEX = 1;



    public CardOrderTable(WebElement e) {
        super(e);

    }

    public int getTotalCardType(){
        //Should not count the Mailing Method line and Total Order Cost line
        return super.getTotalRow()-2;
    }

    public String getMailingMethodText(){
        int MailingCellRowIndex = super.getTotalRow()-1;

        return super.getCellText(MailingCellRowIndex,ITEM_COLUMN_INDEX);
    }

    public String getMailingCost(){
        int MailingCellRowIndex = super.getTotalRow()-1;

        return super.getCellText(MailingCellRowIndex,PRICE_COLUMN_INDEX);
    }

    public String getMailingQuantity(){
        int MailingCellRowIndex = super.getTotalRow()-1;

        return super.getCellText(MailingCellRowIndex,QUANTITY_COLUMN_INDEX);
    }




    public String getTotalOrderCost(){
        int TotalCostCellRowIndex = super.getTotalRow();

        return super.getCellText(TotalCostCellRowIndex,PRICE_COLUMN_INDEX);
    }

    public String getCardCost(String cardName){
        //There is title above card lines
        //Assume that sequence of card location is same as the sequence in select card page.

        return super.getCellText(cardName,PRICE_COLUMN_INDEX);
    }

    public String getCardQuantity(String cardName){
        //There is title above card lines
        //Assume that sequence of card location is same as the sequence in select card page.

        return super.getCellText(cardName,QUANTITY_COLUMN_INDEX);
    }



    public void cancelOrderedCard(String cardName){
        //There is title above card lines
        //Assume that sequence of card location is same as the sequence in select card page.
        By by = By.xpath("./i");
        super.getCell(super.getRowIndex(cardName),CANCEL_COLUMN_INDEX).findElement(by).click();
    }

    public String getItemName(String cardName){
        //There is title above card lines
        //Assume that sequence of card location is same as the sequence in select card page.

        return super.getCellText(cardName,ITEM_COLUMN_INDEX);
    }


}
