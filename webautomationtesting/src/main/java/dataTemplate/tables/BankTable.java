package dataTemplate.tables;

import dataTemplate.tables.Table;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 * Created by workstation47 on 11/7/16.
 */
public class BankTable extends Table {



    public BankTable(WebElement e) {
        super(e);
    }


    public int getTotalBanks(){
        return super.getTotalRow();
    }

    public void deleteBank(String Alias){

//        super.table.findElement(By.xpath(".//tr[./td][1]/td[6]")).findElement(By.xpath(".//button")).click();
        deleteBank(super.getRowIndex(Alias));
    }

    public void deleteBank(int rowNumber){

        super.getCell(rowNumber,super.getTotalColumn()).findElement(By.xpath(".//button")).click();

    }





}
