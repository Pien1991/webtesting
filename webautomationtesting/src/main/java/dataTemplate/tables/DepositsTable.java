package dataTemplate.tables;

import dataTemplate.tables.Table;
import org.openqa.selenium.WebElement;

/**
 * Created by workstation47 on 11/8/16.
 */
public class DepositsTable extends Table {

    public DepositsTable(WebElement e) {
        super(e);
    }

    @Override
    public int getTotalColumn() {
        return super.getTotalColumn()/2;
    }

    @Override
    public int getTotalRow() {
        // Need to ignore the filter tr
        return super.getTotalRow()-1;
    }
}
