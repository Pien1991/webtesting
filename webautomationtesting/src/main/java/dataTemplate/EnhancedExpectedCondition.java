package dataTemplate;

import com.google.common.base.Function;
import org.openqa.selenium.By;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * Created by Shepard  on 7/12/2016.
 *
 *
 */
public interface EnhancedExpectedCondition<T> extends Function<SearchContext, T > {


   Function< ? extends SearchContext , T > injectConditionFactors(By by, SearchContext ... var1);
}
