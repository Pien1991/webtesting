package dataTemplate;

import config.Site;
import config.TestEnv;
import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import util.TestLinkHelper;
import util.TestngConfigurationHelper;

/**
 * Created by bianxiangwei on 22/1/2017.
 */

public abstract class WebTestScript extends AbstractTestScript {




    protected WebDriver driver;
    protected Site site;
    protected User user;
    protected TestEnv testEnv;

    @BeforeClass(alwaysRun = true)
    public  void WebTestConfigSetUp(ITestContext context){
        this.site = TestngConfigurationHelper.getSite(context);
        this.testEnv = TestngConfigurationHelper.getTestEnv(context);
        this.driver = TestngConfigurationHelper.getDriver(context);
        this.user = TestngConfigurationHelper.getUser(context);

        //TODO : Should make driver size configurable in xml level
        this.driver.manage().window().maximize();

    }


    @BeforeClass
    public abstract void TestcasePreconditionTest();


}
