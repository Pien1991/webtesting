package dataTemplate;


import config.Page;
import config.Site;
import config.TestEnv;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.TestException;
import util.navigationHelper.NavigationHelper;
import util.parser.FileLoader;
import util.parser.XMLParser;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by bianxiangwei on 2/12/2016.
 */
public abstract class PageObject {


    protected WebDriver driver ;
    protected Page page;


    public PageObject(WebDriver dr,Page page, TestEnv testEnv , Site site){
        this.driver = dr;
        this.page = page;
        navigateTo(testEnv,site);
    }

    public PageObject(WebDriver dr, String url){
        this.driver = dr;
        this.page = page;
        this.driver.get(url);
        waitUntilLoaded();
    }


    protected void navigateTo(TestEnv testEnv, Site site){
        NavigationHelper.navigate(this.driver,testEnv,site,this.page);
        pageValidation(this.page,this.driver);
        waitUntilLoaded();
    }

    protected void pageValidation(Page page, WebDriver dr) {
        try {
            new WebDriverWait(dr, 2).until(ExpectedConditions.urlContains(page.toString()));
        } catch (TimeoutException e) {
            throw new WebDriverException("this is not in " + page.toString());
        }
    }






    protected abstract  void waitUntilLoaded();
}
