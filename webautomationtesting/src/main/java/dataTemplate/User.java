package dataTemplate;

import com.opencsv.bean.CsvBindByName;

/**
 * Created by workstation47 on 10/17/16.
 */
public class User {

    @CsvBindByName
    private String id = null;
    @CsvBindByName
    private String userName = null;
    @CsvBindByName
    private String password = "hkhk22141580";
    @CsvBindByName
    private String firstName = null;
    @CsvBindByName
    private String lastName = null;
    @CsvBindByName
    private String testEnv = null;
    @CsvBindByName
    private String alias = null;
    @CsvBindByName
    private String companyName = null;
    @CsvBindByName
    private String site = null;
    @CsvBindByName
    private String user_id = null;

    public String getId(){ return id ;}

    public String getUsername(){
        return userName;
    }

    public String getPassword(){
        return password;
    }

    public String getFirstname(){
        return firstName;
    }
    public String getLastname(){
        return lastName;
    }

    public String getFullname(){
        return firstName+" "+lastName;
    }

    public String getTestEnv(){
        return testEnv;
    }

    public String getAlias(){
        return alias;
    }

    public String getCompanyName() {return companyName;}

    public String getSite() {
        return site;
    }

    public String getUserId() {
        return user_id;
    }

    public void setUsername(String name ){
        this.userName = name;
    }

    public void setPassword(String pw ){
        this.password = pw;
    }
}
