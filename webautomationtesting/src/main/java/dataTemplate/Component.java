package dataTemplate;

import org.dom4j.Attribute;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.TestNGException;
import util.parser.XMLParser;

import java.io.File;
import java.util.HashMap;

/**
 * Created by workstation47 on 1/18/17.
 */
public  class  Component extends AbstractComponent {



    private int waitingTime = 2;
    private HashMap<String,Element> locatorMap;

    public Component(int time , File file) {
        this.waitingTime = time;

        try {
            this.locatorMap = XMLParser.getXMLMap(file);
        } catch (DocumentException e) {
            throw new TestNGException("Loading Component error , the xml is missing or broken");
        }
    }

    public Component(File file) {
        try {
            this.locatorMap = XMLParser.getXMLMap(file);
        } catch (DocumentException e) {
            throw new TestNGException("Loading Component error , the xml is missing or broken");
        }
    }

    public void setWaitingTime(int time){
        this.waitingTime = time ;
    }

    private boolean isRootElement(String el_key){
        return this.locatorMap.get(el_key).isRootElement();
    }

    public void isComponentExist(WebDriver driver){

        for (String el_key: this.locatorMap.keySet()) {
            if (isRootElement(el_key)){
                try {
                    super.getElement(driver,el_key);
                }catch (WebDriverException e){
                    // If the driver cannot find the root element , it will throw a WebDriverException error.
                    // For notification purpose,
                    throw new WebDriverException("The component does NOT exist : "+ el_key );
                }
            }
        }


    }



    @Override
    protected WebDriverWait configDriverWait(WebDriver driver, Element targetElement) {
        WebDriverWait wait;

        int elementWaitTime ;
        Attribute time = targetElement.attribute("Time");

        if (time!=null){
            elementWaitTime = Integer.parseInt(time.getValue());
        }else {
            elementWaitTime = this.waitingTime;
        }

        wait = new WebDriverWait(driver,elementWaitTime);

        return wait;
    }

    @Override
    protected Element getXMLElement(String el_key) {
        return locatorMap.get(el_key);
    }


    @Override
    protected By getLocator(String el_key) {
        Element element = locatorMap.get(el_key);
        return super.getLocator(element);
    }

}
