package testScripts.User;

import config.Components;
import config.Site;
import dataTemplate.Component;
import dataTemplate.WebTestScript;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import remoteObject.pages.login.LoginPage;
import util.ComponentHelper;
import util.configsHelper.ConfigHelper;
import util.parser.UsersParser;

/**
 * Created by shepardpin on 5/18/17.
 */
public class user_47 extends WebTestScript{


    Component loginForm = ComponentHelper.getComponent(Components.LOGIN_FORM);
    LoginPage loginPage ;


    /**
     *  Pre-Conditions :
     *  1.  The user EXIST and confirmed his/her registration request .
     *  2.  The user's account is LOCKED .
     */


    @BeforeClass
    @Override
    public void TestcasePreconditionTest() {
        site= Site.ANX;
        user= UsersParser.getUserByAlias("Account_Locked_Test",testEnv,site);
    }


    /**
     *  Actions :
     *      1. Navigate to /signin page in ANXPRO
     *      2. Input valid email name .
     *      3. Input valid as password.
     *      4. Click Login button .
     *
     *  Expected Results :
     *      1. User CANNOT user .
     *      2. The error message will be : "Your account is locked. Please contact our support team""
     *
     */

    @Test
    public void Step_1(){
        ConfigHelper.logoutBeforeTestStart(driver);
        loginPage = new LoginPage(driver,testEnv,site);

        loginPage.inputEmail(user.getUsername());
        loginPage.inputPassword(user.getPassword());
        loginPage.clickLoginSubmitButton();

        //Expected result assertions
        WebElement errorMessage = loginForm.getElement(driver,"Error_Text");
        new WebDriverWait(driver,500).until(ExpectedConditions.textToBePresentInElement(errorMessage,"Your account is locked. Please contact our support team"));
    }
}
