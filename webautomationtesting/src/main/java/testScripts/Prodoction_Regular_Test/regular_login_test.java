package testScripts.Prodoction_Regular_Test;

import dataTemplate.WebTestScript;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import remoteObject.globalComponents.Header;
import util.navigationHelper.NavigationHelper;


/**
 * Created by workstation47 on 10/19/16.
 */
public class regular_login_test extends WebTestScript {


    /**
     *  Pre-Conditions :
     *  1. User can navigate to the tested site's front page and it is loaded properly .
     */

    @BeforeClass
    @Override
    public void TestcasePreconditionTest() {
        NavigationHelper.navigate(super.driver,super.testEnv,super.site);

    }


    /**
     *
     *  Actions :
     *  1. Navigate to /signin page
     *  2. Enter valid user name
     *  3. Enter valid user password
     *  4. Click 'Login' button
     *
     *  Expected Results :
     *  1. User is logged in .
     */
    @Test()
    public void test()  {
        NavigationHelper.login(super.driver, super.testEnv,super.site,super.user);
    }



}
