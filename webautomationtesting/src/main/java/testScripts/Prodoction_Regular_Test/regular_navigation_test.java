package testScripts.Prodoction_Regular_Test;

import config.Components;
import config.SuiteConfig;
import dataTemplate.Component;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestContext;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import remoteObject.pages.explorer.ExplorerFrontPage;

import util.ComponentHelper;
import util.TestngConfigurationHelper;

import java.net.MalformedURLException;

/**
 * Created by workstation47 on 1/11/17.
 *
 *
 *
 */
public class regular_navigation_test {

    WebDriver driver;
    String url;


    ExplorerFrontPage explorerFrontPage;
    Component ExplorerHeader = ComponentHelper.getComponent(Components.EXPLORER_FRONT_HEADER);
    Component ExplorerFooter = ComponentHelper.getComponent(Components.EXPLORER_FRONT_FOOTER);
    Component AboutInfoPanel = ComponentHelper.getComponent(Components.EXPLORER_FRONT_ABOUT_INFO_PANEL);
    Component LatestInfoPanel = ComponentHelper.getComponent(Components.EXPLORER_FRONT_LATEST_INFO_PANEL);


    @BeforeClass
    public  void BeforeTest(ITestContext context) throws MalformedURLException {
        this.driver = TestngConfigurationHelper.getDriver(context);
        this.url = TestngConfigurationHelper.getXmlParameter(context, SuiteConfig.URL);
        this.driver.manage().window().maximize();
        explorerFrontPage = new ExplorerFrontPage(this.driver, url);
    }

    @Test
    public void find_all_elements(){
        ExplorerHeader.getElement(this.driver,"Header").click();
        ExplorerFooter.getElement(this.driver,"Footer").click();
        AboutInfoPanel.getElement(this.driver,"AboutInfoPanel").click();
        LatestInfoPanel.getElement(this.driver,"LatestInfoPanel").click();
    }

    @Test(dependsOnMethods = "find_all_elements")
    public void navigate_to_block(){
        ExplorerHeader.getElement(this.driver,"Block").click();

        //Assertion
        new WebDriverWait(driver,3).until(ExpectedConditions.urlContains("blocks"));

    }


}
