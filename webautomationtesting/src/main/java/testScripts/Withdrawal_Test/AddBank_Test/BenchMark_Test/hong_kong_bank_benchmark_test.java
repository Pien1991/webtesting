package testScripts.Withdrawal_Test.AddBank_Test.BenchMark_Test;

import config.Language;
import config.TestEnv;
import config.Site;
import dataTemplate.User;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.testng.ITestContext;
import org.testng.annotations.*;
import actions.LanguageActions;
import actions.HeaderActions;
import util.configsHelper.ConfigHelper;
import util.TestngConfigurationHelper;

/**
 * Created by workstation47 on 11/15/16.
 *
 * @author Shepard Pin
 */
public class hong_kong_bank_benchmark_test {

    WebDriver driver;

    User user ;

    Site webName ;
    TestEnv testEnv;

    private String ACCOUNT_NUMBER = "1234567890";
    private String ACCOUNT_HOLDER_NAME ;
    private String BANK_NICKNAME = "Test- HK Bank";


//    BankInfoPanel bankInfoPanel = new BankInfoPanel();
//    AddBankPanel addbankPanel = new AddBankPanel();

    @BeforeTest
    public void BeforeTest(ITestContext context){
        this.webName = TestngConfigurationHelper.getSite(context);
        this.testEnv = TestngConfigurationHelper.getTestEnv(context);
        this.driver = TestngConfigurationHelper.getDriver(context);
        this.user = TestngConfigurationHelper.getUser(context);
        this.driver.manage().window().maximize();
    }


    @BeforeClass()
    public  void BeforeClass(){
        ACCOUNT_HOLDER_NAME = (this.user.getCompanyName()==null ?this.user.getFullname() :this.user.getCompanyName());
        ConfigHelper.loginBeforeTestStart(this.driver,this.user,this.webName,this.testEnv);
        HeaderActions.clickFundsButton(this.driver);
        LanguageActions.ChangeLanguageTo(this.driver, Language.ENG);
    }



    @Test
    public void add_bank_button_test (){
       /*Click add bank button*/
//        bankInfoPanel.getElement(driver,"AddBank_Button").click();
        //Assertion
//        addbankPanel.getElement(driver,"AddBank_Form");
    }

    @Test(dependsOnMethods = "add_bank_button_test")
    public void select_hong_kong_bank_test (){
        /*Select international bank*/
//        addbankPanel.getSelect(driver,"Method_Select").selectByValue("string:HKG_BANK_TRANSFER");

    }

    @Test(dependsOnMethods = "select_hong_kong_bank_test")
    public void select_hkg_currency_test (){
        /*Select international bank*/
//        addbankPanel.getSelect(driver,"BankCurrency_Select").selectByVisibleText("HKD");
    }

    @Test(dependsOnMethods = "select_hkg_currency_test")
    public void add_bank_info_input_existence_test (){
        /*Input all user data*/
//        addbankPanel.getElement(driver,"AccountNumber_Input").sendKeys(ACCOUNT_NUMBER);
//        addbankPanel.getElement(driver, "AccountHolder_Input").sendKeys(ACCOUNT_HOLDER_NAME);
//        addbankPanel.getElement(driver,"BankAlias_Input").sendKeys(BANK_NICKNAME);
    }

    @Test(dependsOnMethods = "add_bank_info_input_existence_test")
    public void new_bank_existence_test () throws NoSuchMethodException {
        int bank_no ;

        try{
//            bank_no = bankInfoPanel.getBankInfoTable(this.driver).getTotalBanks();
        }catch (WebDriverException e){
            bank_no=0;
        }

//        addbankPanel.getElement(driver,"AddBankSubmit_Button").click();

//        int expected_bankNumber  = bank_no+1;

//        waitAssertion.assertEquals("getBankInfoTable",bankInfoPanel,null,6,this.driver);
//        waitAssertion.assertEquals("getTotalBanks",bankInfoPanel.getBankInfoTable(driver),expected_bankNumber,6);
    }

    @Test(dependsOnMethods = "new_bank_existence_test")
    public void addbankPanel_disappear_test () throws NoSuchMethodException, IllegalAccessException, InstantiationException {
        //Assertion
//        waitAssertion.assertFail("getElement",addbankPanel,6,this.driver,"AddBank_Form");
    }





    @Test(dependsOnMethods = "addbankPanel_disappear_test")
    public void new_bank_alias_test (){
        //Assertion
//        BankTable table = bankInfoPanel.getBankInfoTable(driver);
//        Assert.assertEquals(table.getCellText(1,1),BANK_NICKNAME);
    }

    @Test(dependsOnMethods = "new_bank_alias_test")
    public void delete_new_bank_test () throws NoSuchMethodException {

        int bank_no ;

//        BankTable table = bankInfoPanel.getBankInfoTable(driver);
        // Get total amount of bank
//        bank_no = table.getTotalBanks();

        //Test Process
//        table.deleteBank(BANK_NICKNAME);
        //Assertion : Delete Bank
//        if (bank_no==1){
            //If there is no bank , should not find the bank table
//            waitAssertion.assertFail("getBankInfoTable",bankInfoPanel,6,this.driver);
//        }else {
//            int expected_bankNumber  = bank_no-1;
//            waitAssertion.assertEquals("getTotalBanks",table,expected_bankNumber,6);
//        }
    }



}
