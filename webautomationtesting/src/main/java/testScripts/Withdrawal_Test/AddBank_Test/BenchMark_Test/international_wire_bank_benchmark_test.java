package testScripts.Withdrawal_Test.AddBank_Test.BenchMark_Test;

import config.Language;
import config.TestEnv;
import config.Site;
import dataTemplate.User;
import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.annotations.*;
import actions.LanguageActions;
import actions.HeaderActions;
import util.configsHelper.ConfigHelper;
import util.TestngConfigurationHelper;

/**
 * Created by workstation47 on 11/15/16.
 */
public class international_wire_bank_benchmark_test {

    WebDriver driver;

    User user ;

    Site webName ;
    TestEnv testEnv;

    private String BENEFICIARY_BANK_NAME = "Testing";
    private String SWIFT_CODE = "abcdefgh";
    private String ACCOUNT_NUMBER = "1234567890";
    private String BRANCH_NAME = "Testing";
    private String BENEFICIARY_BANK_ADDRESS = "Testing it must longer than 10 ";
    private String BENEFICIARY_NAME ;
    private String RECIPIENT_ADDRESS = "Testing it must longer than 10 ";
    private String BANK_NICKNAME = "Test - IW Bank";


//    BankInfoPanel bankInfoPanel = new BankInfoPanel();
//    AddBankPanel addbankPanel = new AddBankPanel();


    @BeforeTest
    public void BeforeTest(ITestContext context){
        this.webName = TestngConfigurationHelper.getSite(context);
        this.testEnv = TestngConfigurationHelper.getTestEnv(context);
        this.driver = TestngConfigurationHelper.getDriver(context);
        this.user = TestngConfigurationHelper.getUser(context);
        this.driver.manage().window().maximize();
    }


    @BeforeClass()
    public   void BeforeClass(){
        BENEFICIARY_NAME = (this.user.getCompanyName()==null ?this.user.getFullname() :this.user.getCompanyName());
        ConfigHelper.loginBeforeTestStart(this.driver,this.user,this.webName,this.testEnv);
        HeaderActions.clickFundsButton(this.driver);
        LanguageActions.ChangeLanguageTo(this.driver, Language.ENG);
    }

    @Test
    public  void add_bank_button_test (){
       /*Click add bank button*/
//        bankInfoPanel.getElement(driver,"AddBank_Button").click();

        //Assertion
//        addbankPanel.getElement(driver,"AddBank_Form");
    }

    @Test(dependsOnMethods = "add_bank_button_test")
    public  void select_international_wire_test (){
        /*Select international bank*/
//        addbankPanel.getSelect(driver,"Method_Select").selectByValue("string:BANK_WIRE");
    }

    @Test(dependsOnMethods = "select_international_wire_test")
    public  void add_bank_info_input_existence_test (){
        /*Input all user data*/
//        addbankPanel.getElement(driver,"BeneficiaryBankName_Input").sendKeys(BENEFICIARY_BANK_NAME);
//        addbankPanel.getElement(driver, "Swift_Input").sendKeys(SWIFT_CODE);
//        addbankPanel.getElement(driver, "AccountNumber_Input").sendKeys(ACCOUNT_NUMBER);
//        addbankPanel.getElement(driver, "BankBranchName_Input").sendKeys(BRANCH_NAME);
//        addbankPanel.getElement(driver, "BankAddress_Input").sendKeys(BENEFICIARY_BANK_ADDRESS);
//        addbankPanel.getElement(driver, "BeneficiaryName_Input").sendKeys(BENEFICIARY_NAME);
//        addbankPanel.getElement(driver, "RecipientAddress_Input").sendKeys(RECIPIENT_ADDRESS);
//        addbankPanel.getElement(driver,"BankAlias_Input").sendKeys(BANK_NICKNAME);

    }

    @Test(dependsOnMethods = "add_bank_info_input_existence_test")
    public void new_bank_existence_test () throws NoSuchMethodException {
        int bank_no ;

//        try{
//            bank_no = bankInfoPanel.getBankInfoTable(this.driver).getTotalBanks();
//        }catch (WebDriverException e){
//            bank_no=0;
//        }

        /*Click Submit button*/
//        addbankPanel.getElement(driver,"AddBankSubmit_Button").click();
//
//        int expected_bankNumber = bank_no + 1;
//        waitAssertion.assertEquals("getBankInfoTable", bankInfoPanel, null, 6, this.driver);
//        waitAssertion.assertEquals("getTotalBanks", bankInfoPanel.getBankInfoTable(driver), expected_bankNumber, 6);
    }

    @Test(dependsOnMethods = "new_bank_existence_test")
    public  void addbankPanel_disappear_test () throws IllegalAccessException, InstantiationException, NoSuchMethodException {
        //Assertion
//        waitAssertion.assertFail("getElement",addbankPanel,6,this.driver,"AddBank_Form");
    }



    @Test(dependsOnMethods = "new_bank_existence_test")
    public void new_bank_alias_test (){
        //Assertion
//        BankTable table = bankInfoPanel.getBankInfoTable(driver);
//        Assert.assertEquals(table.getCellText(1,1),BANK_NICKNAME);
    }

    @Test(dependsOnMethods = "new_bank_alias_test")
    public void delete_new_bank_test () throws NoSuchMethodException {
        int bank_no ;
//        BankTable table = bankInfoPanel.getBankInfoTable(driver);
        // Get total amount of bank
//        bank_no = table.getTotalBanks();

        //Test Process
//        table.deleteBank(BANK_NICKNAME);
        //Assertion : Delete Bank
//        if (bank_no==1){
            //If there is no bank , should not find the bank table
//            waitAssertion.assertFail("getBankInfoTable",bankInfoPanel,6,this.driver);
//        }else {
//            int expected_bankNumber  = bank_no-1;
//            waitAssertion.assertEquals("getTotalBanks",table,expected_bankNumber,6);
//        }
    }


}


