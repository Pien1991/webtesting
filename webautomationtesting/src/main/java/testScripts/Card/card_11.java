package testScripts.Card;

import actions.HeaderActions;
import config.Page;
import config.Site;
import dataTemplate.WebTestScript;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import remoteObject.pages.card.CardPage;
import util.configsHelper.ConfigHelper;
import util.navigationHelper.NavigationHelper;
import util.parser.UsersParser;

/**
 * Created by shepardpin on 5/9/17.
 */
public class card_11 extends WebTestScript {





    CardPage cardPage;


    /**
     *  Pre-Conditions :
     *  1. The verification_state in user table for that user should be VERIFIED .
     *  2. The tested website is DCEXE .
     *  3. User does NOT have ANY cards.
     */


    @BeforeClass
    @Override
    public void TestcasePreconditionTest() {
        user= UsersParser.getUserByAlias("Verified_NoCards_NoOTP_User",testEnv,site);
        site = Site.DCEXE ;
    }


    /**
     *  Actions :
     *  1. Login and Navigate to /card page.
     *
     *  Expected Results :
     *  1. The page should redirect to /card/request page
     */

    @Test
    public void Step_1(){

        // Actions :
        ConfigHelper.loginBeforeTestStart(driver,user,site,testEnv);
        NavigationHelper.navigate(driver,testEnv,site, Page.CARD);

        //Expected Result :
        new WebDriverWait(driver,5).until(ExpectedConditions.urlToBe(Site.getUrl(site,testEnv)+"/card/request"));
    }



}
