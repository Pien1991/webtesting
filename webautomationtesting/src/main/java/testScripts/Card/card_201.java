package testScripts.Card;

import actions.LanguageActions;
import config.Components;
import config.Language;
import config.Site;
import dataTemplate.Component;
import dataTemplate.WebTestScript;
import dataTemplate.tables.CardOrderTable;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import remoteObject.pages.card_request.CardRequestPage;
import util.ComponentHelper;
import util.configsHelper.ConfigHelper;
import util.parser.UsersParser;

/**
 * Created by shepardpin on 5/11/17.
 */
public class card_201 extends WebTestScript{


    CardRequestPage cardRequestPage ;

    Component USDPremiumCard = ComponentHelper.getComponent(Components.REQUEST_USD_PREMIUM_CARD);
    Component cardConfirmPanel = ComponentHelper.getComponent(Components.REQUEST_CARD_CONFIRM_PANEL);

    /**
     *  Pre-Conditions :
     *  1.  For ANX Premium (USD) , the max_quantity_per_user = 5 .
     *  2.  User owns FIVE ANX Premium card and NONE of others .
     *  3.  ALL ANX Premium cards are ACTIVATED .
     *
     */

    @BeforeClass
    @Override
    public void TestcasePreconditionTest() {
        user= UsersParser.getUserByAlias("MaxPremiumCardTest",testEnv,site);
        site = Site.ANX ;
    }


    /**
     *  Actions :
     *  1. Login ANXPRO and Navigate to /card/request page .
     *  2. Select "ATM Card" as Card Type and "1" as Amount for ANX BTC Elite Card .
     *  3.  Select "ATM Card" as Card Type and "1" as Amount for ANX BTC Premium Card .
     *  4. Click "CONTINUE" button to "Delivery"  stage.
     *  5. Click "CONTINUE" button to "Confirmation" stage.
     *
     *  Expected Result:
     *  1. The Error message should show in the botton of the request table in "Confirmation" stage :
     *                  "SORRY, THERE'S A PROBLEM WITH YOUR REQUEST. PLEASE AMEND AND RESUBMIT"
     *      "You have exceeded the maximum allowable ordered quantity for this card type. Please contact support for bulk order inquiries."
     *
     */

    @Test
    public void Step_1()  {

        //Action
        ConfigHelper.loginBeforeTestStart(driver,user,site,testEnv);
        cardRequestPage = new CardRequestPage(driver,testEnv,site);

        cardRequestPage.selectCardAmount(USDPremiumCard,1);


        cardRequestPage.clickContinueButton();
        cardRequestPage.clickContinueButton();

        //Assertion
        WebElement errorMessage_header = cardConfirmPanel.getElement(driver,"ErrorMessage_Header");
        String ExHeaderText = "SORRY, THERE'S A PROBLEM WITH YOUR REQUEST. PLEASE AMEND AND RESUBMIT";
        Assert.assertEquals(errorMessage_header.getText(),ExHeaderText);


        WebElement errorMessage_context = cardConfirmPanel.getElement(driver,"ErrorMessage_Context");
        String ExContextText = "You have exceeded the maximum allowable ordered quantity for this card type. Please contact support for bulk order inquiries.";
        Assert.assertEquals(errorMessage_context.getText(),ExContextText);



    }


    /**
     *  Actions :
     *  1. Change to Simplified Chinese
     *
     *  Expected Result:
     *  1. The Error message should show in the botton of the request table in "Confirmation" stage :
     *                  "对不起, 你的请求出现问题。请修改并重新提交。"
     *          "你的下单数量，已经超过此卡的限度要求。请联系客服咨询大额下单。"
     *
     */

    @Test(dependsOnMethods = "Step_1")
    public void Step_2()  {

        //Action
        LanguageActions.ChangeLanguageTo(driver, Language.SCHI);

        //Assertion
        WebElement errorMessage_header = cardConfirmPanel.getElement(driver,"ErrorMessage_Header");
        String ExHeaderText = "对不起, 你的请求出现问题。请修改并重新提交。";
        Assert.assertEquals(errorMessage_header.getText(),ExHeaderText);


        WebElement errorMessage_context = cardConfirmPanel.getElement(driver,"ErrorMessage_Context");
        String ExContextText = "你的下单数量，已经超过此卡的限度要求。请联系客服咨询大额下单。";
        Assert.assertEquals(errorMessage_context.getText(),ExContextText);

    }


    /**
     *  Actions :
     *  1. Change to Simplified Chinese
     *
     *  Expected Result:
     *  1. The Error message should show in the botton of the request table in "Confirmation" stage :
     *                  "對不起, 你的請求出現問題。請修改並重新提交"
     *          "你的下單數量，已經超過此咭的最大限額。請聯繫客戶服務部查詢大額下單。"
     *
     */

    @Test(dependsOnMethods = "Step_1")
    public void Step_3()  {

        //Action
        LanguageActions.ChangeLanguageTo(driver, Language.TCHI);

        //Assertion
        WebElement errorMessage_header = cardConfirmPanel.getElement(driver,"ErrorMessage_Header");
        String ExHeaderText = "對不起, 你的請求出現問題。請修改並重新提交";
        Assert.assertEquals(errorMessage_header.getText(),ExHeaderText);


        WebElement errorMessage_context = cardConfirmPanel.getElement(driver,"ErrorMessage_Context");
        String ExContextText = "你的下單數量，已經超過此咭的最大限額。請聯繫客戶服務部查詢大額下單。";
        Assert.assertEquals(errorMessage_context.getText(),ExContextText);

    }




}
