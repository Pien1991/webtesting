package testScripts.Card;

import actions.LanguageActions;
import config.Components;
import config.Language;
import config.Site;
import dataTemplate.Component;
import dataTemplate.WebTestScript;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ISelect;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import remoteObject.pages.card.CardPage;
import util.ComponentHelper;
import util.configsHelper.ConfigHelper;
import util.parser.UsersParser;

/**
 * Created by shepardpin on 5/23/17.
 */

//TODO : No Yet Finish!!!
public class card_117 extends WebTestScript{

    CardPage cardPage;
    Component cardFundPanel = ComponentHelper.getComponent(Components.CARD_FUNDS_PANEL);


    /**
     * Pre-Conditions :
     *  1.  User owns ONLY ONE Non-RHA card  ( In OAT , assume Elite Top Up Card in ANXPRO)
     *  2.  In card_type table , the fiat_topup_enabled = true for tested card's card type .
     *  3.  NO recharge fee should exist as top_up fee set as 0
     *  4.  In user's USD account , total balance =  990 USD and available balance = 500 USD
     *  5.  In user's HKD account , total balance = 1500 HKD and available balance = 900 HKD
     *  6.  Available balance of BTC = 0
     *
     */
    @BeforeClass
    @Override
    public void TestcasePreconditionTest() {
        site = Site.ANX;
        user = UsersParser.getUserByAlias("Fiat_Enable_Recharge_Display_Test", testEnv, site);
    }

    /**
     * Actions :
     *  1.   Login and Navigate to /card page
     *  2.  Click "FIAT CURRENCIES" tab
     *  3.  Click Fiat Account Select dropbox
     *
     * Expected Results :
     *  1.  For USD :  the amount should be 500 USD inside the dropdown list.
     *  2.  For HKD :  the amount should be 900 HKD inside the dropdown list.
     *
     */

    @Test
    public void Step_1() throws InterruptedException {
        //Actions :
        ConfigHelper.loginBeforeTestStart(driver, user, site, testEnv);

        cardPage = new CardPage(driver, testEnv, site);

        LanguageActions.ChangeLanguageTo(driver, Language.ENG);

        cardPage.clickChargeByFiatTab();


        //Assertion on  Existence of Charge Account Selection Box
        ISelect RechargeAccountSelect = cardPage.getRechargeAccountSelection();

        //  There is  an unknown bug that cannot select the selection by visible text
        //  To solve it , user need to click the selection again .
        //  It is a quick fix . Root cause still need to be found .
        cardFundPanel.getElement(driver,"ChargeAccount_Select").click();

        //Assertion on Existence of USD Account
        RechargeAccountSelect.selectByVisibleText("USD - 500.00");


        //Assertion on Existence of HKD Account
        RechargeAccountSelect.selectByVisibleText("HKD - 900.00");


    }

    /**
     * Actions :
     * 1.   Click "HKD" in dropdown list.
     * 2.   Click "Unit" dropdown box near the Recharge Amount Input box . Select HKD
     *
     * Expected Results :
     * 1.   The dropdown box should ONLY show USD and BTC.
     *
     *
     */

    @Test(dependsOnMethods = "Step_1")
    public void Step_2() {
        //Actions :
        cardPage.getRechargeUnitSelection().selectByVisibleText("HKD");



        //Assertions :
        WebElement RechargeAmountInput = cardFundPanel.getElement(driver,"ChargeAmount_Input");
        String minMaxAmount = RechargeAmountInput.getAttribute("placeholder");
        Assert.assertEquals(minMaxAmount.contains("900"),true);
    }


}
