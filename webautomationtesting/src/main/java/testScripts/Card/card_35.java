package testScripts.Card;

import actions.LanguageActions;
import config.Components;
import config.Language;
import config.Site;
import dataTemplate.Component;
import dataTemplate.WebTestScript;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import remoteObject.pages.card_request.CardRequestPage;
import util.ComponentHelper;
import util.configsHelper.ConfigHelper;
import util.parser.UsersParser;

/**
 * Created by shepardpin on 5/11/17.
 */
public class card_35 extends WebTestScript  {

    CardRequestPage cardRequestPage ;
    Component PlusCard = ComponentHelper.getComponent(Components.REQUEST_PLUS_CARD);


    /**
     *  Pre-Conditions :
     *  1.  In card_type table ,  the in_stock = false  for the tested card's card type . ( On OAT , we presume it is Plus card )
     *
     */

    @BeforeClass
    @Override
    public void TestcasePreconditionTest() {
        user= UsersParser.getUserByAlias("Verified_NoCards_NoOTP_User",testEnv,site);
        site = Site.ANX ;
    }


    /**
     *  Actions :
     *  1. Login and Navigate to /card/request page .
     *  2. Check the tested card 's title  .
     *
     *
     *  Expected Result:
     *  1. Near the title it should show "Out of stock"
     *
     */

    @Test
    public void Step_1()  {

        //Action
        ConfigHelper.loginBeforeTestStart(driver,user,site,testEnv);
        cardRequestPage = new CardRequestPage(driver,testEnv,site);

        WebElement outOfStore_text =PlusCard.getElement(driver,"OutOfStore_Text");
        Assert.assertEquals(outOfStore_text.getText()," Out of stock");

    }



}
