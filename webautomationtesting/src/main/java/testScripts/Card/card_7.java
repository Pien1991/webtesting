package testScripts.Card;


import config.Components;
import config.Page;
import config.Site;
import dataTemplate.Component;
import dataTemplate.WebTestScript;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import remoteObject.pages.card.CardPage;
import util.ComponentHelper;
import util.configsHelper.ConfigHelper;
import util.navigationHelper.NavigationHelper;
import util.parser.UsersParser;

/**
 * Created by shepardpin on 5/5/17.
 */
public class card_7 extends WebTestScript{

    CardPage cardPage;

    Component AdvertisementPanel = ComponentHelper.getComponent(Components.CARD_ADVERTISEMENT_PANEL);

    /**
     *  Pre-Conditions :
     *  1. The verification_state in user table for that user should be PENDING_VERIFICATION .
     *  2. The tested website is ANXPRO .
     */

    @BeforeClass
    @Override
    public void TestcasePreconditionTest() {
        site= Site.ANX;
        user= UsersParser.getUserByAlias("PendingVerification_NoCard_User",testEnv,site);
    }

    /**
     *  Actions :
     *  1. Login and navigate to /card page
     *
     *  Expected Results :
     *  1. The advertisement panel should exist .
     *
     */

    @Test
    public void Step_1(){
        ConfigHelper.loginBeforeTestStart(driver,user,site,testEnv);

        cardPage = new CardPage(driver,testEnv,site);
        //Assertions :
        AdvertisementPanel.isComponentExist(driver);

    }

    /**
     *  Actions :
     *  1. Click "GET ONE NOW" button in ANX ELITE card feature division.
     *
     *  Expected Results :
     *  1. It will navigate to /card/request.
     *
     */

    @Test(dependsOnMethods = "Step_1")
    public void Step_2(){

        //Actions :
        AdvertisementPanel.getElement(driver,"EliteCard_Link").click();

        //Assertion :
        new WebDriverWait(driver,2).until(ExpectedConditions.urlContains("/card/request"));

    }

    /**
     *  Actions :
     *  1. Return to /card page.
     *  2. Click "GET ONE NOW" button in ANX PREMIUM card feature division.
     *
     *  Expected Results :
     *  1. It will navigate to /card/request
     *
     */

    @Test(dependsOnMethods = "Step_1")
    public void Step_3(){

        //Actions :
        cardPage = new CardPage(driver,testEnv,site);

        AdvertisementPanel.getElement(driver,"PremiumCard_Link").click();

        //Assertion :
        new WebDriverWait(driver,2).until(ExpectedConditions.urlContains("/card/request"));

    }


    /**
     *  Actions :
     *  1. Return to /card page.
     *  2. Click "COMPARE CARDS" button at the end of advertisement.
     *
     *  Expected Results :
     *  1. It will navigate to /page/card-compare
     *
     */

    @Test(dependsOnMethods = "Step_1")
    public void Step_4(){

        //Actions :
        cardPage = new CardPage(driver,testEnv,site);
        AdvertisementPanel.getElement(driver,"CompareCard_Link").click();

        //Assertion :
        new WebDriverWait(driver,2).until(ExpectedConditions.urlContains("/pages/card-compare"));

    }



}
