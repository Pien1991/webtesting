package testScripts.Card;

import config.Components;
import config.Site;
import dataTemplate.Component;
import dataTemplate.WebTestScript;
import org.openqa.selenium.WebDriverException;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import remoteObject.pages.card_request.CardRequestPage;
import util.ComponentHelper;
import util.configsHelper.ConfigHelper;
import util.parser.UsersParser;

/**
 * Created by shepardpin on 4/19/17.
 *
 */
public class card_14 extends WebTestScript{

    CardRequestPage cardRequestPage;

    Component cardChoicePanel = ComponentHelper.getComponent(Components.REQUEST_SELECT_CARD_PANEL);
    Component cardDeliveryPanel = ComponentHelper.getComponent(Components.REQUEST_CARD_DELIVERY_PANEL);
    Component cardConfirmPanel = ComponentHelper.getComponent(Components.REQUEST_CARD_CONFIRM_PANEL);
    Component eliteCard = ComponentHelper.getComponent(Components.REQUEST_ELITE_CARD);



    /**
     *  Pre-Conditions :
     *  1. The user finished verification. Hence , the verification_state in user table should be VERIFIED
     */

    @BeforeClass
    @Override
    public void TestcasePreconditionTest() {
        user= UsersParser.getUserByAlias("Verified_NoCards_NoOTP_User",testEnv,site);
        site = Site.ANX ;
    }

    /**
     *  Actions :
     *  1. Login and Navigate to /card/request page .
     *
     *  Expected Results :
     *  1. User navigates to /card/request page.
     *  2. The request stages should be in  "Select Card"
     */

    @Test
    public void Step_1(){
        ConfigHelper.loginBeforeTestStart(driver,user,site,testEnv);
        cardRequestPage = new CardRequestPage(driver,testEnv,site);


        //Expected result assertions
        String url = driver.getCurrentUrl();
        Assert.assertEquals(url.contains("/card/request"),true);

        cardChoicePanel.isComponentExist(driver);

    }

    /**
     *  Actions :
     *  1. Choose one of the card with at least ONE quantity.
     *  2. Click "CONTINUE" button to next stage of card request.
     *
     *  Expected Results :
     *  1. The request stages should be in  "Delivery"
     */
    @Test(dependsOnMethods = "Step_1")
    public void Step_2(){
        cardRequestPage.selectCardAmount(eliteCard,1);
        cardRequestPage.clickContinueButton();

        cardDeliveryPanel.isComponentExist(driver);
    }

    /**
     *  Actions :
     *  1. Click "CONTINUE" button to next stage of card request.
     *
     *  Expected Results :
     *  1. The request stages should be in  "Confirmation"
     */
    @Test(dependsOnMethods = "Step_2")
    public void Step_3(){
        cardRequestPage.clickContinueButton();

        cardConfirmPanel.isComponentExist(driver);
    }



    /**
     *  Actions :
     *  1. Click "Delivery" button in card request navigation bar
     *
     *  Expected Results :
     *  1. The stage of card request is "Delivery" .
     */
    @Test(dependsOnMethods = "Step_3")
    public void Step_4(){
        //Actions
        cardRequestPage.clickDeliveryStage();

        // Assertion
        cardDeliveryPanel.isComponentExist(driver);

    }

    /**
     *  Actions :
     *  1. Click "Confirmation" button in card request navigation bar
     *
     *  Expected Results :
     *  1. The stage of card request is STILL  "Delivery" .
     *
     */
    @Test(dependsOnMethods = "Step_4")
    public void Step_5(){
        //Actions
        cardRequestPage.clickConfirmationStage();

        // Assertion
        cardDeliveryPanel.isComponentExist(driver);

    }

    /**
     *  Actions :
     *  1. Click "Select Card" button in card request navigation bar
     *
     *  Expected Results :
     *  1. The stage of card request is "Select Card" .
     */
    @Test(dependsOnMethods = "Step_5")
    public void Step_6(){
        //Actions
        cardRequestPage.clickSelectCardStage();

        // Assertion
        cardChoicePanel.isComponentExist(driver);

    }


    /**
     *  Actions :
     *  1. Click "Delivery" button in card request navigation bar
     *
     *  Expected Results :
     *  1. The stage of card request is STILL  "Select Card" .
     *  2. The "Delivery" button is NOT clickable .
     */
    @Test(dependsOnMethods = "Step_6")
    public void Step_7(){
        try {
            //Actions
            cardRequestPage.clickDeliveryStage();
            //Assertions
            Assert.fail("The Delivery Stage should NOT be clickable");
        }catch (WebDriverException e){}

        //Assertions
        cardChoicePanel.isComponentExist(driver);

    }

}
