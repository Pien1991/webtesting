package testScripts.Card;

import actions.LanguageActions;
import config.Components;
import config.Language;
import config.Site;
import dataTemplate.Component;
import dataTemplate.WebTestScript;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import remoteObject.pages.card.CardPage;
import util.ComponentHelper;
import util.configsHelper.ConfigHelper;
import util.parser.UsersParser;

/**
 * Created by shepardpin on 5/15/17.
 */
public class card_59 extends WebTestScript {

    CardPage cardPage;
    Component DetailsPanel = ComponentHelper.getComponent(Components.CARD_DETAIL_PANEL);
    WebDriverWait wait ;



    /**
     * Pre-Conditions :
     *  1.  User own ONLY ONE Plus card  .
     *  2.  For Plus Card in ANXPRO , the network_type = CUP and base_currency = HKD in card_type table . ( which means China Union Pay )
     */
    @BeforeClass
    @Override
    public void TestcasePreconditionTest() {
        wait = new WebDriverWait(driver, 2);
        site = Site.ANX;
        user = UsersParser.getUserByAlias("PlusCardTest", testEnv, site);
    }


    /**
     * Actions :
     * 1. Login ANXPRO and Navigate to /card page  .
     *
     * Expected Results :
     * 1. The card details should  show as :
     *      Network             :   UNION PAY
     *      Available Balance   :   - HKD
     *      Current Status	    :   ACTIVATED
     *      Card Type	        :   ATM Card
     * 2. The Current Status should NOT exist card status information .
     * 3. The Card Limit Level should NOT exist .
     * 4. The Advanced Setting should NOT exist .
     */


    @Test
    public void Step_1() {
        //Actions :
        ConfigHelper.loginBeforeTestStart(driver, user, site, testEnv);
        cardPage = new CardPage(driver,testEnv,site);
        LanguageActions.ChangeLanguageTo(driver, Language.ENG);


        //Assertions :
        DetailsPanel.isComponentExist(driver);

        //Assertion on Correctness and Existence of Network image .
        WebElement NetworkImage = DetailsPanel.getElement(driver, "Network_Image");
        // Check the src to verify whether the image is correct
        wait.until(ExpectedConditions.attributeToBe(NetworkImage, "anx-src", "/images/UnionPay_logo.svg"));

        //Assertion on Correctness and Existence of Available Balance unit .
        WebElement AvailableBalanceUnit = DetailsPanel.getElement(driver, "AvailableBalance_Unit");
        wait.until(ExpectedConditions.textToBePresentInElement(AvailableBalanceUnit, "HKD"));

        //Assertion on NON-Existence of ACTIVATE NOW button
        try {
            DetailsPanel.getElement(driver, "Activate_Button");
            Assert.fail("The 'Activate Now' Button should NOT exist");
        } catch (WebDriverException e) {
        }

        //Assertion on NON-Existence of  Get Verified Button
        try {
            DetailsPanel.getElement(driver, "GetVerified_Button");
            Assert.fail("The 'Get Verified' Button should NOT exist");
        } catch (WebDriverException e) {
        }


        //Assertion on Existence and Correctness of card status information
        WebElement CardStatusText = DetailsPanel.getElement(driver, "CardStatus_Text");
        wait.until(ExpectedConditions.textToBePresentInElement(CardStatusText, "Activated"));



        //Assertion on NON-Existence of  Card Limit Level
        try {
            DetailsPanel.getElement(driver, "CardLimitLevel_Column");
            Assert.fail("The card limit level column should NOT exist");
        } catch (WebDriverException e) {
        }



        //Assertion on Correctness and Existence of Card Type information
        WebElement CardTypeText = DetailsPanel.getElement(driver, "CardType_Text");
        wait.until(ExpectedConditions.textToBePresentInElement(CardTypeText, "ATM Card"));

        //Assertion on NON-Existence of Advanced Setting
        try {
            DetailsPanel.getElement(driver, "AdvancedSettings_Column");
            Assert.fail("The advanced settings column should NOT exist");
        } catch (WebDriverException e) {
        }



    }

}
