package testScripts.Card;

import actions.LanguageActions;
import config.Components;
import config.Language;
import config.Site;
import dataTemplate.Component;
import dataTemplate.WebTestScript;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import remoteObject.pages.card.CardPage;
import util.ComponentHelper;
import util.configsHelper.ConfigHelper;
import util.parser.UsersParser;

/**
 * Created by shepardpin on 5/12/17.
 */
public class card_78 extends WebTestScript {



    CardPage cardPage;
    Component DetailsPanel = ComponentHelper.getComponent(Components.CARD_DETAIL_PANEL);
    WebDriverWait wait;

    /**
     * Pre-Conditions :
     * 1. User own ONLY ONE RHA Virtual card , where the card_statues  = ACTIVATED  and kyc2status = KYC_REQUIRED .
     * 2. In card table , the daily_limit =  2000 for tested card.
     .
     */


    @BeforeClass
    @Override
    public void TestcasePreconditionTest() {
        wait = new WebDriverWait(driver, 2);
        site = Site.ANX;
        user = UsersParser.getUserByAlias("Virtual_After_Activation_Before_Upgrade_Test", testEnv, site);
    }


    /**
     * Actions :
     *  1.  Login and Navigate to /card page .
     *  2.  Click 'Daily Limit' button
     * Expected Results :
     *  1. The Card Detail will change to Current Daily Limit ;
     *
     *
     */


    @Test
    public void Step_1() {
        ConfigHelper.loginBeforeTestStart(driver, user, site, testEnv);
        cardPage = new CardPage(driver,testEnv,site);
        LanguageActions.ChangeLanguageTo(driver, Language.ENG);


        //Assertions :
        cardPage.clickDailyLimitButton();

        WebElement DailyLimitTitle = DetailsPanel.getElement(driver,"CurrentDailyLimit_Title");
        Assert.assertEquals(DailyLimitTitle.getText(),"Current Daily Limit*:");

        WebElement DailyLimitAmount = DetailsPanel.getElement(driver,"CurrentDailyLimit_Amount");
        Assert.assertEquals(DailyLimitAmount.getText(),"2,000.00");

        WebElement DailyLimitUnit = DetailsPanel.getElement(driver,"CurrentDailyLimit_Unit");
        Assert.assertEquals(DailyLimitUnit.getText()," USD");

    }




}
