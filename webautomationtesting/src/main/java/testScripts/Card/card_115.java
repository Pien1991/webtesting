package testScripts.Card;

import actions.LanguageActions;
import config.Components;
import config.Language;
import config.Site;
import dataTemplate.Component;
import dataTemplate.WebTestScript;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import remoteObject.pages.card.CardPage;
import util.ComponentHelper;
import util.configsHelper.ConfigHelper;
import util.parser.UsersParser;

/**
 * Created by shepardpin on 5/15/17.
 */
public class card_115 extends WebTestScript {



    CardPage cardPage;
    Component cardFundPanel = ComponentHelper.getComponent(Components.CARD_FUNDS_PANEL);
    WebDriverWait wait;


    /**
     * Pre-Conditions :
     * 1.  User own ONLY ONE RHA card.
     */
    @BeforeClass
    @Override
    public void TestcasePreconditionTest() {
        wait = new WebDriverWait(driver, 2);
        site = Site.ANX;
        user = UsersParser.getUserByAlias("RhaPhysicalCardTest", testEnv, site);
    }

    /**
     * Actions :
     * 1.   Login ANXPRO and Navigate to /card page  .
     *
     *
     * Expected Results :
     * 1.   The notification in RECHARGE area should be :
     *          This card is linked  with your BTC wallet
     */


    @Test
    public void Step_1() {
        //Actions :
        ConfigHelper.loginBeforeTestStart(driver, user, site, testEnv);
        cardPage = new CardPage(driver, testEnv, site);
        LanguageActions.ChangeLanguageTo(driver, Language.ENG);


        //Correctness of notification in RECHARGE area:
        WebElement notification = cardFundPanel.getElement(driver, "RHACardCharge_Notification");
        String ExText = "This card is linked with your BTC wallet";
        Assert.assertEquals(notification.getText(),ExText);

        //Existence of RHA Card Charge Image in RECHARGE area:
        cardFundPanel.getElement(driver, "RHACardCharge_Image");
        //Existence of Manage Funds Button in RECHARGE area:
        cardFundPanel.getElement(driver, "ManageFunds_Button");
        //Existence of Buy BTC Button in RECHARGE area:
        cardFundPanel.getElement(driver, "BuyBTC_Button");

    }


    /**
     * Actions :
     * 1.   Click "MANAGE FUNDS" button .
     * Expected Results :
     * 1.   Browser will navigate to /funds page .
     */


    @Test(dependsOnMethods = "Step_1")
    public void Step_2() {
        //Actions :
        cardFundPanel.getElement(driver,"ManageFunds_Button").click();

        //Assertion:
        wait.until(ExpectedConditions.urlContains("/funds"));

    }


    /**
     * Actions :
     * 1.   Navigate to /card page .
     * 2.   Click "BUY BTC" button .
     *
     * Expected Results :
     * 1.   Browser will navigate to /trade page .
     */


    @Test(dependsOnMethods = "Step_1")
    public void Step_3() {
        //Actions :
        cardPage = new CardPage(driver, testEnv, site);
        cardFundPanel.getElement(driver,"BuyBTC_Button").click();

        //Assertion:
        wait.until(ExpectedConditions.urlContains("/trade"));

    }


}
