package testScripts.Card;

import config.Components;
import config.Site;
import dataTemplate.Component;
import dataTemplate.WebTestScript;
import org.openqa.selenium.WebDriverException;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import remoteObject.pages.card_request.CardRequestPage;
import util.ComponentHelper;
import util.configsHelper.ConfigHelper;
import util.parser.UsersParser;

/**
 * Created by shepardpin on 5/11/17.
 */
public class card_27 extends WebTestScript{

    CardRequestPage cardRequestPage ;

    Component eliteCard = ComponentHelper.getComponent(Components.REQUEST_ELITE_CARD);
    //It represents Premium card in DCEXE which the purchase unit is USD and Premium card in ANXPRO which the purchase unit is BTC
    Component USDPremiumCard = ComponentHelper.getComponent(Components.REQUEST_USD_PREMIUM_CARD);

    Component deliveryPanel = ComponentHelper.getComponent(Components.REQUEST_CARD_DELIVERY_PANEL);



    @BeforeClass
    @Override
    public void TestcasePreconditionTest() {
        site = Site.ANX;
        user = UsersParser.getUserByAlias("Verified_NoCards_NoOTP_User", testEnv, site);
    }


    /**
     *  Actions :
     *  1. Login ANXPRO and Navigate to /card/request page .
     *  2. Select "Digital Card" as Card Type and "1" as Amount for ANX BTC Elite Card and the amounts of other cards should be "0".
     *  3. Click "CONTINUE" button to "Delivery"  stage.
     *
     *  Expected Result:
     *  2.  The Delivery Method Selection should display
     *
     */

    @Test
    public void Step_1(){

        //Action
        ConfigHelper.loginBeforeTestStart(driver,user,site,testEnv);
        cardRequestPage = new CardRequestPage(driver,testEnv,site);

        cardRequestPage.selectCardAmount(eliteCard,1);
        cardRequestPage.selectCardAmount(USDPremiumCard,1);


        cardRequestPage.selectVirtual(eliteCard);
        cardRequestPage.clickContinueButton();



        //Assertion
        deliveryPanel.getElement(driver,"DeliveryMethod_Select");

        try {
            deliveryPanel.getElement(driver,"VirtualDelivery_Message");
            Assert.fail("Should NOT exist virtual delivery message");
        }catch (WebDriverException e){

        }

    }
}
