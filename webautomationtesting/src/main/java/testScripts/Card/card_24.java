package testScripts.Card;

import actions.LanguageActions;
import config.Components;
import config.Language;
import config.Site;
import dataTemplate.Component;
import dataTemplate.WebTestScript;
import dataTemplate.tables.CardOrderTable;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import remoteObject.pages.card_request.CardRequestPage;
import util.ComponentHelper;
import util.configsHelper.ConfigHelper;
import util.parser.UsersParser;

/**
 * Created by shepardpin on 5/10/17.
 */
public class card_24 extends WebTestScript{

    CardRequestPage cardRequestPage ;

    Component eliteCard = ComponentHelper.getComponent(Components.REQUEST_ELITE_CARD);

    //It represents Premium card in DCEXE which the purchase unit is USD and Premium card in ANXPRO which the purchase unit is BTC
    Component USDPremiumCard = ComponentHelper.getComponent(Components.REQUEST_USD_PREMIUM_CARD);

    @BeforeClass
    @Override
    public void TestcasePreconditionTest() {
        site = Site.ANX;
        user = UsersParser.getUserByAlias("Verified_NoCards_NoOTP_User", testEnv, site);
    }


    /**
     *  Actions :
     *  1. Login ANXPRO and Navigate to /card/request page .
     *  2. Select "Digital Card" as Card Type and "1" as Amount for ANX BTC Elite Card and the amounts of other cards should be "0".
     *  3. Click "CONTINUE" button to "Delivery"  stage.
     *  4. Click "CONTINUE" button to "Confirmation" stage.
     *  5. Change to Simplified Chinese
     *
     *  Expected Result:
     *  1. The price of ANX BTC Elite Card is 0.05 BTC
     *  2. The quantity of ANX BTC Elite Card is 1
     *
     */

    @Test
    public void Step_1(){

        //Action
        ConfigHelper.loginBeforeTestStart(driver,user,site,testEnv);
        cardRequestPage = new CardRequestPage(driver,testEnv,site);

        cardRequestPage.selectCardAmount(eliteCard,1);
        cardRequestPage.selectCardAmount(USDPremiumCard,0);
        cardRequestPage.selectVirtual(eliteCard);
        cardRequestPage.clickContinueButton();
        cardRequestPage.clickContinueButton();


        LanguageActions.ChangeLanguageTo(driver, Language.SCHI);

        //Assertion
        CardOrderTable cardOrderTable = cardRequestPage.getCardOrderTable();

        String cardQuantity = cardOrderTable.getCardQuantity("非凡卡");
        String cardPrice = cardOrderTable.getCardCost("非凡卡");

        Assert.assertEquals(cardQuantity,"1");
        Assert.assertEquals(cardPrice,"0.05 比特币");
    }


}
