package testScripts.Card;

import actions.LanguageActions;
import config.Language;
import config.Site;
import dataTemplate.Component;
import dataTemplate.WebTestScript;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import remoteObject.globalComponents.PushErrorMessageBox;
import remoteObject.pages.card_request.CardRequestPage;
import util.configsHelper.ConfigHelper;
import util.parser.UsersParser;

/**
 * Created by shepardpin on 5/11/17.
 */
public class card_34 extends WebTestScript{


    CardRequestPage cardRequestPage ;

    Component pushErrorMessage = new PushErrorMessageBox();

    /**
     *  Pre-Conditions :
     *  1.  User is VERIFIED .
     *
     */

    @BeforeClass
    @Override
    public void TestcasePreconditionTest() {
        user= UsersParser.getUserByAlias("Verified_NoCards_NoOTP_User",testEnv,site);
        site = Site.ANX ;
    }

    /**
     *  Actions :
     *  1. Login ANXPRO and Navigate to /card/request page .
     *  2. Click "CONTINUE" button in "Select Card" stage.

     *
     *  Expected Result:
     *  1. A error message will pop up  :
     *                  "Please select at least one"
     *
     */

    @Test
    public void Step_1()  {

        //Action
        ConfigHelper.loginBeforeTestStart(driver,user,site,testEnv);
        cardRequestPage = new CardRequestPage(driver,testEnv,site);

        cardRequestPage.clickContinueButton();

        //Assertion
        pushErrorMessage.isComponentExist(driver);
        WebElement errorMessage = pushErrorMessage.getElement(driver,"ErrorMessage_Text");
        Assert.assertEquals(errorMessage.getText(),"Please select at least one");

    }

    /**
     *  Actions :
     *  1. Change to Simplified Chinese

     *
     *  Expected Result:
     *  1. A error message will pop up  :
     *                  "请选择至少一项"
     *
     */

    @Test(dependsOnMethods = "Step_1")
    public void Step_2() throws InterruptedException {

        Thread.sleep(5000); // Wait until the error message disappear

        //Action
        LanguageActions.ChangeLanguageTo(driver, Language.SCHI);

        cardRequestPage.clickContinueButton();


        //Assertion
        pushErrorMessage.isComponentExist(driver);
        WebElement errorMessage = pushErrorMessage.getElement(driver,"ErrorMessage_Text");
        Assert.assertEquals(errorMessage.getText(),"请选择至少一项");

    }





}
