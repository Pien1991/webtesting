package testScripts.Card;

import actions.LanguageActions;
import config.Components;
import config.Language;
import config.Site;
import dataTemplate.Component;
import dataTemplate.WebTestScript;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import remoteObject.pages.card.CardPage;
import util.ComponentHelper;
import util.configsHelper.ConfigHelper;
import util.parser.UsersParser;

/**
 * Created by shepardpin on 5/12/17.
 */
public class card_80 extends WebTestScript {


    CardPage cardPage;
    Component DetailsPanel = ComponentHelper.getComponent(Components.CARD_DETAIL_PANEL);
    WebDriverWait wait;

    /**
     * Pre-Conditions :
     * 1. User own ONLY ONE RHA Virtual card , where the card_statues  = ACTIVATED  and kyc2status = KYC_REQUIRED .
     */


    @BeforeClass
    @Override
    public void TestcasePreconditionTest() {
        wait = new WebDriverWait(driver, 2);
        site = Site.ANX;
        user = UsersParser.getUserByAlias("Virtual_After_Activation_Before_Upgrade_Test", testEnv, site);
    }


    /**
     * Actions :
     *  1.  Login and Navigate to /card page .
     *  2.  Click 'Daily Limit' button
     * Expected Results :
     *  1. The Card Detail will change to Current Daily Limit ;
     *
     *
     */


    @Test
    public void Step_1() throws InterruptedException {
        ConfigHelper.loginBeforeTestStart(driver, user, site, testEnv);
        cardPage = new CardPage(driver, testEnv, site);
        LanguageActions.ChangeLanguageTo(driver, Language.ENG);

        //Wait until the card is proper loaded .
        Thread.sleep(2000);

        //Assertions :
        cardPage.clickFreeUpgradeButton();

        String BasicLifeTimeLimitAmount = DetailsPanel.getElement(driver, "BasicLifeTimeLimit_Amount").getText();
        Assert.assertEquals(BasicLifeTimeLimitAmount, "2,500 USD");

        String BasicMaxDailyLimitAmount = DetailsPanel.getElement(driver, "BasicMaxDailyLimit_Amount").getText();
        Assert.assertEquals(BasicMaxDailyLimitAmount, "2,500 USD");

        String BasicSingleLimitAmount = DetailsPanel.getElement(driver, "BasicSingleLimit_Amount").getText();
        Assert.assertEquals(BasicSingleLimitAmount, "2,500 USD");

        String ProLifeTimeLimitAmount = DetailsPanel.getElement(driver, "ProLifeTimeLimit_Amount").getText();
        Assert.assertEquals(ProLifeTimeLimitAmount, "No limit");

        String ProMaxDailyLimitAmount = DetailsPanel.getElement(driver, "ProMaxDailyLimit_Amount").getText();
        Assert.assertEquals(ProMaxDailyLimitAmount, "20,000 USD");

        String ProSingleLimitAmount = DetailsPanel.getElement(driver, "ProSingleLimit_Amount").getText();
        Assert.assertEquals(ProSingleLimitAmount, "10,000 USD");

        //Assertion on Existence of Current mark in Basic Level Panel
        DetailsPanel.getElement(driver, "BasicCurrent_Text");

        //Assertion on Existence of Back Button
        DetailsPanel.getElement(driver, "Back_Button");

        //Assertion on Existence of Upgrade Confirm Button
        DetailsPanel.getElement(driver, "UpgradeConfirm_Button");

        //Assertion on Non Existence of Current mark in Pro Level Panel
        try {
            DetailsPanel.getElement(driver, "ProCurrent_Text");
        } catch (WebDriverException e) {
        }

    }
}
