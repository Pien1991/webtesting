package testScripts.Card;

import config.Components;
import config.Site;
import dataTemplate.Component;
import dataTemplate.WebTestScript;
import org.openqa.selenium.WebDriverException;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import remoteObject.pages.card_request.CardRequestPage;
import util.ComponentHelper;
import util.configsHelper.ConfigHelper;
import util.parser.UsersParser;

/**
 * Created by shepardpin on 5/4/17.
 */
public class card_13 extends WebTestScript{

    CardRequestPage cardRequestPage ;
    Component CardNavigationBar = ComponentHelper.getComponent(Components.CARD_NAVIGATION_BAR);
    Component SelectCardPanel = ComponentHelper.getComponent(Components.REQUEST_SELECT_CARD_PANEL);

    /**
     *  Pre-Conditions :
     *  1. User is VERIFIED .
     *  2. The tested website is ANXPRO .
     *  3. User does NOT have any card .
     */

    @BeforeClass
    @Override
    public void TestcasePreconditionTest() {
        site= Site.ANX;
        user= UsersParser.getUserByAlias("Verified_NoCards_NoOTP_User",testEnv,site);

    }

    /**
     *  Actions :
     *  1. Login and navigate to /card/request page
     *
     *  Expected Results :
     *  1. The request status is in "Select Card" .
     *  2. ONLY a RED "REQUEST CARDS" button near "DEBIT CARDS" title .
     */

    @Test
    public void Step_1(){

        // Actions
        ConfigHelper.loginBeforeTestStart(driver,user,site,testEnv);
        cardRequestPage = new CardRequestPage(driver,testEnv,site);

        //Expected Results
        SelectCardPanel.isComponentExist(driver);
        CardNavigationBar.getElement(driver,"CardPage_Title");
        CardNavigationBar.getElement(driver,"CardRequestPage_Link");
        try {
            CardNavigationBar.getElement(driver,"CardPage_Link");
            Assert.fail();
        }catch (WebDriverException e){

        }

    }



}
