package testScripts.Card;

import config.Components;
import config.Site;
import dataTemplate.Component;
import dataTemplate.WebTestScript;
import dataTemplate.tables.CardOrderTable;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import remoteObject.pages.card_request.CardRequestPage;
import util.ComponentHelper;
import util.configsHelper.ConfigHelper;
import util.parser.UsersParser;

/**
 * Created by shepardpin on 5/11/17.
 */
public class card_29 extends WebTestScript {

    CardRequestPage cardRequestPage ;

    Component eliteCard = ComponentHelper.getComponent(Components.REQUEST_ELITE_CARD);

    Component USDPremiumCard = ComponentHelper.getComponent(Components.REQUEST_USD_PREMIUM_CARD);


    @BeforeClass
    @Override
    public void TestcasePreconditionTest() {
        site = Site.ANX;
        user = UsersParser.getUserByAlias("Verified_NoCards_NoOTP_User", testEnv, site);
    }

    /**
     *  Actions :
     *  1. Login ANXPRO and Navigate to /card/request page .
     *  2. Select "ATM Card" as Card Type and "2" as Amount for ANX BTC Elite Card .
     *  3.  Select "ATM Card" as Card Type and "1" as Amount for ANX BTC Premium Card .
     *  4. Click "CONTINUE" button to "Delivery"  stage.
     *  5. Click "CONTINUE" button to "Confirmation" stage.
     *
     *  Expected Result:
     *  1. In Confirmation table :
     *          Item	                Quantity	        Price	    Cancel
     *    ANX BTC Elite (USD)	            1	            0.05 BTC	        X
     *      ANX BTC Premium (USD)	        1	            0.05 BTC	        X
     *      Registered Mail 	                            0.04 BTC
     *      Total Order Cost	                            0.11 BTC
     *
     */

    @Test
    public void Step_1() throws InterruptedException {

        //Action
        ConfigHelper.loginBeforeTestStart(driver,user,site,testEnv);
        cardRequestPage = new CardRequestPage(driver,testEnv,site);

        cardRequestPage.selectCardAmount(eliteCard,1);
        cardRequestPage.selectPhysical(eliteCard);

        cardRequestPage.selectCardAmount(USDPremiumCard,1);


        cardRequestPage.clickContinueButton();
        cardRequestPage.clickContinueButton();

        //Assertion
        CardOrderTable cardOrderTable = cardRequestPage.getCardOrderTable();

        Thread.sleep(3000); // Only for waiting the resposne of total fee reqeust  .

        Assert.assertEquals(cardOrderTable.getTotalOrderCost(),"0.11 BTC");


    }
}
