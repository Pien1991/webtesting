package testScripts.Card;

import config.Components;
import config.Site;
import dataTemplate.Component;
import dataTemplate.WebTestScript;
import dataTemplate.tables.CardOrderTable;
import org.openqa.selenium.WebDriverException;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import remoteObject.pages.card_request.CardRequestPage;
import util.ComponentHelper;
import util.configsHelper.ConfigHelper;
import util.parser.UsersParser;

/**
 * Created by shepardpin on 5/11/17.
 */
public class card_28 extends WebTestScript {

    CardRequestPage cardRequestPage ;

    Component eliteCard = ComponentHelper.getComponent(Components.REQUEST_ELITE_CARD);
    Component selectCardPanel = ComponentHelper.getComponent(Components.REQUEST_SELECT_CARD_PANEL);
    Component deliveryPanel = ComponentHelper.getComponent(Components.REQUEST_CARD_DELIVERY_PANEL);
    Component confirmationPanel = ComponentHelper.getComponent(Components.REQUEST_CARD_CONFIRM_PANEL);


    @BeforeClass
    @Override
    public void TestcasePreconditionTest() {
        site = Site.ANX;
        user = UsersParser.getUserByAlias("Verified_NoCards_NoOTP_User", testEnv, site);
    }


    /**
     *  Actions :
     *  1. Login ANXPRO and Navigate to /card/request page .
     *  2. Select "ATM Card" as Card Type and "1" as Amount for ANX BTC Elite Card .
     *  4. Click "CONTINUE" button to "Delivery"  stage.
     *  5. Click "CONTINUE" button to "Confirmation" stage.
     *  6. Click "X" for ANX BTC Elite Card .
     *
     *  Expected Result:
     *  1. The browser will jump to "Select Card" stage  .
     *
     */

    @Test
    public void Step_1(){

        //Action
        ConfigHelper.loginBeforeTestStart(driver,user,site,testEnv);
        cardRequestPage = new CardRequestPage(driver,testEnv,site);

        cardRequestPage.selectCardAmount(eliteCard,1);
        cardRequestPage.selectPhysical(eliteCard);

        cardRequestPage.clickContinueButton();
        cardRequestPage.clickContinueButton();

        CardOrderTable cardOrderTable = cardRequestPage.getCardOrderTable();
        cardOrderTable.cancelOrderedCard("Elite");


        //Assertion
        selectCardPanel.isComponentExist(driver);

        try {
            deliveryPanel.isComponentExist(driver);
            Assert.fail("Delivery Panel should NOT exist");
        }catch (WebDriverException e){

        }

        try {
            confirmationPanel.isComponentExist(driver);
            Assert.fail("Confirmation Panel should NOT exist");
        }catch (WebDriverException e){

        }


    }

}
