package testScripts.Card;

import actions.LanguageActions;
import config.Components;
import config.Language;
import config.Site;
import dataTemplate.Component;
import dataTemplate.WebTestScript;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import remoteObject.pages.card.CardPage;
import util.ComponentHelper;
import util.configsHelper.ConfigHelper;
import util.parser.UsersParser;

/**
 * Created by shepardpin on 5/8/17.
 */
public class card_50 extends WebTestScript {

    CardPage cardPage;
    Component DetailsPanel = ComponentHelper.getComponent(Components.CARD_DETAIL_PANEL);
    WebDriverWait wait;

    /**
     * Pre-Conditions :
     * 1. User own ONLY ONE RHA ATM card , where the card_statues  = BLANK  and kyc2status = KYC_REQUIRED .
     * 2. The tested website is ANXPRO .
     * 3. For RHA virtual card , the network_type = VISA in card_type table .
     * 4. User 's TOTAL balance is 1 BTC  and it's AVAILABLE balance is 0.5 BTC .
     */
    @BeforeClass
    @Override
    public void TestcasePreconditionTest() {
        wait = new WebDriverWait(driver, 2);
        site = Site.ANX;
        user = UsersParser.getUserByAlias("Physical_Before_KYC2_Test", testEnv, site);
    }


    /**
     * Actions :
     * 1. Login and Navigate to /card page .
     * <p>
     * Expected Results :
     * 1. The card details should  show as :
     *      Network             :   VISA
     *      Available Balance   :   0.50000000 BTC
     *      Current Status	    :   GET VERIFIED
     *      Card Type	        :   ATM Card
     * 2. The Available Balance should NOT point to total balance .
     *
     */

    @Test
    public void Step_1() {
        //Actions :
        ConfigHelper.loginBeforeTestStart(driver, user, site, testEnv);
        cardPage = new CardPage(driver, testEnv, site);
        LanguageActions.ChangeLanguageTo(driver, Language.ENG);

        //Assertions :
        DetailsPanel.isComponentExist(driver);


        //Assertion on Correctness and Existence of Network image .
        WebElement NetworkImage = DetailsPanel.getElement(driver, "Network_Image");
        // Check the src to verify whether the image is correct
        wait.until(ExpectedConditions.attributeToBe(NetworkImage, "anx-src", "/images/Logo_Visa.svg"));


        //Assertion on Correctness and Existence of Available Balance amount .
        WebElement AvailableBalanceAmount = DetailsPanel.getElement(driver, "AvailableBalance_Amount");
        wait.until(ExpectedConditions.textToBePresentInElement(AvailableBalanceAmount, "0.50000000"));

        //Assertion on Correctness and Existence of Available Balance unit .
        WebElement AvailableBalanceUnit = DetailsPanel.getElement(driver, "AvailableBalance_Unit");
        wait.until(ExpectedConditions.textToBePresentInElement(AvailableBalanceUnit, "BTC"));



        //Assertion on Existence of Get Verified button
        DetailsPanel.getElement(driver, "GetVerified_Button");

        //Assertion on NON-Existence of ACTIVATE NOW button
        try {
            DetailsPanel.getElement(driver, "Activate_Button");
            Assert.fail("The 'ACTIVATE' Button should NOT exist");
        } catch (WebDriverException e) {
        }


        //Assertion on NON-Existence of card status information
        try {
            DetailsPanel.getElement(driver, "CardStatus_Text");
            Assert.fail("The card status information should NOT exist");
        } catch (WebDriverException e) {
        }


        //Assertion on NON-Existence of  Card Limit Level
        try {
            DetailsPanel.getElement(driver, "CardLimitLevel_Column");
            Assert.fail("The 'Card Limit Level' column should NOT exist");
        } catch (WebDriverException e) {
        }



        //Assertion on Correctness and Existence of Card Type information
        WebElement CardTypeText = DetailsPanel.getElement(driver, "CardType_Text");
        wait.until(ExpectedConditions.textToBePresentInElement(CardTypeText, "ATM Card"));

        //Assertion on NON-Existence of Advanced Setting
        try {
            DetailsPanel.getElement(driver, "AdvancedSettings_Column");
            Assert.fail("The 'Advanced Settings' column should NOT exist");
        } catch (WebDriverException e) {
        }

    }

    /**
     * Actions :
     * 1. Use mouse to hover the "?" icon inside Current Status near "GET VERIFIED" button
     * <p>
     * Expected Results :
     * 1. A message should show up , please refer to picture :
     *      "
     *          Please submit proof of address so that we can continue with your application.
     *          Proof of address must have been issued within the last three months.
     *      "
     */
    @Test(dependsOnMethods = "Step_1")
    public void Step_2() {
        //Assertion on Existence of "?" icon .
        WebElement QuestionMark_Button =  DetailsPanel.getElement(driver,"GetVerifiedQuestion_Button");

        //Action : hover the "?" icon
        Actions action = new Actions(driver);
        action.moveToElement(QuestionMark_Button).perform();

        //Assertion on Visibility and Existence of Extra Card Info
        WebElement ExtraKYCText = DetailsPanel.getElement(driver,"ExtraKYC_Text");
        wait.until(ExpectedConditions.visibilityOf(ExtraKYCText));

        //Assertion on Correctness of Extra Card Info
        String ExText = "Please submit proof of address so that we can continue with your application. Proof of address must have been issued within the last three months.";
        Assert.assertEquals(ExtraKYCText.getText(),ExText);
    }


    /**
     * Actions :
     * 1. Change to Simplified Chinese .
     * 2. Use mouse to hover the "?" icon inside Card Type Status near "ATM Card"
     * <p>
     * Expected Results :
     * 1. A message should show up , please refer to picture :
     *      "
     *          ATM Card is a physical card.
     *          You can use it for online and Point-of-sales (POS) payments and withdraw cash via ATM machines that support VISA network.
     *      "
     */
    @Test(dependsOnMethods = "Step_1")
    public void Step_3() {

        //Assertion on Existence of "?" icon .
        WebElement QuestionMark_Button =  DetailsPanel.getElement(driver,"CardTypeQuestion_Button");

        //Action : hover the "?" icon
        Actions action = new Actions(driver);
        action.moveToElement(QuestionMark_Button).perform();

        //Assertion on Visibility and Existence of Extra Card Info
        WebElement ExtraCardInfoText = DetailsPanel.getElement(driver,"ExtraCardInfo_Text");
        wait.until(ExpectedConditions.visibilityOf(ExtraCardInfoText));

        //Assertion on Correctness of Extra Card Info
        String ExText = "ATM Card is a physical card. You can use it for online and Point-of-sales (POS) payments and withdraw cash via ATM machines that support VISA network.";
        Assert.assertEquals(ExtraCardInfoText.getText(),ExText);
    }


}
