package testScripts.Card;

import actions.LanguageActions;
import config.Components;
import config.Language;
import config.Page;
import config.Site;
import dataTemplate.Component;
import dataTemplate.WebTestScript;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import remoteObject.pages.card.CardPage;
import util.ComponentHelper;
import util.configsHelper.ConfigHelper;
import util.navigationHelper.NavigationHelper;
import util.parser.UsersParser;

/**
 * Created by shepardpin on 5/8/17.
 */
public class card_47 extends WebTestScript {


    CardPage cardPage;
    Component DetailsPanel = ComponentHelper.getComponent(Components.CARD_DETAIL_PANEL);
    WebDriverWait wait ;

    /**
     * Pre-Conditions :
     * 1. User own ONLY ONE RHA Virtual card , where the card_statues  = PENDING_ACTIVATION  and kyc2status = KYC_REQUIRED .
     * 2. The tested website is ANXPRO .
     * 3. For RHA virtual card , the network_type = VISA in card_type table .
     * 4. User 's TOTAL balance is 1 BTC  and it's AVAILABLE balance is 0.5 BTC .
     */

    @BeforeClass
    @Override
    public void TestcasePreconditionTest() {
        wait = new WebDriverWait(driver, 2);
        site = Site.ANX;
        user = UsersParser.getUserByAlias("Virtual_Before_Activation_Test", testEnv, site);
    }


    /**
     * Actions :
     * 1. Login and Navigate to /card page .
     *
     * Expected Results :
     * 1. The card details should  show as :
     *      Network             :   VISA
     *      Available Balance   :   0.50000000 BTC
     *      Current Status	    :   ACTIVATE NOW
     *      Card Type	        :   Digital Card
     * 2. The Current Status should NOT exist card status information .
     * 3. The Card Limit Level should NOT exist .
     * 4. The Advanced Setting should NOT exist .
     */

    @Test
    public void Step_1() {
        //Actions :
        ConfigHelper.loginBeforeTestStart(driver, user, site, testEnv);
        cardPage = new CardPage(driver,testEnv,site);
        LanguageActions.ChangeLanguageTo(driver, Language.ENG);


        //Assertions :
        DetailsPanel.isComponentExist(driver);

        //Assertion on Correctness and Existence of Network image .
        WebElement NetworkImage = DetailsPanel.getElement(driver, "Network_Image");
        // Check the src to verify whether the image is correct
        wait.until(ExpectedConditions.attributeToBe(NetworkImage, "anx-src", "/images/Logo_Visa.svg"));

        //Assertion on Correctness and Existence of Available Balance amount .
        WebElement AvailableBalanceAmount = DetailsPanel.getElement(driver, "AvailableBalance_Amount");
        wait.until(ExpectedConditions.textToBePresentInElement(AvailableBalanceAmount, "0.50000000"));

        //Assertion on Correctness and Existence of Available Balance unit .
        WebElement AvailableBalanceUnit = DetailsPanel.getElement(driver, "AvailableBalance_Unit");
        wait.until(ExpectedConditions.textToBePresentInElement(AvailableBalanceUnit, "BTC"));

        //Assertion on Existence of ACTIVATE NOW button
        DetailsPanel.getElement(driver, "Activate_Button");

        //Assertion on NON-Existence of  Get Verified Button
        try {
            DetailsPanel.getElement(driver, "GetVerified_Button");
            Assert.fail("The 'Get Verified' Button should NOT exist");
        } catch (WebDriverException e) {
        }


        //Assertion on NON-Existence of card status information
        try {
            DetailsPanel.getElement(driver, "CardStatus_Text");
            Assert.fail("The card status information should NOT exist");
        } catch (WebDriverException e) {
        }


        //Assertion on NON-Existence of  Card Limit Level
        try {
            DetailsPanel.getElement(driver, "CardLimitLevel_Column");
            Assert.fail("The card limit level column should NOT exist");
        } catch (WebDriverException e) {
        }



        //Assertion on Correctness and Existence of Card Type information
        WebElement CardTypeText = DetailsPanel.getElement(driver, "CardType_Text");
        wait.until(ExpectedConditions.textToBePresentInElement(CardTypeText, "Digital Card"));

        //Assertion on NON-Existence of Advanced Setting
        try {
            DetailsPanel.getElement(driver, "AdvancedSettings_Column");
            Assert.fail("The advanced settings column should NOT exist");
        } catch (WebDriverException e) {
        }



    }


    /**
     * Actions :
     * 1. Check whether the "?" button exist
     * 2. Use mouse to hover the "?" icon inside Card Type near Digital Card .
     * <p>
     * Expected Results :
     * 1. A message should show up , please refer to picture :
     *      "
     *          Digital Card is a virtual card. You can get the card instantly and use it for online payments.
     *          Card details (card number, expiration date and CVV) will be available after activation.
     *      "
     */
    @Test(dependsOnMethods = "Step_1")
    public void Step_2() {
        //Assertion on Existence of "?" icon .
        WebElement QuestionMark_Button =  DetailsPanel.getElement(driver,"CardTypeQuestion_Button");

        //Action : hover the "?" icon
        Actions action = new Actions(driver);
        action.moveToElement(QuestionMark_Button).perform();

        //Assertion on Visibility and Existence of Extra Card Info
        WebElement ExtraCardInfoText = DetailsPanel.getElement(driver,"ExtraCardInfo_Text");
        wait.until(ExpectedConditions.visibilityOf(ExtraCardInfoText));

        //Assertion on Correctness of Extra Card Info
        String ExText = "Digital Card is a virtual card. You can get the card instantly and use it for online payments. Card details (card number, expiration date and CVV) will be available after activation.";
        Assert.assertEquals(ExtraCardInfoText.getText(),ExText);
    }

    /**
     * Actions :
     * 1. Change to Traditional Chinese
     * 2. Check whether the "?" button exist
     * 3. Use mouse to hover the "?" icon inside Card Type near Digital Card .
     * <p>
     * Expected Results :
     * 1. A message should show up , please refer to picture :
     *      "
     *          網絡金融卡是是電子卡，它是實時發出並可用於網絡消費。卡資料（包括卡號、到期日、驗證碼）將在卡激活後可供查閱。
     *      "
     */
    @Test(dependsOnMethods = "Step_1")
    public void Step_3() {
        //Actions:
        LanguageActions.ChangeLanguageTo(driver,Language.TCHI);

        //Assertion on Existence of "?" icon .
        WebElement QuestionMark_Button =  DetailsPanel.getElement(driver,"CardTypeQuestion_Button");

        //Action : hover the "?" icon
        Actions action = new Actions(driver);
        action.moveToElement(QuestionMark_Button).perform();

        //Assertion on Visibility and Existence of Extra Card Info
        WebElement ExtraCardInfoText = DetailsPanel.getElement(driver,"ExtraCardInfo_Text");
        wait.until(ExpectedConditions.visibilityOf(ExtraCardInfoText));

        //Assertion on Correctness of Extra Card Info
        String ExText = "網絡金融卡是是電子卡，它是實時發出並可用於網絡消費。卡資料（包括卡號、到期日、驗證碼）將在卡激活後可供查閱。";
        Assert.assertEquals(ExtraCardInfoText.getText(),ExText);

    }
}