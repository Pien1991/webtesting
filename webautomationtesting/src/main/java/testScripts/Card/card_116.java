package testScripts.Card;

import actions.LanguageActions;
import config.Components;
import config.Language;
import config.Site;
import dataTemplate.Component;
import dataTemplate.WebTestScript;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import remoteObject.pages.card.CardPage;
import util.ComponentHelper;
import util.configsHelper.ConfigHelper;
import util.parser.UsersParser;

/**
 * Created by shepardpin on 5/15/17.
 */
public class card_116 extends WebTestScript {


    CardPage cardPage;
    Component cardFundPanel = ComponentHelper.getComponent(Components.CARD_FUNDS_PANEL);


    /**
     * Pre-Conditions :
     * 1.  User own ONLY ONE Non-RHA activated card  (In OAT , assume to be Plus card)
     * 2.  In card_type table , the top_up_enabled = false the tested card .
     */
    @BeforeClass
    @Override
    public void TestcasePreconditionTest() {
        site = Site.ANX;
        user = UsersParser.getUserByAlias("PlusCardTest", testEnv, site);
    }

    /**
     * Actions :
     * 1. Login ANXPRO and Navigate to /card page  .
     * <p>
     * Expected Results :
     * 1. The message in RECHARGE area should be :
     *      Recharge function is currently disabled. Please contact cards@anxpro.com
     */


    @Test
    public void Step_1() {
        //Actions :
        ConfigHelper.loginBeforeTestStart(driver, user, site, testEnv);
        cardPage = new CardPage(driver, testEnv, site);
        LanguageActions.ChangeLanguageTo(driver, Language.ENG);

        //Assertions :
        cardFundPanel.getElement(driver,"DisableDRecharge_Image");

        //Correctness of message in RECHARGE area:
        WebElement message = cardFundPanel.getElement(driver, "DisableDRecharge_Message");
        String ExText = "Recharge function is currently disabled.\nPlease contact cards@anxpro.com";
        Assert.assertEquals(message.getText(),ExText);
    }


}
