package testScripts.Card;

import config.Components;
import config.Page;
import config.Site;
import dataTemplate.Component;
import dataTemplate.WebTestScript;
import org.openqa.selenium.WebDriverException;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import remoteObject.pages.card.CardPage;
import util.ComponentHelper;
import util.configsHelper.ConfigHelper;
import util.navigationHelper.NavigationHelper;
import util.parser.UsersParser;

/**
 * Created by shepardpin on 5/5/17.
 */
public class card_8 extends WebTestScript{

    CardPage cardPage;
    Component AdvertisementPanel = ComponentHelper.getComponent(Components.CARD_ADVERTISEMENT_PANEL);
    Component CardNavigationBar = ComponentHelper.getComponent(Components.CARD_NAVIGATION_BAR);


    /**
     *  Pre-Conditions :
     *  1. The verification_state in user table for that user should be PENDING_VERIFICATION .
     *  2. The tested website is ANXPRO .
     */

    @BeforeClass
    @Override
    public void TestcasePreconditionTest() {
        site= Site.ANX;
        user= UsersParser.getUserByAlias("PendingVerification_NoCard_User",testEnv,site);
    }

    /**
     *  Actions :
     *  1. Login and navigate to /card page
     *
     *  Expected Results :
     *  1. The advertisement panel should exist .
     *
     */

    @Test
    public void Step_1(){
        ConfigHelper.loginBeforeTestStart(driver,user,site,testEnv);
        cardPage = new CardPage(driver,testEnv,site);

        //Assertions :
        AdvertisementPanel.isComponentExist(driver);

    }

    /**
     *  Actions :
     *  1. Check the button near the "Debit Card" title
     *
     *  Expected Results :
     *  2. ONLY a RED "REQUEST CARDS" button near "DEBIT CARDS" title .
     *
     */

    @Test(dependsOnMethods = "Step_1")
    public void Step_2(){

        //Expected Results :
        CardNavigationBar.getElement(driver,"CardPage_Title");
        CardNavigationBar.getElement(driver,"CardRequestPage_Link");
        try {
            CardNavigationBar.getElement(driver,"CardPage_Link");
            Assert.fail();
        }catch (WebDriverException e){

        }

    }

}
