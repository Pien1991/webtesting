package testScripts.Card;

import actions.HeaderActions;
import actions.LanguageActions;
import config.Components;
import config.Language;
import config.Site;
import dataTemplate.Component;
import dataTemplate.WebTestScript;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import util.ComponentHelper;
import util.configsHelper.ConfigHelper;
import util.parser.UsersParser;

/**
 * Created by shepardpin on 5/4/17.
 */
public class card_5 extends WebTestScript {


    Component notificationPanel = ComponentHelper.getComponent(Components.CARD_NOTIFICATION_PANEL);

    /**
     *  Pre-Conditions :
     *  1. The user did not finish verification. Hence , the verification_state in user table for that user should be UNVERIFIED .
     *  2. The testing website is ANX.
     */

    @BeforeClass
    @Override
    public void TestcasePreconditionTest() {
        site= Site.ANX;
        user= UsersParser.getUserByAlias("Unverified_User",testEnv,site);
    }

    /**
     *  Actions :
     *  1. Login and navigate to /card page
     *  2. Click " Debit Card" button in header.
     *
     *  Expected Results :
     *  1. User navigates to /card page.
     *
     */

    @Test
    public void Step_1(){
        ConfigHelper.loginBeforeTestStart(driver,user,site,testEnv);
        HeaderActions.clickDebitCardButton(driver);

        //Expected result assertions
        String url = driver.getCurrentUrl();
        Assert.assertEquals(url.contains("/card"),true);
    }

    /**
     *  Actions :
     *  1. Change to English.
     *  2. Check the warning division
     *
     *  Expected Results :
     *  1. The warning should show :
     *  "Please complete our online verification process before applying for your debit card. Select verify below to begin the process."
     *  2. The wording inside button should show :
     *  "VERIFY NOW"
     *
     */

    @Test(dependsOnMethods = "Step_1")
    public void Step_2(){
        LanguageActions.ChangeLanguageTo(driver, Language.ENG);

        String actualWarning = notificationPanel.getElement(driver,"Notification_Message").getText();
        String expectedWarning = "Please complete our online verification process before applying for your debit card. Select verify below to begin the process.";

        Assert.assertEquals(actualWarning,expectedWarning);

        String actualButtonWording = notificationPanel.getElement(driver,"Verify_Button").getText();
        String expectedButtonWording = "VERIFY NOW";

        Assert.assertEquals(actualButtonWording,expectedButtonWording);

    }

}
