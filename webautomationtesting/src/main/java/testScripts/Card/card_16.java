package testScripts.Card;

import config.Components;
import config.Site;
import dataTemplate.Component;
import dataTemplate.WebTestScript;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ISelect;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import remoteObject.pages.card_request.CardRequestPage;
import util.ComponentHelper;
import util.configsHelper.ConfigHelper;
import util.parser.UsersParser;

/**
 * Created by shepardpin on 5/10/17.
 */
public class card_16 extends WebTestScript{

    CardRequestPage cardRequestPage ;

    Component eliteCard = ComponentHelper.getComponent(Components.REQUEST_ELITE_CARD);
    Component deliveryPanel = ComponentHelper.getComponent(Components.REQUEST_CARD_DELIVERY_PANEL);

    @BeforeClass
    @Override
    public void TestcasePreconditionTest() {
        site = Site.ANX;
        user = UsersParser.getUserByAlias("Verified_NoCards_NoOTP_User", testEnv, site);
    }


    /**
     *  Actions :
     *  1. Login to ANXPRO .
     *  2. Navigate to /card/request page.
     *  3. Select ONE NON-virtual card and click "CONTINUE" to next stage.
     *  4. Select "Registered Mail" .
     *
     *  Expected Result:
     *  1. The cost of Registered Mail should be 0.04 .
     *  2. The cost unit of Registered Mail should be BTC .
     *
     */

    @Test
    public void Step_1(){

        //Action
        ConfigHelper.loginBeforeTestStart(driver,user,site,testEnv);
        cardRequestPage = new CardRequestPage(driver,testEnv,site);

        cardRequestPage.selectCardAmount(eliteCard,1);
        cardRequestPage.selectPhysical(eliteCard);
        cardRequestPage.clickContinueButton();

        //Assertion
        deliveryPanel.isComponentExist(driver);
        ISelect deliveryMethodSelect = deliveryPanel.getSelect(driver,"DeliveryMethod_Select");

        WebElement diliverylOption = deliveryMethodSelect.getOptions().get(0);

        Assert.assertEquals(diliverylOption.getText(),"Registered Mail - 0.04 BTC");


    }



    /**
     *  Actions :
     *  1. Select "Express Registered Mail (Region A)" .
     *
     *  Expected Result:
     *  1. The cost of Registered Mail should be 0.17 .
     *  2. The cost unit of Registered Mail should be BTC .
     *
     */

    @Test(dependsOnMethods = "Step_1")
    public void Step_2(){

        //Action
        ISelect deliveryMethodSelect = deliveryPanel.getSelect(driver,"DeliveryMethod_Select");


        //Assertion

        WebElement diliverylOption = deliveryMethodSelect.getOptions().get(1);

        Assert.assertEquals(diliverylOption.getText(),"Express Registered Mail (Region A) - 0.17 BTC");


    }
}
