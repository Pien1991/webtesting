package testScripts.Card;

import actions.LanguageActions;
import config.Components;
import config.Language;
import config.Site;
import dataTemplate.Component;
import dataTemplate.WebTestScript;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import remoteObject.pages.card.CardPage;
import util.ComponentHelper;
import util.configsHelper.ConfigHelper;
import util.parser.UsersParser;

/**
 * Created by shepardpin on 5/12/17.
 */
public class card_92 extends WebTestScript {

    CardPage cardPage;
    Component DetailsPanel = ComponentHelper.getComponent(Components.CARD_DETAIL_PANEL);
    WebDriverWait wait;

    /**
     * Pre-Conditions :
     * 1. User own ONLY ONE RHA ATM card , where the card_statues  = BLANK  and kyc2status = KYC_SUBMITTED .
     * 2. The tested website is ANXPRO .
     * 3. For RHA virtual card , the network_type = VISA in card_type table .
     * 4. User 's TOTAL balance is 1 BTC  and it's AVAILABLE balance is 0.5 BTC .
     */
    @BeforeClass
    @Override
    public void TestcasePreconditionTest() {
        wait = new WebDriverWait(driver, 2);
        site = Site.ANX;
        user = UsersParser.getUserByAlias("Physical_After_KYC2_Before_Activation_Test", testEnv, site);
    }

    /**
     * Actions :
     * 1. Login and Navigate to /card page .
     * <p>
     * Expected Results :
     * 1. The card details should  show as :
     *      Network             :   VISA
     *      Available Balance   :   0.50000000 BTC
     *      Current Status	    :   ACTIVATE NOW button
     *      Card Type	        :   ATM Card
     * 2. The Available Balance should NOT point to total balance .
     *
     */

    @Test
    public void Step_1() {
        //Actions :
        ConfigHelper.loginBeforeTestStart(driver, user, site, testEnv);
        cardPage = new CardPage(driver, testEnv, site);
        LanguageActions.ChangeLanguageTo(driver, Language.ENG);

        //Assertions :
        DetailsPanel.isComponentExist(driver);


        //Assertion on Correctness and Existence of Network image .
        WebElement NetworkImage = DetailsPanel.getElement(driver, "Network_Image");
        // Check the src to verify whether the image is correct
        wait.until(ExpectedConditions.attributeToBe(NetworkImage, "anx-src", "/images/Logo_Visa.svg"));


        //Assertion on Correctness and Existence of Available Balance amount .
        WebElement AvailableBalanceAmount = DetailsPanel.getElement(driver, "AvailableBalance_Amount");
        wait.until(ExpectedConditions.textToBePresentInElement(AvailableBalanceAmount, "0.50000000"));

        //Assertion on Correctness and Existence of Available Balance unit .
        WebElement AvailableBalanceUnit = DetailsPanel.getElement(driver, "AvailableBalance_Unit");
        wait.until(ExpectedConditions.textToBePresentInElement(AvailableBalanceUnit, "BTC"));


        //Assertion on Existence of  ACTIVATE NOW Button
        DetailsPanel.getElement(driver, "Activate_Button");



        //Assertion on NON-Existence of  Get Verified Button
        try {
            DetailsPanel.getElement(driver, "GetVerified_Button");
            Assert.fail("The 'Get Verified' Button should NOT exist");
        } catch (WebDriverException e) {
        }


        //Assertion on NON-Existence of  Card Status Information
        try {
            DetailsPanel.getElement(driver, "CardStatus_Text");
            Assert.fail("The Card Status Information should NOT exist");
        } catch (WebDriverException e) {
        }


        //Assertion on NON-Existence of  Card Limit Level
        try {
            DetailsPanel.getElement(driver, "CardLimitLevel_Column");
            Assert.fail("The 'Card Limit Level' column should NOT exist");
        } catch (WebDriverException e) {
        }



        //Assertion on Correctness and Existence of Card Type information
        WebElement CardTypeText = DetailsPanel.getElement(driver, "CardType_Text");
        wait.until(ExpectedConditions.textToBePresentInElement(CardTypeText, "ATM Card"));

        //Assertion on NON-Existence of Advanced Setting
        try {
            DetailsPanel.getElement(driver, "AdvancedSettings_Column");
            Assert.fail("The 'Advanced Settings' column should NOT exist");
        } catch (WebDriverException e) {
        }

    }

    /**
     * Actions :
     *  1.  Change to Simplified Chinese .
     *  2.  Check "ACTIVATE NOW" button.
     *
     * Expected Results :
     * 1. Inside "ACTIVATE NOW" button , it should be : 激活新卡
     *
     */

    @Test(dependsOnMethods = "Step_1")
    public void Step_2() {
        //Actions :
        LanguageActions.ChangeLanguageTo(driver, Language.SCHI);

        //Assertions :
        WebElement ActivateButton  =  DetailsPanel.getElement(driver, "Activate_Button");
        Assert.assertEquals(ActivateButton.getText(),"激活新卡");

    }

    /**
     * Actions :
     *  1.  Change to Traditional Chinese .
     *  2.  Check "ACTIVATE NOW" button.
     *
     * Expected Results :
     *  1. Inside "ACTIVATE NOW" button , it should be : 激活新卡
     *
     */

    @Test(dependsOnMethods = "Step_1")
    public void Step_3() {
        //Actions :
        LanguageActions.ChangeLanguageTo(driver, Language.TCHI);

        //Assertions :
        WebElement ActivateButton  =  DetailsPanel.getElement(driver, "Activate_Button");
        Assert.assertEquals(ActivateButton.getText(),"激活新卡");

    }


}
