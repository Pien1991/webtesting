package testScripts.Card;

import config.Components;
import config.Site;
import dataTemplate.Component;
import dataTemplate.WebTestScript;
import dataTemplate.tables.CardOrderTable;
import org.openqa.selenium.WebDriverException;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import remoteObject.pages.card_request.CardRequestPage;
import util.ComponentHelper;
import util.configsHelper.ConfigHelper;
import util.parser.UsersParser;

/**
 * Created by shepardpin on 5/11/17.
 */
public class card_30 extends WebTestScript{

    CardRequestPage cardRequestPage ;

    Component eliteCard = ComponentHelper.getComponent(Components.REQUEST_ELITE_CARD);
    //It represents Premium card in DCEXE which the purchase unit is USD and Premium card in ANXPRO which the purchase unit is BTC
    Component USDPremiumCard = ComponentHelper.getComponent(Components.REQUEST_USD_PREMIUM_CARD);

    @BeforeClass
    @Override
    public void TestcasePreconditionTest() {
        site = Site.ANX;
        user = UsersParser.getUserByAlias("Verified_NoCards_NoOTP_User", testEnv, site);
    }


    /**
     *  Actions :
     *  1. Login ANXPRO and Navigate to /card/request page .
     *  2. Select "Digital Card" as Card Type and "1" as Amount for ANX BTC Elite Card .
     *  3.  Select "ATM Card" as Card Type and "1" as Amount for ANX BTC Premium Card .
     *  4. Click "CONTINUE" button to "Delivery"  stage.
     *  5. Click "CONTINUE" button to "Confirmation" stage.
     *  6. Click "X" for ANX BTC Premium Card .
     *
     *  Expected Result:
     *  1. In Confirmation table :
     *      -   The row should disappear for  ANX BTC Premium Card .
     *      -   Only ANX BTC Elite Card should exist .
     *      -   The Delivery method should change to "Free Shipping" with 0 BTC price
     *
     */

    @Test
    public void Step_1(){

        //Action
        ConfigHelper.loginBeforeTestStart(driver,user,site,testEnv);
        cardRequestPage = new CardRequestPage(driver,testEnv,site);

        cardRequestPage.selectCardAmount(eliteCard,1);
        cardRequestPage.selectVirtual(eliteCard);

        cardRequestPage.selectCardAmount(USDPremiumCard,1);


        cardRequestPage.clickContinueButton();
        cardRequestPage.clickContinueButton();

        //Assertion
        CardOrderTable cardOrderTable = cardRequestPage.getCardOrderTable();
        Assert.assertEquals(cardOrderTable.getTotalCardType(),2);
        Assert.assertEquals(cardOrderTable.getMailingMethodText(),"Registered Mail");

        cardOrderTable.cancelOrderedCard("Premium");

        Assert.assertEquals(cardOrderTable.getTotalCardType(),1);
        cardOrderTable.getItemName("Elite");
        Assert.assertEquals(cardOrderTable.getMailingMethodText(),"Free Shipping");

    }


}
