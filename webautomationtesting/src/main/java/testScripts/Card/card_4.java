package testScripts.Card;


import actions.HeaderActions;
import actions.LanguageActions;
import config.Components;
import config.Language;
import config.Site;
import dataTemplate.Component;
import dataTemplate.WebTestScript;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import util.ComponentHelper;
import util.configsHelper.ConfigHelper;
import util.parser.UsersParser;

/**
 * Created by shepardpin on 4/13/17.
 *
 *
 */
public class card_4 extends WebTestScript{



    Component notificationPanel = ComponentHelper.getComponent(Components.CARD_NOTIFICATION_PANEL);

    /**
     *  Pre-Conditions :
     *  1. The user did not finish verification. Hence , the verification_state in user table for that user should be UNVERIFIED .
     *  2. The testing website is NIG-EX.
     */

    @BeforeClass
    @Override
    public void TestcasePreconditionTest() {
        site= Site.NIG;
        user= UsersParser.getUserByAlias("Unverified_User",testEnv,site);
    }

    /**
     *  Actions :
     *  1. Login and navigate to /card page
     *  2. Click " Debit Card" button in header.
     *
     *  Expected Results :
     *  1. User navigates to /card page.
     *
     */

    @Test
    public void Step_1(){
        ConfigHelper.loginBeforeTestStart(driver,user,site,testEnv);
        HeaderActions.clickDebitCardButton(driver);

        //Expected result assertions
        String url = driver.getCurrentUrl();
        Assert.assertEquals(url.contains("/card"),true);
    }


    /**
     *  Actions :
     *  1. Change to Japanese.
     *  2. Check the warning division
     *
     *  Expected Results :
     *  1. The warning should show :
     *  "デビットカードのお申し込みされる前に、オンライン認証を完了させてください。オンライン認証を進めるため、「確認」ボタンをクリックしてください。"
     *  2. The wording inside button should show :
     *  "確認する"
     *
     */

    @Test(dependsOnMethods = "Step_1")
    public void Step_2(){
        LanguageActions.ChangeLanguageTo(driver,Language.JP);

        String actualWarning = notificationPanel.getElement(driver,"Notification_Message").getText();
        String expectedWarning = "デビットカードのお申し込みされる前に、オンライン認証を完了させてください。オンライン認証を進めるため、「確認」ボタンをクリックしてください。";

        Assert.assertEquals(actualWarning,expectedWarning);

        String actualButtonWording = notificationPanel.getElement(driver,"Verify_Button").getText();
        String expectedButtonWording = "確認する";

        Assert.assertEquals(actualButtonWording,expectedButtonWording);

    }

    /**
     *  Actions :
     *  1. Change to Simplified Chinese.
     *  2. Check the warning division
     *
     *  Expected Results :
     *  1. The warning should show :
     *  "申请您​​的签帐金融卡之前，请完成我们在线的验证过程。您可以按下面的按钮来进行验证您的帐户。"
     *  2. The wording inside button should show :
     *  "现在验证"
     *
     */
    @Test(dependsOnMethods = "Step_1")
    public void Step_3(){
        LanguageActions.ChangeLanguageTo(driver,Language.SCHI);

        String actualWarning = notificationPanel.getElement(driver,"Notification_Message").getText();
        String expectedWarning = "申请您的签帐金融卡之前，请完成我们在线的验证过程。您可以按下面的按钮来进行验证您的帐户。";

        Assert.assertEquals(actualWarning,expectedWarning);

        String actualButtonWording = notificationPanel.getElement(driver,"Verify_Button").getText();
        String expectedButtonWording = "现在验证";

        Assert.assertEquals(actualButtonWording,expectedButtonWording);

    }

    /**
     *  Actions :
     *  1. Change to Traditional Chinese.
     *  2. Check the warning division
     *
     *  Expected Results :
     *  1. The warning should show :
     *  "申請您​​的簽帳金融卡之前，請完成我們在線的驗證過程。您可以按下面的按鈕來進行驗證您的帳戶。"
     *  2. The wording inside button should show :
     *  "現在驗證"
     *
     */
    @Test(dependsOnMethods = "Step_1")
    public void Step_4(){
        LanguageActions.ChangeLanguageTo(driver,Language.TCHI);

        String actualWarning = notificationPanel.getElement(driver,"Notification_Message").getText();
        String expectedWarning = "申請您的簽帳金融卡之前，請完成我們在線的驗證過程。您可以按下面的按鈕來進行驗證您的帳戶。";

        Assert.assertEquals(actualWarning,expectedWarning);

        String actualButtonWording = notificationPanel.getElement(driver,"Verify_Button").getText();
        String expectedButtonWording = "現在驗證";

        Assert.assertEquals(actualButtonWording,expectedButtonWording);

    }

    /**
     *  Actions :
     *  1. Change to English.
     *  2. Check the warning division
     *
     *  Expected Results :
     *  1. The warning should show :
     *  "Please complete our online verification process before applying for your debit card. Select verify below to begin the process."
     *  2. The wording inside button should show :
     *  "VERIFY NOW"
     *
     */

    @Test(dependsOnMethods = "Step_1")
    public void Step_5(){
        LanguageActions.ChangeLanguageTo(driver,Language.ENG);

        String actualWarning = notificationPanel.getElement(driver,"Notification_Message").getText();
        String expectedWarning = "Please complete our online verification process before applying for your debit card. Select verify below to begin the process.";

        Assert.assertEquals(actualWarning,expectedWarning);

        String actualButtonWording = notificationPanel.getElement(driver,"Verify_Button").getText();
        String expectedButtonWording = "VERIFY NOW";

        Assert.assertEquals(actualButtonWording,expectedButtonWording);

    }

    /**
     *  Actions :
     *  1. Click "VERIFY NOW" button
     *
     *  Expected Results :
     *  1. User will navigate to /setting/verify
     *
     */

    @Test(dependsOnMethods = "Step_5")
    public void Step_6(){

        notificationPanel.getElement(driver,"Verify_Button").click();

        //Expected result assertions
        String url = driver.getCurrentUrl();
        new WebDriverWait(driver,2).until(ExpectedConditions.urlContains("/settings/verify"));

    }


}
