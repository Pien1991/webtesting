package testScripts.Card;


import actions.LanguageActions;
import config.Components;
import config.Language;
import config.Site;
import dataTemplate.Component;
import dataTemplate.WebTestScript;
import org.openqa.selenium.WebDriverException;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import remoteObject.pages.card.CardPage;
import util.ComponentHelper;
import util.configsHelper.ConfigHelper;
import util.parser.UsersParser;

/**
 * Created by shepardpin on 4/11/17.
 *
 */

public class card_9 extends WebTestScript{



    CardPage cardPage;
    Component notificationPanel = ComponentHelper.getComponent(Components.CARD_NOTIFICATION_PANEL);


    /**
     *  Pre-Conditions :
     *  1. The user did not finish verification. Hence , the verification_state in user table for that user should be UNVERIFIED .
     *  2. The testing website is NIG-EX .
     */


    @BeforeClass
    @Override
    public void TestcasePreconditionTest() {
        site= Site.NIG;
        user= UsersParser.getUserByAlias("Verified_NoCards_NoOTP_User",testEnv,site);
    }


    /**
     *  Actions :
     *  1. Login and navigate to /card page
     *
     *  Expected Results :
     *  1. The warning should show "アカウントが認証されました。"
     *  2. No 'Verify' button should exist inside card notification panel
     */

    @Test
    public void Step_1(){
        ConfigHelper.loginBeforeTestStart(driver,user,site,testEnv);
        cardPage = new CardPage(driver,testEnv,site);
        LanguageActions.ChangeLanguageTo(driver,Language.JP);

        String warning = notificationPanel.getElement(driver,"Notification_Message").getText();

        //Expected result assertions
        Assert.assertEquals(warning,"アカウントが認証されました。");

        try{
            notificationPanel.getElement(driver,"Verify_Button");
            Assert.fail("No 'Verify' button should exist");
        }catch (WebDriverException e){
        }
    }

    /**
     *  Actions :
     *  1. Change to ENGLISH.
     *  2. Check the warning text
     *
     *  Expected Results :
     *  1. The warning should show as : "Please note that your account has been verified."
     *
     */

    @Test(dependsOnMethods = "Step_1")
    public void Step_2(){
        LanguageActions.ChangeLanguageTo(driver,Language.ENG);
        String warning = notificationPanel.getElement(driver,"Notification_Message").getText();

        //Expected result assertions
        Assert.assertEquals(warning,"Please note that your account has been verified.");
    }


}
