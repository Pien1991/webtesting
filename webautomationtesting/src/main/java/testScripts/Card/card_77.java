package testScripts.Card;

import actions.LanguageActions;
import config.Components;
import config.Language;
import config.Site;
import dataTemplate.Component;
import dataTemplate.WebTestScript;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import remoteObject.pages.card.CardPage;
import util.ComponentHelper;
import util.configsHelper.ConfigHelper;
import util.parser.UsersParser;

/**
 * Created by shepardpin on 5/12/17.
 */
public class card_77 extends WebTestScript {

    CardPage cardPage;
    Component DetailsPanel = ComponentHelper.getComponent(Components.CARD_DETAIL_PANEL);
    WebDriverWait wait;

    /**
     * Pre-Conditions :
     * 1. User own ONLY ONE RHA ATM card , where the card_statues  = ACTIVATED  ,   kyc2status = KYC_APPROVED and identifer = 6362 .
     * 2. For RHA virtual card , the network_type = VISA in card_type table .
     * 3. User 's TOTAL balance is 1 BTC  and it's AVAILABLE balance is 5 BTC .
     */


    @BeforeClass
    @Override
    public void TestcasePreconditionTest() {
        wait = new WebDriverWait(driver, 2);
        site = Site.ANX;
        user = UsersParser.getUserByAlias("Physical_After_KYC2_After_Activation_Test", testEnv, site);
    }


    /**
     * Actions :
     * 1. Login and Navigate to /card page .
     * <p>
     * Expected Results :
     * 1. The card details should  show as :
     *      Network             :   VISA
     *      Available Balance   :   5.00000000 BTC
     *      Card Number         :   6362
     *      PIN                 :   Eye XXXX
     *      Expiry Date         :   03/20
     *      Card Limit Level	:   View  Pro
     *      Current Status	    :   ACTIVATED
     *      Card Type	        :   ATM Card
     *      Advanced Settings   :   Suspend | Daily Limit | Report Lost
     * 2. The Card Number should be the last four digit of Identifier which assumes to be 6362 ,
     * 3. Expiry Date is 03/20
     * 4. The Available Balance should NOT point to total balance .
     * 5. Card Limit Level should NOT show kyc status
     */


    @Test
    public void Step_1() {
        ConfigHelper.loginBeforeTestStart(driver, user, site, testEnv);
        cardPage = new CardPage(driver,testEnv,site);
        LanguageActions.ChangeLanguageTo(driver, Language.ENG);


        //Assertions :
        DetailsPanel.isComponentExist(driver);

        //Assertion on Correctness and Existence of Network image .
        WebElement NetworkImage = DetailsPanel.getElement(driver, "Network_Image");
        // Check the src to verify whether the image is correct
        wait.until(ExpectedConditions.attributeToBe(NetworkImage, "anx-src", "/images/Logo_Visa.svg"));

        //Assertion on Correctness and Existence of Available Balance amount .
        WebElement AvailableBalanceAmount = DetailsPanel.getElement(driver, "AvailableBalance_Amount");
        wait.until(ExpectedConditions.textToBePresentInElement(AvailableBalanceAmount, "5.00000000"));

        //Assertion on Correctness and Existence of Available Balance unit .
        WebElement AvailableBalanceUnit = DetailsPanel.getElement(driver, "AvailableBalance_Unit");
        wait.until(ExpectedConditions.textToBePresentInElement(AvailableBalanceUnit, "BTC"));

        //Assertion on Correctness and Existence of Card Number
        WebElement CardNumber_Amount = DetailsPanel.getElement(driver, "CardNumber_Amount");
        wait.until(ExpectedConditions.textToBePresentInElement(CardNumber_Amount, "0405"));


        //Assertion on NON - Existence of Eye button inside Card Number
        try {
            DetailsPanel.getElement(driver, "CardEye_Button");
            Assert.fail("The Eye button should NOT exist in Card Number column");
        } catch (WebDriverException e) {}


        //Assertion on Existence of Eye button inside PIN column
        DetailsPanel.getElement(driver, "PINEye_Button");


        //Assertion on Correctness and Existence of PIN number in PIN column
        WebElement PINNumber_Amount = DetailsPanel.getElement(driver, "PINNumber_Amount");
        wait.until(ExpectedConditions.textToBePresentInElement(PINNumber_Amount, "XXXX"));

        //Assertion on Correctness and Existence of Expiry Date number in Expiry Date column
        WebElement ExpiryDate_Amount = DetailsPanel.getElement(driver, "ExpiryDate_Amount");
        wait.until(ExpectedConditions.textToBePresentInElement(ExpiryDate_Amount, "05/20"));



        //Assertion on Correctness and Existence of Card Limit Level Status .
        WebElement LevelStatusText = DetailsPanel.getElement(driver, "LevelStatus_Text");
        wait.until(ExpectedConditions.textToBePresentInElement(LevelStatusText, "Pro"));

        //Assertion on Correctness and Existence of View Button .
        WebElement ViewButton = DetailsPanel.getElement(driver, "View_Button");
        wait.until(ExpectedConditions.textToBePresentInElement(ViewButton, "View"));


        //Assertion on NON-Existence of KYC Status Text .
        try {
            DetailsPanel.getElement(driver, "KycStatus_Text");
            Assert.fail();
        } catch (WebDriverException e) {}

        //Assertion on NON-Existence of Free Upgrade Button .
        try {
            DetailsPanel.getElement(driver, "FreeUpgrade_Button");
            Assert.fail();
        } catch (WebDriverException e) {}


        //Assertion on Correctness and Existence of ACTIVATE NOW button
        WebElement CardStatusText  = DetailsPanel.getElement(driver, "CardStatus_Text");
        wait.until(ExpectedConditions.textToBePresentInElement(CardStatusText, "Activated"));

        //Assertion on NON-Existence of ACTIVATE NOW button
        try {
            DetailsPanel.getElement(driver, "Activate_Button");
            Assert.fail();
        } catch (WebDriverException e) {}

        //Assertion on Correctness and Existence of Card Type information
        WebElement CardTypeText = DetailsPanel.getElement(driver, "CardType_Text");
        wait.until(ExpectedConditions.textToBePresentInElement(CardTypeText, "ATM Card"));


        //Assertion on Existence of Suspend Button
        DetailsPanel.getElement(driver, "Suspend_Button");
        //Assertion on Existence of Daily Limit Button
        DetailsPanel.getElement(driver, "DailyLimit_Button");
        //Assertion on Existence of Report Lost Button
        DetailsPanel.getElement(driver, "ReportLost_Button");

    }

}
