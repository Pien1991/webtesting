package testScripts.Card;

import actions.LanguageActions;
import config.Components;
import config.Language;
import config.Site;
import dataTemplate.Component;
import dataTemplate.WebTestScript;
import org.openqa.selenium.WebDriverException;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import remoteObject.pages.card_request.CardRequestPage;
import util.ComponentHelper;
import util.configsHelper.ConfigHelper;
import util.parser.UsersParser;

/**
 * Created by shepardpin on 5/10/17.
 */
public class card_18 extends WebTestScript {


    CardRequestPage cardRequestPage ;

    Component eliteCard = ComponentHelper.getComponent(Components.REQUEST_ELITE_CARD);
    Component deliveryPanel = ComponentHelper.getComponent(Components.REQUEST_CARD_DELIVERY_PANEL);

    @BeforeClass
    @Override
    public void TestcasePreconditionTest() {
        site = Site.ANX;
        user = UsersParser.getUserByAlias("Verified_NoCards_NoOTP_User", testEnv, site);
    }


    /**
     *  Actions :
     *  1.  Login ANXPRO and Navigate to /card/request page .
     *  2.  Select at least ONE ANX ELITE (USD) and click "Digital Card" as card type
     *  3.  Click "CONTINUE" button
     *
     *  Expected Result:
     *  1.  Message will show :
     *      "Digital Card will be issued instantly, you don't have to pay for delivery."
     *
     */

    @Test
    public void Step_1(){

        //Action
        ConfigHelper.loginBeforeTestStart(driver,user,site,testEnv);
        cardRequestPage = new CardRequestPage(driver,testEnv,site);

        cardRequestPage.selectCardAmount(eliteCard,1);
        cardRequestPage.selectVirtual(eliteCard);
        cardRequestPage.clickContinueButton();

        //Assertion
        WebElement virtualDeliveryMessage = deliveryPanel.getElement(driver,"VirtualDelivery_Message");
        Assert.assertEquals(virtualDeliveryMessage.getText(),"Digital Card will be issued instantly, you don't have to pay for delivery.");

        try{
            deliveryPanel.getElement(driver,"DeliveryMethod_Select");
            Assert.fail("The Delivery method selection should NOT exist");
        }catch (WebDriverException e){}


    }


    /**
     *  Actions :
     *  1. Change to Simplified Chinese .
     *
     *  Expected Result:
     *  1.  Message will show :
     *      "网络金融卡以电子形式发生，无需送货。"
     *
     */

    @Test(dependsOnMethods = "Step_1")
    public void Step_2(){

        //Action
        LanguageActions.ChangeLanguageTo(driver, Language.SCHI);

        //Assertion
        WebElement virtualDeliveryMessage = deliveryPanel.getElement(driver,"VirtualDelivery_Message");
        Assert.assertEquals(virtualDeliveryMessage.getText(),"网络金融卡以电子形式发生，无需送货。");

        try{
            deliveryPanel.getElement(driver,"DeliveryMethod_Select");
            Assert.fail("The Delivery method selection should NOT exist");
        }catch (WebDriverException e){}


    }




    /**
     *  Actions :
     *  1. Change to Traditional Chinese .
     *
     *  Expected Result:
     *  1.  Message will show :
     *      "网络金融卡以电子形式发生，无需送货。"
     *
     */

    @Test(dependsOnMethods = "Step_1")
    public void Step_3(){

        //Action
        LanguageActions.ChangeLanguageTo(driver, Language.TCHI);

        //Assertion
        WebElement virtualDeliveryMessage = deliveryPanel.getElement(driver,"VirtualDelivery_Message");
        Assert.assertEquals(virtualDeliveryMessage.getText(),"網絡金融卡以電子形式發生，無需送貨。");

        try{
            deliveryPanel.getElement(driver,"DeliveryMethod_Select");
            Assert.fail("The Delivery method selection should NOT exist");
        }catch (WebDriverException e){}


    }

}
