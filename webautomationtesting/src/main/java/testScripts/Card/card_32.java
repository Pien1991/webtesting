package testScripts.Card;

import actions.LanguageActions;
import config.Components;
import config.Site;
import dataTemplate.Component;
import dataTemplate.WebTestScript;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import remoteObject.pages.card_request.CardRequestPage;
import util.ComponentHelper;
import util.configsHelper.ConfigHelper;
import util.parser.UsersParser;


/**
 * Created by shepardpin on 5/10/17.
 */
public class card_32 extends WebTestScript {

    CardRequestPage cardRequestPage ;

    Component eliteCard = ComponentHelper.getComponent(Components.REQUEST_ELITE_CARD);

    //It represents Premium card in DCEXE which the purchase unit is USD and Premium card in ANXPRO which the purchase unit is BTC
    Component USDPremiumCard = ComponentHelper.getComponent(Components.REQUEST_USD_PREMIUM_CARD);


    @BeforeClass
    @Override
    public void TestcasePreconditionTest() {
        site = Site.ANX;
        user = UsersParser.getUserByAlias("Verified_NoCards_NoOTP_User", testEnv, site);
    }

    /**
     *  Actions :
     *  1. Login and Navigate to /card/request page .
     *  2. Select amount selection for ANX Premium .
     *
     *  Expected Result:
     *  1. The max amount of ANX Premium should be 5 .
     *
     *
     */

    @Test
    public void Step_1(){

        //Action
        ConfigHelper.loginBeforeTestStart(driver,user,site,testEnv);
        cardRequestPage = new CardRequestPage(driver,testEnv,site);


        try {
            cardRequestPage.selectCardAmount(USDPremiumCard,6);
            Assert.fail("The max card for Premium in ANXPRO should be 5.");
        }catch (Exception e){
        }

    }


}
