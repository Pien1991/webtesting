package testScripts.Card;

import config.Site;
import dataTemplate.WebTestScript;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import remoteObject.pages.card.CardPage;
import util.configsHelper.ConfigHelper;
import util.parser.UsersParser;

/**
 * Created by shepardpin on 5/12/17.
 */
public class card_121 extends WebTestScript {

    CardPage cardPage;

    /**
     * Pre-Conditions :
     * 1. User own ONLY TWO Non-RHA card with same card type , One of them is deactivated in card table where card_status = DEACTIVATED . Other is Non - deactivated which the card_status is NOT equal to DEACTIVATED
     * 2. The tested website is ANXPRO .
     * 3. The deactivated card 's alias is "Test Deactivated Card"
     * 4. The Non - deactivated card 's alias is "Test Non - Deactivated Card"
     */
    @BeforeClass
    @Override
    public void TestcasePreconditionTest() {
        site = Site.ANX;
        user = UsersParser.getUserByAlias("Deactivated_Non_RHA_Card_Test", testEnv, site);
    }


    /**
     * Actions :
     * 1. Login and Navigate to /card page .
     *
     * Expected Results :
     * 1. ONLY ONE card exist in /card page and the card's alias called "Test Non Deactivated Card"
     *
     */

    @Test
    public void Step_1() throws InterruptedException {
        //Actions :
        ConfigHelper.loginBeforeTestStart(driver, user, site, testEnv);
        cardPage = new CardPage(driver, testEnv, site);

//        Assertions :
        String currentCardName  = cardPage.getCurrentCardAlias(driver);
        Assert.assertEquals(currentCardName,"Test Non Deactivated Card");

        Assert.assertEquals(cardPage.getCardAmount(),1);

    }




    }
