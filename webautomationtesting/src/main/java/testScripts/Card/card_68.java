package testScripts.Card;

import actions.LanguageActions;
import config.Components;
import config.Language;
import config.Site;
import dataTemplate.Component;
import dataTemplate.WebTestScript;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import remoteObject.pages.card.CardPage;
import util.ComponentHelper;
import util.configsHelper.ConfigHelper;
import util.parser.UsersParser;

/**
 * Created by shepardpin on 5/8/17.
 */
public class card_68 extends WebTestScript {


    CardPage cardPage;
    Component DetailsPanel = ComponentHelper.getComponent(Components.CARD_DETAIL_PANEL);
    WebDriverWait wait;

    /**
     * Pre-Conditions :
     * 1. User own ONLY ONE RHA Virtual card , where the card_statues  = ACTIVATED  ,   kyc2status = KYC_APPROVED and identifer = 2520 .
     * 2. For RHA virtual card , the network_type = VISA in card_type table .
     * 3. User 's TOTAL balance is 1 BTC  and it's AVAILABLE balance is 0.5 BTC .
     */


    @BeforeClass
    @Override
    public void TestcasePreconditionTest() {
        wait = new WebDriverWait(driver, 2);
        site = Site.ANX;
        user = UsersParser.getUserByAlias("Virtual_After_Activation_After_Upgrade_Test", testEnv, site);
    }


    /**
     * Actions :
     * 1. Login and Navigate to /card page .
     * <p>
     * Expected Results :
     * 1. The card details should  show as :
     *      Network             :   VISA
     *      Available Balance   :   0.50000000 BTC
     *      Card Number         :   XXXX XXXX XXXX 2520
     *      Card Limit Level	:   View  Pro
     *      Current Status	    :   ACTIVATED
     *      Card Type	        :   Digital Card
     *      Advanced Settings   :   Suspend | Daily Limit | Report Lost
     * 2. The Card Number should be the Identifier which assumes to be XXXX XXXX XXXX 2520 , nearby there is "Eye" icon
     * 3. The Available Balance should NOT point to total balance .
     * 4. Card Limit Level should NOT show kyc status
     */


    @Test
    public void Step_1() {
        ConfigHelper.loginBeforeTestStart(driver, user, site, testEnv);
        cardPage = new CardPage(driver,testEnv,site);
        LanguageActions.ChangeLanguageTo(driver, Language.ENG);


        //Assertions :
        DetailsPanel.isComponentExist(driver);

        //Assertion on Correctness and Existence of Network image .
        WebElement NetworkImage = DetailsPanel.getElement(driver, "Network_Image");
        // Check the src to verify whether the image is correct
        wait.until(ExpectedConditions.attributeToBe(NetworkImage, "anx-src", "/images/Logo_Visa.svg"));

        //Assertion on Correctness and Existence of Available Balance amount .
        WebElement AvailableBalanceAmount = DetailsPanel.getElement(driver, "AvailableBalance_Amount");
        wait.until(ExpectedConditions.textToBePresentInElement(AvailableBalanceAmount, "0.50000000"));

        //Assertion on Correctness and Existence of Available Balance unit .
        WebElement AvailableBalanceUnit = DetailsPanel.getElement(driver, "AvailableBalance_Unit");
        wait.until(ExpectedConditions.textToBePresentInElement(AvailableBalanceUnit, "BTC"));

        //Assertion on Correctness and Existence of Card Number
        WebElement CardNumber_Amount = DetailsPanel.getElement(driver, "CardNumber_Amount");
        wait.until(ExpectedConditions.textToBePresentInElement(CardNumber_Amount, "XXXX XXXX XXXX 2520"));


        //Assertion on Existence of Eye button inside Card Number
        WebElement EyeButton = DetailsPanel.getElement(driver, "CardEye_Button");



        //Assertion on Correctness and Existence of Card Limit Level Status .
        WebElement LevelStatusText = DetailsPanel.getElement(driver, "LevelStatus_Text");
        wait.until(ExpectedConditions.textToBePresentInElement(LevelStatusText, "Pro"));

        //Assertion on Correctness and Existence of View Button .
        WebElement ViewButton = DetailsPanel.getElement(driver, "View_Button");
        wait.until(ExpectedConditions.textToBePresentInElement(ViewButton, "View"));


        //Assertion on NON-Existence of KYC Status Text .
        try {
            DetailsPanel.getElement(driver, "KycStatus_Text");
            Assert.fail();
        } catch (WebDriverException e) {}

        //Assertion on NON-Existence of Free Upgrade Button .
        try {
            DetailsPanel.getElement(driver, "FreeUpgrade_Button");
            Assert.fail();
        } catch (WebDriverException e) {}


        //Assertion on Correctness and Existence of ACTIVATE NOW button
        WebElement CardStatusText  = DetailsPanel.getElement(driver, "CardStatus_Text");
        wait.until(ExpectedConditions.textToBePresentInElement(CardStatusText, "Activated"));

        //Assertion on NON-Existence of ACTIVATE NOW button
        try {
            DetailsPanel.getElement(driver, "Activate_Button");
            Assert.fail();
        } catch (WebDriverException e) {}

        //Assertion on Correctness and Existence of Card Type information
        WebElement CardTypeText = DetailsPanel.getElement(driver, "CardType_Text");
        wait.until(ExpectedConditions.textToBePresentInElement(CardTypeText, "Digital Card"));


        //Assertion on Existence of Suspend Button
        DetailsPanel.getElement(driver, "Suspend_Button");
        //Assertion on Existence of Daily Limit Button
        DetailsPanel.getElement(driver, "DailyLimit_Button");
        //Assertion on Existence of Report Lost Button
        DetailsPanel.getElement(driver, "ReportLost_Button");

    }

    /**
     * Actions :
     * 1. Change to Simplified Chinese  to check the Card Limit Level .
     * <p>
     * Expected Results :
     * 1. Inside Card Limit Level , it should be :
     *     查看         尊尚
     */

    @Test(dependsOnMethods = "Step_1")
    public void Step_2() {
        //Action
        LanguageActions.ChangeLanguageTo(driver,Language.SCHI);

        //Assertion on Correctness and Existence of KYC Status Text .
        String ExText = "查看";
        WebElement ViewButton = DetailsPanel.getElement(driver, "View_Button");
        wait.until(ExpectedConditions.textToBePresentInElement(ViewButton, ExText));

        //Assertion on Correctness and Existence of Card Limit Level Status .
        WebElement LevelStatusText = DetailsPanel.getElement(driver, "LevelStatus_Text");
        wait.until(ExpectedConditions.textToBePresentInElement(LevelStatusText, "尊尚"));

    }

    /**
     * Actions :
     * 1. Change to Traditional Chinese  to check the Card Limit Level .
     * <p>
     * Expected Results :
     * 1. Inside Card Limit Level , it should be :
     *     查看         尊尚
     */

    @Test(dependsOnMethods = "Step_1")
    public void Step_3() {
        //Action
        LanguageActions.ChangeLanguageTo(driver,Language.TCHI);

        //Assertion on Correctness and Existence of KYC Status Text .
        String ExText = "查看";
        WebElement ViewButton = DetailsPanel.getElement(driver, "View_Button");
        wait.until(ExpectedConditions.textToBePresentInElement(ViewButton, ExText));

        //Assertion on Correctness and Existence of Card Limit Level Status .
        WebElement LevelStatusText = DetailsPanel.getElement(driver, "LevelStatus_Text");
        wait.until(ExpectedConditions.textToBePresentInElement(LevelStatusText, "尊尚"));

    }

    /**
     * Actions :
     * 1. Change to Japanese to check the Card Limit Level .
     * <p>
     * Expected Results :
     * 1. Inside Card Limit Level , it should be :
     *      詳しくは         プレミアム
     */

    @Test(dependsOnMethods = "Step_1")
    public void Step_4() {
        //Action
        LanguageActions.ChangeLanguageTo(driver,Language.JP);

        //Assertion on Correctness and Existence of KYC Status Text .
        String ExText = " 詳しくは";
        WebElement ViewButton = DetailsPanel.getElement(driver, "View_Button");
        wait.until(ExpectedConditions.textToBePresentInElement(ViewButton, ExText));

        //Assertion on Correctness and Existence of Card Limit Level Status .
        WebElement LevelStatusText = DetailsPanel.getElement(driver, "LevelStatus_Text");
        wait.until(ExpectedConditions.textToBePresentInElement(LevelStatusText, "プレミアム"));

    }


}
