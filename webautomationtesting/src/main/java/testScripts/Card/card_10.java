package testScripts.Card;

import config.Components;
import config.Page;
import config.Site;
import dataTemplate.Component;
import dataTemplate.WebTestScript;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import remoteObject.pages.card.CardPage;
import util.ComponentHelper;
import util.configsHelper.ConfigHelper;
import util.parser.UsersParser;

/**
 * Created by shepardpin on 5/5/17.
 */
public class card_10 extends WebTestScript{

    CardPage cardPage;
    Component CardNavigationBar = ComponentHelper.getComponent(Components.CARD_NAVIGATION_BAR);
    Component CardDetailPanel = ComponentHelper.getComponent(Components.CARD_DETAIL_PANEL); ;
    Component CardSelectionPanel = ComponentHelper.getComponent(Components.CARD_SELECTION_PANEL);;
    Component CardFundsPanel = ComponentHelper.getComponent(Components.CARD_FUNDS_PANEL);
    Component CardTransactionsPanel = ComponentHelper.getComponent(Components.CARD_TRANSACTIONS_PANEL);



    /**
     *  Pre-Conditions :
     *  1. The verification_state in user table for that user should be VERIFIED .
     *  2. User has AT LEAST ONE  activated card.
     */

    @BeforeClass
    @Override
    public void TestcasePreconditionTest() {
        site= Site.ANX;
        user= UsersParser.getUserByAlias("Virtual_Before_Activation_Test",testEnv,site);
    }

    /**
     *  Actions :
     *  1. Login and navigate to /card page.
     *
     *  Expected Results :
     *  1. Following divisions should exist:
     *      -   Card Selection Division
     *      -   Card Details Division
     *      -   Card Recharge Division
     *      -   Card Transactions Division

     */

    @Test
    public void Step_1(){
        ConfigHelper.loginBeforeTestStart(driver,user,site,testEnv);
        cardPage = new CardPage(driver,testEnv,site);

        //Assertions :
        CardDetailPanel.isComponentExist(driver);
        CardSelectionPanel.isComponentExist(driver);
        CardFundsPanel.isComponentExist(driver);
        CardTransactionsPanel.isComponentExist(driver);

    }

    /**
     *  Actions :
     *  1. Check the button near the "Debit Card" title .
     *
     *  Expected Results :
     *  1. " MY CARD" and  "REQUEST CARDS" buttons should EXIST near "DEBIT CARD" title and the button is RED
     */

    @Test(dependsOnMethods = "Step_1")
    public void Step_2(){
        //Expected Results :
        CardNavigationBar.getElement(driver,"CardPage_Title");
        CardNavigationBar.getElement(driver,"CardRequestPage_Link");
        CardNavigationBar.getElement(driver,"CardPage_Link");

    }





}
