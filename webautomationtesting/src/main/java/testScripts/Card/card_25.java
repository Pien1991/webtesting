package testScripts.Card;

import actions.LanguageActions;
import config.Components;
import config.Language;
import config.Site;
import dataTemplate.Component;
import dataTemplate.WebTestScript;
import dataTemplate.tables.CardOrderTable;
import org.openqa.selenium.WebDriverException;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import remoteObject.pages.card_request.CardRequestPage;
import util.ComponentHelper;
import util.configsHelper.ConfigHelper;
import util.parser.UsersParser;

/**
 * Created by shepardpin on 5/11/17.
 */
public class card_25 extends WebTestScript{

    CardRequestPage cardRequestPage ;

    Component eliteCard = ComponentHelper.getComponent(Components.REQUEST_ELITE_CARD);


    @BeforeClass
    @Override
    public void TestcasePreconditionTest() {
        site = Site.ANX;
        user = UsersParser.getUserByAlias("Verified_NoCards_NoOTP_User", testEnv, site);
    }

    /**
     *  Actions :
     *  1. Login ANXPRO and Navigate to /card/request page .
     *  2. Select "Digital Card" as Card Type and "1" as Amount for ANX BTC Elite Card and the amounts of other cards should be "0".
     *  3. Click "CONTINUE" button to "Delivery"  stage.
     *  4. Click "CONTINUE" button to "Confirmation" stage.
     *
     *  Expected Result:
     *  1.  For Delivery method in Confirmation table :
     *          Item    : Free Shipping
     *          Price   : 0 BTC
     *  2.  No Quantity cell should display .
     *  3.  No Cancel cell should display .
     */

    @Test
    public void Step_1(){

        //Action
        ConfigHelper.loginBeforeTestStart(driver,user,site,testEnv);
        cardRequestPage = new CardRequestPage(driver,testEnv,site);

        cardRequestPage.selectCardAmount(eliteCard,1);
        cardRequestPage.selectVirtual(eliteCard);
        cardRequestPage.clickContinueButton();
        cardRequestPage.clickContinueButton();



        //Assertion
        CardOrderTable cardOrderTable = cardRequestPage.getCardOrderTable();

        String mailingMethodText = cardOrderTable.getMailingMethodText();
        String mailingCost = cardOrderTable.getMailingCost();

        Assert.assertEquals(mailingMethodText,"Free Shipping");
        Assert.assertEquals(mailingCost,"0 BTC");
    }


    /**
     *  Actions :
     *  1. Click "Select Card" button in card navigation bar and return to "Select Card" stage.
     *  2. Select "Digital Card" as Card Type and "1" as Amount for ANX BTC Elite Card and the amounts of other cards should be "0".
     *  3. Click "CONTINUE" button to "Delivery"  stage.
     *  4. Click "CONTINUE" button to "Confirmation" stage.
     *
     *  Expected Result:
     *  1.  For Delivery method in Confirmation table :
     *          Item    : Registered Mail
     *          Price   : 0.04 BTC
     *  2.  No Quantity cell should display .
     *  3.  No Cancel cell should display .
     */

    @Test(dependsOnMethods = "Step_1")
    public void Step_2(){

        //Action
        cardRequestPage.clickSelectCardStage();

        cardRequestPage.selectCardAmount(eliteCard,1);
        cardRequestPage.selectPhysical(eliteCard);
        cardRequestPage.clickContinueButton();

        cardRequestPage.getDeliveryMethodSelect().selectByValue("registered");

        cardRequestPage.clickContinueButton();



        //Assertion
        CardOrderTable cardOrderTable = cardRequestPage.getCardOrderTable();

        String mailingMethodText = cardOrderTable.getMailingMethodText();
        String mailingCost = cardOrderTable.getMailingCost();

        Assert.assertEquals(mailingMethodText,"Registered Mail");
        Assert.assertEquals(mailingCost,"0.04 BTC");

        Assert.assertEquals(cardOrderTable.getMailingQuantity(),"");

        try {
            cardOrderTable.cancelOrderedCard("Registered");
            Assert.fail("The cancel button should NOT exist on Mail method");
        }catch (WebDriverException e){}

    }


    /**
     *  Actions :
     *  1. Click "Delivery" button in card navigation bar and return to "Delivery" stage.
     *  2. Select "Digital Card" as Card Type and "1" as Amount for ANX BTC Elite Card and the amounts of other cards should be "0".
     *  3. Click "CONTINUE" button to "Delivery"  stage.
     *  4. Click "CONTINUE" button to "Confirmation" stage.
     *
     *  Expected Result:
     *  1.  For Delivery method in Confirmation table :
     *          Item    : Express Registered Mail (Region A)
     *          Price   : 0.17 BTC
     *  2.  No Quantity cell should display .
     *  3.  No Cancel cell should display .
     */

    @Test(dependsOnMethods = "Step_1")
    public void Step_3(){

        //Action
        cardRequestPage.clickDeliveryStage();
        cardRequestPage.getDeliveryMethodSelect().selectByValue("ShippingTypeA");
        cardRequestPage.clickContinueButton();



        //Assertion
        CardOrderTable cardOrderTable = cardRequestPage.getCardOrderTable();

        String mailingMethodText = cardOrderTable.getMailingMethodText();
        String mailingCost = cardOrderTable.getMailingCost();

        Assert.assertEquals(mailingMethodText,"Express Registered Mail (Region A)");
        Assert.assertEquals(mailingCost,"0.17 BTC");

        Assert.assertEquals(cardOrderTable.getMailingQuantity(),"");

        try {
            cardOrderTable.cancelOrderedCard("Registered");
            Assert.fail("The cancel button should NOT exist on Mail method");
        }catch (WebDriverException e){}
    }


}
