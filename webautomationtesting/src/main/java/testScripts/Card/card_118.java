package testScripts.Card;

import actions.LanguageActions;
import config.Components;
import config.Language;
import config.Site;
import dataTemplate.Component;
import dataTemplate.WebTestScript;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ISelect;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import remoteObject.pages.card.CardPage;
import util.ComponentHelper;
import util.configsHelper.ConfigHelper;
import util.parser.UsersParser;

/**
 * Created by shepardpin on 5/15/17.
 */
public class card_118 extends WebTestScript {

    CardPage cardPage;
    Component cardFundPanel = ComponentHelper.getComponent(Components.CARD_FUNDS_PANEL);


    /**
     * Pre-Conditions :
     *  1.  User own ONLY ONE Non-RHA activated card  (In OAT , assume to be Plus card)
     *  2.  In card_type table , the fiat_topup_enabled = false , topup_enabled = true , base_currency = USD , min_topup = 100 and max_topup = 50000 for tested card's card type .
     *  3.  NO recharge fee should exist .
     *  4.  In user's BTC account , his/her total balance = 9.94 BTC,  available BTC balance = 5 BTC
     *  5.  In user's USD account , his/her total balance = 1000 USD,  available BTC balance = 500 USD
     *
     */
    @BeforeClass
    @Override
    public void TestcasePreconditionTest() {
        site = Site.ANX;
        user = UsersParser.getUserByAlias("Fiat_Disable_Recharge_Display_Test", testEnv, site);
    }


    /**
     * Actions :
     * 1.   Login ANXPRO and Navigate to /card page  .
     * 2.   Click "FIAT CURRENCIES" tab
     *
     * Expected Results :
     * 1.   NO Fiat Account Select Box should exist in Recharge area .
     *
     */


    @Test
    public void Step_1() {
        //Actions :
        ConfigHelper.loginBeforeTestStart(driver, user, site, testEnv);

        cardPage = new CardPage(driver, testEnv, site);

        LanguageActions.ChangeLanguageTo(driver, Language.ENG);

        cardPage.clickChargeByFiatTab();


        //Assertion on Non Existence of Charge Account Selection Box
        try {
            cardFundPanel.getElement(driver, "ChargeAccount_Select");
        } catch (WebDriverException e) {
        }

        //Assertion on Correctness of Available Balance
        WebElement AvailableBalance =  cardFundPanel.getElement(driver,"AvailableBalance_Text");
        Assert.assertEquals(AvailableBalance.getText(),"Available Balance: 5.00000000 BTC");

        //Assertion on  Existence of Quote Price button
        cardFundPanel.getElement(driver, "QuotePrice_Button");

        //Assertion on  Existence of Recharge Amount Input
        cardFundPanel.getElement(driver, "ChargeAmount_Input");
        //Assertion on  Existence of Recharge Amount Base Currency Selection
        cardFundPanel.getElement(driver, "RechargeUnit_Select");


    }

    /**
     * Actions :
     * 1.   Click "Unit" dropdown box near the Recharge Amount Input box . Select BTC
     * 2.   Click "Unit" dropdown box near the Recharge Amount Input box . Select USD
     *
     * Expected Results :
     * 1.   The dropdown box should ONLY show USD and BTC.
     *
     */

    @Test(dependsOnMethods = "Step_1")
    public void Step_2() {
        //Actions :
        ISelect RechargeUnitSelect = cardPage.getRechargeUnitSelection();


        RechargeUnitSelect.selectByVisibleText("BTC");
        RechargeUnitSelect.selectByVisibleText("USD");
        Assert.assertEquals(RechargeUnitSelect.getOptions().size(),2);

    }



    /**
     * Actions :
     * 1.   Click "Unit" dropdown box near the Recharge Amount Input box . Select USD
     *
     * Expected Results :
     * 1.   Inside the Recharge Amount Input box , the amount should be 100-500
     *
     */

    @Test(dependsOnMethods = "Step_2")
    public void Step_3() {
        //Actions :
        cardPage.getRechargeUnitSelection().selectByVisibleText("USD");

        //Assertions :
        WebElement RechargeAmountInput = cardFundPanel.getElement(driver,"ChargeAmount_Input");
        String minMaxAmount = RechargeAmountInput.getAttribute("placeholder");
        Assert.assertEquals(minMaxAmount,"100 - 500");


    }




}
