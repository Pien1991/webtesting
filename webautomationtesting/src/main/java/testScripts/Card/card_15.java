package testScripts.Card;

import config.Components;
import config.Site;
import dataTemplate.Component;
import dataTemplate.WebTestScript;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import remoteObject.pages.card_request.CardRequestPage;
import util.ComponentHelper;
import util.conditionsHelper.EnhancedExpectedConditions;
import util.configsHelper.ConfigHelper;
import util.parser.UsersParser;

import java.util.regex.Pattern;

/**
 * Created by shepardpin on 5/10/17.
 */
public class card_15 extends WebTestScript {


    CardRequestPage cardRequestPage;
    Component eliteCard = ComponentHelper.getComponent(Components.REQUEST_ELITE_CARD);


    /**
     *  Pre-Conditions :
     *  1. Tested User is BETA user . The beta_features =  true in user table for the tested user .
     *  2. Test Site is in beta_feature table where is_beta =  true .
     *  3. Tested User is Verified .
     *  4. For ANX ELITE (USD) , the order_cost = 0.05 , order_currency = BTC , card_cost = 0.03 and base_currency = USD in card_type table .
     *
     */

    @BeforeClass
    @Override
    public void TestcasePreconditionTest() {
        user= UsersParser.getUserByAlias("Verified_NoCards_NoOTP_User",testEnv,site);
        site = Site.ANX ;
    }

    /**
     *  Actions :
     *  1. Login and Navigate to /card/request page .
     *
     *  Expected Results :
     *  1. The Cost should display 0.05 as Card Cost and BTC as Cost Unit.
     *
     */

    @Test
    public void Step_1(){
        ConfigHelper.loginBeforeTestStart(driver,user,site,testEnv);
        cardRequestPage = new CardRequestPage(driver,testEnv,site);

        //Expected result assertions
        WebElement cardTitle = eliteCard.getElement(driver,"Card_Title");
        new WebDriverWait(driver,3).until(EnhancedExpectedConditions.textContains(cardTitle,"0.05 BTC"));
    }

}
