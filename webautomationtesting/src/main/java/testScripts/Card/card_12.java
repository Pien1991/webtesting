package testScripts.Card;

import config.Components;
import config.Page;
import config.Site;
import dataTemplate.Component;
import dataTemplate.WebTestScript;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import util.ComponentHelper;
import util.configsHelper.ConfigHelper;
import util.navigationHelper.NavigationHelper;
import util.parser.UsersParser;

/**
 * Created by shepardpin on 5/4/17.
 *
 */
public class card_12 extends WebTestScript {


    Component _404_panel = ComponentHelper.getComponent(Components._404_PAGE);

    /**
     *  Pre-Conditions :
     *  1. The user finished verification. Hence , the verification_state in user table should be VERIFIED
     */

    @BeforeClass
    @Override
    public void TestcasePreconditionTest() {
        user= UsersParser.getUserByAlias("Unverified_User",testEnv,site);
        site = Site.ANX ;
    }


    /**
     *  Actions :
     *  1. Login and Navigate to /card/request page.
     *
     *  Expected Results :
     *  1. The page should redirect to /card page
     */

    @Test
    public void Step_1(){

        // Actions :
        ConfigHelper.loginBeforeTestStart(driver,user,site,testEnv);
        NavigationHelper.navigate(driver,testEnv,site, Page.CARD_REQUEST);

        //Expected Result :
        new WebDriverWait(driver,6).until(ExpectedConditions.urlToBe(Site.getUrl(site,testEnv)+"/card"));
    }

    /**
     *  Actions :
     *  1. Navigate to /card/11111111 page.
     *
     *  Expected Results :
     *  1. The page should be 404 page .
     */
    @Test(dependsOnMethods = "Step_1")
    public void Step_2(){

        // Actions :
        driver.get(Site.getUrl(site,testEnv)+"/card/111111");

        //Expected Result :
        new WebDriverWait(driver,5).until(ExpectedConditions.urlToBe(Site.getUrl(site,testEnv)+"/pages/404"));
        _404_panel.isComponentExist(driver);
        _404_panel.getElement(driver,"Rotating_Logo_Picture");
        _404_panel.getElement(driver,"Error_404_Title");
        _404_panel.getElement(driver,"Error_404_Message");

    }



}
