package testScripts.Card;

import config.Components;
import config.Site;
import dataTemplate.Component;
import dataTemplate.WebTestScript;
import dataTemplate.tables.CardOrderTable;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import remoteObject.pages.card_request.CardRequestPage;
import util.ComponentHelper;
import util.configsHelper.ConfigHelper;
import util.parser.UsersParser;

/**
 * Created by shepardpin on 5/10/17.
 */
public class card_21 extends WebTestScript{

    CardRequestPage cardRequestPage ;

    Component eliteCard = ComponentHelper.getComponent(Components.REQUEST_ELITE_CARD);


    //It represents Premium card in DCEXE which the purchase unit is USD and Premium card in ANXPRO which the purchase unit is BTC
    Component USDPremiumCard = ComponentHelper.getComponent(Components.REQUEST_USD_PREMIUM_CARD);




    @BeforeClass
    @Override
    public void TestcasePreconditionTest() {
        site = Site.ANX;
        user = UsersParser.getUserByAlias("Verified_NoCards_NoOTP_User", testEnv, site);
    }


    /**
     *  Actions :
     *  1. Login ANXPRO and Navigate to /card/request page .
     *  2. Select "Digital Card" as Card Type and "1" as Amount for ANX BTC Elite Card and the amounts of other cards should be "0".
     *  3. Click "CONTINUE" button to "Delivery"  stage.
     *  4. Click "CONTINUE" button to "Confirmation" stage.
     *
     *  Expected Result:
     *  1. The price of ANX BTC Elite Card is 0.05 BTC
     *  2. The quantity of ANX BTC Elite Card is 1
     *
     */

    @Test
    public void Step_1(){

        //Action
        ConfigHelper.loginBeforeTestStart(driver,user,site,testEnv);
        cardRequestPage = new CardRequestPage(driver,testEnv,site);

        cardRequestPage.selectCardAmount(eliteCard,1);
        cardRequestPage.selectCardAmount(USDPremiumCard,0);
        cardRequestPage.selectVirtual(eliteCard);
        cardRequestPage.clickContinueButton();
        cardRequestPage.clickContinueButton();
        CardOrderTable cardOrderTable = cardRequestPage.getCardOrderTable();

        //Assertion
        String cardQuantity = cardOrderTable.getCardQuantity("Elite (USD)");
        String cardPrice = cardOrderTable.getCardCost("Elite (USD)");

        Assert.assertEquals(cardQuantity,"1");
        Assert.assertEquals(cardPrice,"0.05 BTC");
    }


    /**
     *  Actions :
     *  1. Click "Select Card" button in card navigation bar and return to "Select Card" stage.
     *  2. Select "Digital Card" as Card Type and "2" as Amount for ANX BTC Elite Card and the amounts of other cards should be "0".
     *  3. Click "CONTINUE" button to "Delivery"  stage.
     *  4. Click "CONTINUE" button to "Confirmation" stage.
     *
     *  Expected Result:
     *  1. The price of ANX BTC Elite Card is 0.05 BTC
     *  2. The quantity of ANX BTC Elite Card is 2
     *
     */

    @Test()
    public void Step_2(){

        //Action
        cardRequestPage.clickSelectCardStage();

        cardRequestPage.selectCardAmount(eliteCard,2);
        cardRequestPage.selectCardAmount(USDPremiumCard,0);
        cardRequestPage.selectVirtual(eliteCard);
        cardRequestPage.clickContinueButton();
        cardRequestPage.clickContinueButton();
        CardOrderTable cardOrderTable = cardRequestPage.getCardOrderTable();

        //Assertion
        String cardQuantity = cardOrderTable.getCardQuantity("Elite (USD)");
        String cardPrice = cardOrderTable.getCardCost("Elite (USD)");

        Assert.assertEquals(cardQuantity,"2");
        Assert.assertEquals(cardPrice,"0.05 BTC");
    }

    /**
     *  Actions :
     *  1. Click "Select Card" button in card navigation bar and return to "Select Card" stage.
     *  2. Select "ATM Card" as Card Type and "1" as Amount for ANX BTC Elite Card and the amounts of other cards should be "0".
     *  3. Click "CONTINUE" button to "Delivery"  stage.
     *  4. Click "CONTINUE" button to "Confirmation" stage.
     *
     *  Expected Result:
     *  1. The price of ANX BTC Elite Card is 0.05 BTC
     *  2. The quantity of ANX BTC Elite Card is 2
     *
     */

    @Test()
    public void Step_3(){

        //Action :
        cardRequestPage.clickSelectCardStage();
        cardRequestPage.selectCardAmount(eliteCard,1);
        cardRequestPage.selectCardAmount(USDPremiumCard,0);
        cardRequestPage.selectPhysical(eliteCard);
        cardRequestPage.clickContinueButton();
        cardRequestPage.clickContinueButton();
        CardOrderTable cardOrderTable = cardRequestPage.getCardOrderTable();

        //Assertion
        String cardQuantity = cardOrderTable.getCardQuantity("Elite (USD)");
        String cardPrice = cardOrderTable.getCardCost("Elite (USD)");

        Assert.assertEquals(cardQuantity,"1");
        Assert.assertEquals(cardPrice,"0.05 BTC");
    }


    /**
     *  Actions :
     *  1. Click "Select Card" button in card navigation bar and return to "Select Card" stage.
     *  2. Select "ATM Card" as Card Type and "1" as Amount for ANX BTC Premium Card and the amounts of other cards should be "0".
     *  3. Click "CONTINUE" button to "Delivery"  stage.
     *  4. Click "CONTINUE" button to "Confirmation" stage.
     *
     *  Expected Result:
     *  1. The price of ANX BTC Elite Card is 0.02 BTC
     *  2. The quantity of ANX BTC Elite Card is 1
     *
     */

    @Test()
    public void Step_4(){


        //Action :
        cardRequestPage.clickSelectCardStage();

        cardRequestPage.selectCardAmount(eliteCard,0);
        cardRequestPage.selectCardAmount(USDPremiumCard,1);

        cardRequestPage.clickContinueButton();
        cardRequestPage.clickContinueButton();
        CardOrderTable cardOrderTable = cardRequestPage.getCardOrderTable();

        //Assertion
        String cardQuantity = cardOrderTable.getCardQuantity("Premium");
        String cardPrice = cardOrderTable.getCardCost("Premium");

        Assert.assertEquals(cardQuantity,"1");
        Assert.assertEquals(cardPrice,"0.02 BTC");
    }

    /**
     *  Actions :
     *  1. Click "Select Card" button in card navigation bar and return to "Select Card" stage.
     *  2. Select "ATM Card" as Card Type and "1" as Amount for ANX BTC Premium Card .
     *  3. Select "ATM Card" as Card Type and "1" as Amount for ANX BTC Elite Card .
     *  4. Click "CONTINUE" button to "Delivery"  stage.
     *  5. Click "CONTINUE" button to "Confirmation" stage.
     *
     *  Expected Result:
     *  1. The price of ANX BTC Elite Card is 0.02 BTC
     *  2. The quantity of ANX BTC Elite Card is 1
     *
     */

    @Test()
    public void Step_5(){

        //Action :
        cardRequestPage.clickSelectCardStage();

        cardRequestPage.selectCardAmount(eliteCard,1);
        cardRequestPage.selectPhysical(eliteCard);

        cardRequestPage.selectCardAmount(USDPremiumCard,1);

        cardRequestPage.clickContinueButton();
        cardRequestPage.clickContinueButton();
        CardOrderTable cardOrderTable = cardRequestPage.getCardOrderTable();


        //Assertion
        String eliteCardQuantity = cardOrderTable.getCardQuantity("Elite (USD)");
        String eliteCardPrice = cardOrderTable.getCardCost("Elite (USD)");

        Assert.assertEquals(eliteCardQuantity,"1");
        Assert.assertEquals(eliteCardPrice,"0.05 BTC");


        String premiumCardQuantity = cardOrderTable.getCardQuantity("Premium");
        String premiumCardPrice = cardOrderTable.getCardCost("Premium");

        Assert.assertEquals(premiumCardQuantity,"1");
        Assert.assertEquals(premiumCardPrice,"0.02 BTC");
    }


}
