package testScripts.Card;

import config.Components;
import config.Site;
import dataTemplate.Component;
import dataTemplate.WebTestScript;
import dataTemplate.tables.CardOrderTable;
import org.openqa.selenium.WebDriverException;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import remoteObject.pages.card_request.CardRequestPage;
import util.ComponentHelper;
import util.configsHelper.ConfigHelper;
import util.parser.UsersParser;

/**
 * Created by shepardpin on 5/11/17.
 */
public class card_44 extends WebTestScript {

    CardRequestPage cardRequestPage ;

    Component eliteCard = ComponentHelper.getComponent(Components.REQUEST_ELITE_CARD);

    Component cardChoicePanel = ComponentHelper.getComponent(Components.REQUEST_SELECT_CARD_PANEL);
    Component cardDeliveryPanel = ComponentHelper.getComponent(Components.REQUEST_CARD_DELIVERY_PANEL);
    Component cardConfirmPanel = ComponentHelper.getComponent(Components.REQUEST_CARD_CONFIRM_PANEL);


    @BeforeClass
    @Override
    public void TestcasePreconditionTest() {
        site = Site.ANX;
        user = UsersParser.getUserByAlias("Verified_NoCards_NoOTP_User", testEnv, site);
    }


    /**
     *  Actions :
     *  1. Login ANXPRO and Navigate to /card/request page .
     *  2. Select "Digital Card" as Card Type and "1" as Amount for ANX BTC Elite Card .
     *  3.  Select "ATM Card" as Card Type and "1" as Amount for ANX BTC Premium Card .
     *  4. Click "CONTINUE" button to "Delivery"  stage.
     *  5. Click "CONTINUE" button to "Confirmation" stage.
     *  6. Click "CANCEL" button.
     *
     *  Expected Result:
     *  1. In Confirmation table :
     *      -   The row should disappear for  ANX BTC Premium Card .
     *      -   Only ANX BTC Elite Card should exist .
     *      -   The Delivery method should change to "Free Shipping" with 0 BTC price
     *
     */

    @Test
    public void Step_1(){

        //Action
        ConfigHelper.loginBeforeTestStart(driver,user,site,testEnv);
        cardRequestPage = new CardRequestPage(driver,testEnv,site);

        cardRequestPage.selectCardAmount(eliteCard,1);
        cardRequestPage.selectVirtual(eliteCard);
        cardRequestPage.clickContinueButton();
        cardRequestPage.clickContinueButton();

        cardRequestPage.clickCancelButton();

        //Assertion
        cardChoicePanel.isComponentExist(driver);

        try{
            cardDeliveryPanel.isComponentExist(driver);
            Assert.fail("Should NOT find card delivery panel after cancel request in confirmation panel");
        }catch (WebDriverException e){}

        try{
            cardConfirmPanel.isComponentExist(driver);
            Assert.fail("Should NOT find card confirmation panel after cancel request in confirmation panel");
        }catch (WebDriverException e){}

    }

}
