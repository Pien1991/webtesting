package config;

/**
 * Created by workstation47 on 10/18/16.
 */
public enum Page  {
        LOGIN ("/signin"),
        EXPLORER_FRONT(""),
        FRONT (""),
        FUNDS ("/funds"),
        CARD ("/card"),
        PORTFOLIO ("/user"),
        ACTIVITY("/activity"),
        TRADE("/trade"),
        CARD_REQUEST("/card/request"),
        REPORTS("/reports"),
        SETTINGS("/settings"),
        SETTINGS_VERIFY("/settings/verify"),
        SETTINGS_PASSWORD("/settings/password"),
        SETTINGS_SECURITY("/settings/twoFactor"),
        SETTINGS_NOTIFICATIONS("/settings/notifications"),
        SETTINGS_APIKEYS("/settings/twoFactor"),
        SETTINGS_EMAIL("/settings/waiveEmailConfirmation"),;


    private final String page;

    private Page(String s) {
        page = s;
    }


    private static String getServerName(String url){
        String [] urls = url.split(".com");
        return urls[0]+".com";
    }



    public static String getTargetUrl(String currentUrl,Page page){
        return getServerName(currentUrl)+ page.toString();
    }

    @Override
    public String toString() {
        return page;
    }



}
