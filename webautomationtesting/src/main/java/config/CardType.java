package config;

/**
 * Created by workstation47 on 1/4/17.
 */
public enum CardType {

    ELITE("Elite"),
    PREMIUM("Premium"),
    PLUS("Plus"),
    WEC_PREMIUM_USD("USD"),
    WEC_PREMIUM_HKD("HKD");

    private final String name;

    private CardType(String s) {
        name = s;
    }

    public String toString() {
        return this.name;
    }


}
