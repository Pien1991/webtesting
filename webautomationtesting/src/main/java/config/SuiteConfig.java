package config;

/**
 * Created by workstation47 on 12/12/16.
 */
public enum SuiteConfig {

    USERNAME ("USERNAME"),
    WEBSITE ("WEBSITE"),
    URL("URL"),
    USERALIAS ("USERALIAS"),
    TEST_ENV("TEST_ENV"),
    BROWSER("BROWSER"),
    SYSTEM("SYSTEM"),
    REPORT_SERVER("REPORT_SERVER"),
    TEST_PROJECT("TEST_PROJECT"),
    TEST_PLAN("TEST_PLAN");


    private final String name;

    private SuiteConfig(String s) {
        name = s;
    }

    public String toString() {
        return this.name;
    }

    public boolean equalsName(String otherName) {
        return (otherName == null) ? false : name.equals(otherName);
    }

}
