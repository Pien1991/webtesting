package config;


/**
 * Created by workstation47 on 10/18/16.
 */
public enum TestEnv {


        PRO("PRO"),
        OAT("OAT"),
        UAT("UAT"),
        SIT("SIT"),
        UIT("UIT");

    private final String name;

    private TestEnv(String s) {
        name = s;
    }

    public String toString() {
        return this.name;
    }



    public static String getIP(TestEnv testEnv){
        switch (testEnv){
            case OAT    :   return "35.162.221.75";
            default     :   throw new IllegalArgumentException("We don't have the IP : "+ testEnv);
        }
    }

    public static String getIP(String server){
        return getIP(getTestEnv(server));
    }

    public static TestEnv getTestEnv(String server){
        switch (server){
            case "PRO"      :   return TestEnv.PRO;
            case "OAT"      :   return TestEnv.OAT;
            case "UAT"      :   return TestEnv.UAT;
            case "SIT"      :   return TestEnv.SIT;
            case "UIT"      :   return TestEnv.UIT;
            default         :   throw new IllegalArgumentException("We don't have the environment : "+server);
        }
    }


}
