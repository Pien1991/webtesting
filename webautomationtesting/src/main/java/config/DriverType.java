package config;

/**
 * Created by workstation47 on 11/3/16.
 *
 *
 *  We do not support different browser size in this moment . 18/04/2017
 *
 */
public enum DriverType {

        FIREFOX("FIREFOX"),
        CHROME("CHROME"),
        SAFARI("SAFARI");




    private final String name;

    private DriverType(String s) {
        name = s;
    }

    public String toString() {
        return this.name;
    }

    public static DriverType getDriverType(String driver){
        switch (driver){
            case "FIREFOX"      :   return DriverType.FIREFOX;
            case "CHROME"      :   return DriverType.CHROME;
//            case "SAFARI"      :   return DriverType.SAFARI;
            default         :   throw new IllegalArgumentException("Did not support this driver yet : "+driver);
        }
    }






}
