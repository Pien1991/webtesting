package config;

/**
 * Created by shepardpin on 4/25/17.
 */
public enum Components {


    EXPLORER_FRONT_ABOUT_INFO_PANEL("/locators/explorer/AboutInfoPanel.xml"),
    EXPLORER_FRONT_FOOTER("/locators/explorer/ExplorerFooter.xml"),
    EXPLORER_FRONT_HEADER("/locators/explorer/ExplorerHeader.xml"),
    EXPLORER_FRONT_LATEST_INFO_PANEL("/locators/explorer/LatestInfoPanel.xml"),
    CARD_ALIAS_PANEL("/locators/card/AliasPanel.xml"),
    CARD_ADVERTISEMENT_PANEL("/locators/card/AdvertisementPanel.xml"),
    CARD_DETAIL_PANEL("/locators/card/CardDetailPanel.xml"),
    CARD_FUNDS_PANEL("/locators/card/CardRechargePanel.xml"),
    CARD_TRANSACTIONS_PANEL("/locators/card/CardTransactionsPanel.xml"),
    CARD_NAVIGATION_BAR("/locators/card/CardNavigationBar.xml"),
    CARD_SELECTION_PANEL("/locators/card/CardSelectionPanel.xml"),
    CARD_NOTIFICATION_PANEL("/locators/card/NotificationPanel.xml"),
    LOGIN_FORM("/locators/user/LoginForm.xml"),
    REQUEST_SELECT_CARD_PANEL("/locators/cardsRequest/CardChoicePanel.xml"),
    REQUEST_CARD_CONFIRM_PANEL("/locators/cardsRequest/CardConfirmPanel.xml"),
    REQUEST_CARD_CONFIRMATION_MESSAGE_BOX("/locators/cardsRequest/ConfirmationMessagePanel.xml"),
    REQUEST_CARD_DELIVERY_PANEL("/locators/cardsRequest/CardDeliveryPanel.xml"),
    REQUEST_CARD_STAGE_BAR("/locators/cardsRequest/RequestStagBar.xml"),
    REQUEST_ELITE_CARD("/locators/cardsRequest/RHACard.xml"),
    REQUEST_HKD_PREMIUM_CARD("/locators/cardsRequest/HKDPremiumCard.xml"),
    REQUEST_USD_PREMIUM_CARD("/locators/cardsRequest/USDPremiumCard.xml"),
    REQUEST_PLUS_CARD("/locators/cardsRequest/PlusCard.xml"),


    HEADER("/locators/Header.xml"),
    _404_PAGE("/locators/_404_Page.xml");

    private final String filePath;

    private Components(String s) {
        filePath = s;
    }

    public String toString() {
        return this.filePath;
    }


}
