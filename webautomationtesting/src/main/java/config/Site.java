package config;

import org.openqa.selenium.WebDriver;

/**
 * Created by workstation47 on 10/13/16.
 */
public enum Site {

        ANX("ANX"),
        DIGA("DIGA"),
        DCEXE("DCEXE"),
        KCOIN("KCOIN"),
        MYT("MYT"),
        NIG("NIG"),
        EXPLORER("EXPLORER");

    private final String name;

    private Site(String s) {
        name = s;
    }

    public String toString() {
        return this.name;
    }


    private static String httpHead  ="https://";

    public static Site getSite(String name){
        if (name == null){
            // Default site is ANX
            return Site.ANX;
        }
        switch (name){
            case "ANX"      :   return Site.ANX;
            case "DIGA"     :   return Site.DIGA;
            case "DCEXE"    :   return Site.DCEXE;
            case "KCOIN"    :   return Site.KCOIN;
            case "MYT"      :   return Site.MYT;
            case "NIG"      :   return Site.NIG;
            case "EXPLORER" :   return Site.EXPLORER;
            default     :   throw new IllegalArgumentException("No such Site Origin : "+name);
        }
    }


    public static String getSiteOrigin(Site shortName){

        switch (shortName){
            case ANX    :   return "_ANXPRO_COM";
            case DIGA   :   return "_DIGATRADE_COM";
            case DCEXE  :   return "_WOWDIGIT_COM";
            case KCOIN  :   return "_KCOIN_COM";
            case MYT    :   return "_MYTHOLOGY_COM";
            case NIG    :   return "_NIGEX_COM";
            default     :   throw new IllegalArgumentException("No such Site Origin : "+shortName);
        }
    }



    public static String getUrl(Site site , TestEnv testEnv){
        switch (testEnv){
            case OAT:   return oatURL(site);
            case UAT:   return uatURL(site);
            case UIT:   return uitURL(site);
            case SIT:   return sitURL(site);
            case PRO:   return productionURL(site);
            default     :   throw new IllegalArgumentException("No such URL - shortname : " +site+" & "+"- testEnv : "+ testEnv);
        }
    }

    public static String getUrl(String name , TestEnv testEnv){
        return getUrl(getSite(name), testEnv);
    }

    public static String getUrl(String name , String server){
        return getUrl(getSite(name), TestEnv.getTestEnv(server));
    }

    public static boolean isSameSite(Site name, WebDriver driver){
        if (driver.getCurrentUrl().contains(getUrl(name))) {
            return true;
        }
        return false;
    }

    private static String productionURL(Site site){
        return httpHead+ getUrl(site);
    }

    private static String oatURL(Site site){
        return httpHead+"oat."+ getUrl(site);
    }

    private static String uitURL(Site site){
        return httpHead+"uit."+ getUrl(site);
    }

    private  static String sitURL(Site site){
        return httpHead+"sit."+ getUrl(site);
    }

    private  static String uatURL(Site site){
        return httpHead+"uat."+ getUrl(site);
    }

    private static String getUrl(Site site){

        switch (site){
            case ANX        :   return "anxpro.com";
            case DIGA       :   return "digatrade.com";
            case DCEXE      :   return "dcexe.com";
            case KCOIN      :   return "kcoin.com";
            case MYT        :   return "mydicewallet.com";
            case NIG        :   return "nig-ex.com";
            case EXPLORER   :
            default         :   throw new IllegalArgumentException("No such URL : "+site);
        }
    }

}
